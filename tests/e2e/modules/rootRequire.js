module.exports = function() {
    global.rootRequire = function(name) {
        return require(__dirname + '/' + name);
    };
};