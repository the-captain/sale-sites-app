const Nightmare = require('nightmare');
const assert = require('assert');
require('./rootRequire')();
rootRequire('../Config');

beforeEach(() => {
    nightmare = new Nightmare();
})

module.exports = {
    checkMainBlocks: function(descriptionTest, block) {
        it(descriptionTest, done => {
            nightmare
                .goto(mainLink + pathPage)
                .wait(block)
                .end()
                .then((result) => {
                    done()
                })
                .catch(done)
        })
    },
    checkButtons: function(descriptionTest, link) {
        it(descriptionTest, done => {
            nightmare
                .goto(mainLink + pathPage)
                .click(link)
                .end()
                .then((result) => {
                    done()
                })
                .catch(done)
        })
    },
    correctClickOnButton: function(descriptionTest, link, correctMainLink) {
        it (descriptionTest, done => {
            nightmare
                .goto(mainLink + pathPage)
                .click(link)
                .url()
                .end()
                .then((url) => {
                    assert.equal(url, mainLink + correctMainLink);
                    done()
                })
                .catch(done)
        })
    },
    correctClickOnTwoButtons: function(
        descriptionTest,
        firstBut,
        secondBut,
        correctMainLink
    ) {
        it (descriptionTest, done => {
            nightmare
                .goto(mainLink + pathPage)
                .click(firstBut)
                .click(secondBut)
                .url()
                .end()
                .then((url) => {
                    assert.equal(url, mainLink + correctMainLink);
                    done()
                })
                .catch(done)
        })
    },
    existingPage: function(descriptionTest, correctMainLink) {
        it (descriptionTest, done => {
            nightmare
                .goto(mainLink + correctMainLink)
                .end()
                .then((result) => {
                    assert.equal(result.code, 200);
                    done()
                })
        })
    },
    describeDevider: function() {
        after(() => {
            console.log(' ');
        })
    }
};