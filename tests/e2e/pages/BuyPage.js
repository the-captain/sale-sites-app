const Nightmare = require('nightmare');
require('../modules/rootRequire')();
const modules = rootRequire('Modules');
const layout = rootRequire('../pages/Layout');
pathPage = '/buy';

describe('Testing' + ' ' + pathPage + ' ' + 'page from user =>' + ' ' + userId, function() {
    this.timeout('30s');

    layout();
    describe('On button click go to certain page', function() {
        modules.describeDevider();
        modules.correctClickOnButton(
            'Clicking the checkout basket in header',
            'header .checkout a',
            '/checkout'
        );
    });

    describe('The output of categories of items', function() {
        modules.describeDevider();
        modules.correctClickOnButton(
            'click apple iphone category go to iphone page',
            '.models .col-md-3:nth-child(1) a',
            '/buy/category/1-apple-iphone/items/list'
        );
        modules.correctClickOnButton(
            'click apple ipad category go to ipad page',
            '.models .col-md-3:nth-child(2) a',
            '/buy/category/2-apple-ipad/items/list'
        );
        modules.correctClickOnButton(
            'click apple macbook category go to macbook page',
            '.models .col-md-3:nth-child(4) a',
            '/buy/category/14-apple-macbook/items/list'
        );
        modules.correctClickOnTwoButtons(
            'click tablets category after clicking show more and go to tablets page',
            '.show-all',
            '.hide-models .col-md-3:nth-child(1) a',
            '/buy/category/15-tablets/items/list'
        );
        modules.correctClickOnTwoButtons(
            'click tools category after clicking show more and go to tools page',
            '.show-all',
            '.hide-models .col-md-3:nth-child(2) a',
            '/buy/category/157-tools/items/list'
        );
        modules.correctClickOnTwoButtons(
            'click yahoo category after clicking show more and go to yahoo page',
            '.show-all',
            '.hide-models .col-md-3:nth-child(3) a',
            '/buy/category/161-yahoo/items/list'
        );
    });

    describe('Existence of pages', function() {
        modules.describeDevider();
        modules.existingPage(
            'should load buy/iphone page',
            '/buy/category/1-apple-iphone/items/list'
        );
        modules.existingPage(
            'should load buy/ipad page',
            '/buy/category/2-apple-ipad/items/list'
        );
        modules.existingPage(
            'should load buy/macbook page',
            '/buy/category/14-apple-macbook/items/list'
        );
        modules.existingPage(
            'should load buy/tablet page',
            '/buy/category/15-tablets/items/list'
        );
        modules.existingPage(
            'should load buy/tools page',
            '/buy/category/157-tools/items/list'
        );
        modules.existingPage(
            'should load buy/yahoo page',
            '/buy/category/161-yahoo/items/list'
        );
    });
});