const Nightmare = require('nightmare');
require('../modules/rootRequire')();
const modules = rootRequire('Modules');

describe('Testing existence of pages from user =>' + ' ' + userId, function() {
    this.timeout('30s');

    modules.existingPage('Index page', '/');
    modules.existingPage('Buy page', '/buy');
    modules.existingPage('Sell page', '/sell');
    modules.existingPage('Repair page', '/repair');
    modules.existingPage('Faq page', '/faq');
    modules.existingPage('Contact us page', '/contact');
    modules.existingPage('Privacy page', '/privacy');
    modules.existingPage('Terms page', '/terms');
});