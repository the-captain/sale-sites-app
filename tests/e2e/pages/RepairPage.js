const Nightmare = require('nightmare');
require('../modules/rootRequire')();
const modules = rootRequire('Modules');
const layout = rootRequire('../pages/Layout');
pathPage = '/repair';

describe('Testing' + ' ' + pathPage + ' ' + 'page from user =>' + ' ' + userId, function() {
    this.timeout('30s');

    layout();
    describe('On button click go to certain page', function() {
        modules.correctClickOnButton(
            'Clicking the checkout basket in header',
            'header .checkout a',
            '/checkout'
        );
    });
});