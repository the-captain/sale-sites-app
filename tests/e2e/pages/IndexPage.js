const Nightmare = require('nightmare');
require('../modules/rootRequire')();
const modules = rootRequire('Modules');
const layout = rootRequire('../pages/Layout');
pathPage = '/';

describe('Testing index page from user =>' + ' ' + userId, function() {
    this.timeout('30s');

    layout();
    describe('On button click in body go to certain page', function() {
        modules.describeDevider();
        modules.correctClickOnButton(
            'Clicking on the big-phone buy button',
            'a.first-img',
            '/buy'
        );
        modules.correctClickOnButton(
            'Clicking on the big-phone sell button',
            'a.second-img',
            '/sell'
        );
        modules.correctClickOnButton(
            'Clicking on the big-phone repair button',
            'a.third-img',
            '/repair'
        );
    });
});