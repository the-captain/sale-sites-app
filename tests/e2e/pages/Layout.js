const Nightmare = require('nightmare');
var modules = rootRequire('Modules');

module.exports = function() {
        describe('Check header and footer', function() {
            describe('Exists block', function() {
                modules.describeDevider();
                modules.checkMainBlocks('Header', 'header');
                modules.checkMainBlocks('Footer', 'footer');
            });

            describe('Exists buttons in header', function() {
                modules.describeDevider();
                modules.checkButtons('Sell button', 'header a.btn.sell');
                modules.checkButtons('Buy button', 'header a.btn.buy');
                modules.checkButtons('Repair button', 'header a.btn.repair');
                modules.checkButtons('Faq button', '.top-icons #faq');
                modules.checkButtons('Location button', '.top-icons #locations');
                modules.checkButtons('Logo in header', 'header a.logo');
            });

            describe('Exists buttons in footer', function() {
                modules.describeDevider();
                modules.checkButtons('Home link', 'footer #index-page');
                modules.checkButtons('Faq link', 'footer #faq-page');
                modules.checkButtons('Contact us link', 'footer #contact-page');
                modules.checkButtons('Privacy Policy link', 'footer #privacy-page');
                modules.checkButtons('Terms & Conditions link', 'footer #terms-page');
            });
        });
};