<?php

namespace Tests\SaleSitesBundle;

use FOS\UserBundle\Model\UserInterface;
use PHPUnit\Framework\MockObject\MockObject;
use SaleSitesBundle\EventListener\ClientListener;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use ThreeWebOneEntityBundle\Entity\User;

/**
 * Class AbstractWebTestCase
 */
abstract class AbstractWebTestCase extends WebTestCase
{
    /**
     * @var MockObject
     */
    private $clientListener;

    /**
     * @return MockObject
     */
    public function getClientListener(): MockObject
    {
        return $this->clientListener;
    }

    /**
     * @return bool
     */
    public function hasClientListener(): bool
    {
        return !is_null($this->clientListener);
    }

    /**
     * Gets a service.
     *
     * @param string $id              The service identifier
     * @param int    $invalidBehavior The behavior when the service does not exist
     *
     * @return object The associated service
     *
     * @throws ServiceCircularReferenceException When a circular reference is detected
     * @throws ServiceNotFoundException          When the service is not defined
     *
     * @see Reference
     */
    public function get($id, $invalidBehavior = ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE)
    {
        $container = static::$kernel->getContainer();

        return $container->get($id, $invalidBehavior);
    }

    /**
     * Simulates that customer logined into the system
     *
     * @return User
     */
    protected function loginCustomer(): User
    {
        $user = new User();
        $user->setRoles([UserInterface::ROLE_DEFAULT]);
        $this->loginUser($user);

        return $user;
    }

    /**
     * Imitates login of the user
     *
     * @param UserInterface $user
     * @return void
     */
    protected function loginUser(UserInterface $user)
    {
        if (is_null($this->clientListener) === true) {
            $mockBuilder = $this->getMockBuilder(ClientListener::class)->disableOriginalConstructor();
            $this->clientListener = $mockBuilder->getMock();
        }

        $this->clientListener->method('getUser')->willReturn($user);
    }

    /**
     * Simulates that admin logined into the system
     *
     * @return User
     */
    protected function loginAdmin(): User
    {
        $user = new User();
        $user->setRoles([UserInterface::ROLE_SUPER_ADMIN]);

        $this->loginUser($user);

        return $user;
    }

    protected function setUp()
    {
        self::bootKernel();
    }
}
