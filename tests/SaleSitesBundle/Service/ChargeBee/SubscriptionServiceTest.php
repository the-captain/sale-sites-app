<?php

namespace Tests\SaleSitesBundle\Service\ChargeBee;

use ArgumentCountError;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use ReflectionClass;
use SaleSitesBundle\EventListener\ClientListener;
use SaleSitesBundle\Service\ChargeBee\SubscriptionService;
use Tests\SaleSitesBundle\AbstractWebTestCase;
use ThreeWebOneEntityBundle\Entity\Billing\Customer;
use ThreeWebOneEntityBundle\Entity\Billing\Subscription;
use ThreeWebOneEntityBundle\Entity\Billing\SubscriptionInterface;

/**
 * Class SubscriptionServiceTest.
 */
class SubscriptionServiceTest extends AbstractWebTestCase
{
    /**
     * checks that service do not instantiates without arguments
     */
    public function testWrongServiceInstantiation()
    {
        $this->expectException(ArgumentCountError::class);
        new SubscriptionService();
    }

    /**
     * checks that instantiation of the service works properly
     */
    public function testNormalServiceInstantiation()
    {
        $this->loginAdmin();

        $saleSitesSubscriptionService = $this->getService();

        $this->assertTrue($saleSitesSubscriptionService->hasClientListener());
        $this->assertInstanceOf(ClientListener::class, $saleSitesSubscriptionService->getClientListener());
    }

    /**
     * @return SubscriptionService
     * @throws \Exception
     */
    protected function getService(): SubscriptionService
    {
        $class = new ReflectionClass(SubscriptionService::class);
        $method = $class->getMethod('initialize');
        $method->setAccessible(true);

        $em = $this->get('doctrine.orm.entity_manager');
        $kernel = $this->get('kernel');

        $subscriptionService = new SubscriptionService($this->getClientListener(), $em, $kernel);

        $method->invoke($subscriptionService);

        return $subscriptionService;
    }

    /**
     * checks that all the setters works properly
     *
     * setted value must be equal to the getted one
     */
    public function testSettersWithOkData()
    {
        $this->loginAdmin();
        $clientListener = $this->getClientListener();
        $em = $this->get('doctrine.orm.entity_manager');
        $kernel = $this->get('kernel');

        $saleSitesSubscriptionService = new SubscriptionService($clientListener, $em, $kernel);

        $saleSitesSubscriptionService
            ->setClientListener($clientListener);

        $this->assertTrue($saleSitesSubscriptionService->hasClientListener());
        $this->assertEquals($clientListener, $saleSitesSubscriptionService->getClientListener());
    }

    /**
     * Ensures that constructor accepts only valid token storage
     */
    public function testConstructorWithWrongCl()
    {
        $this->expectExceptionMessage('ClientListener');
        new SubscriptionService("UNDEFINED STRING");
    }

    /**
     * Ensures that setter accepts only valid token
     */
    public function testSetTokenWithCl()
    {
        $this->loginAdmin();
        $this->expectExceptionMessage('ClientListener');
        $subscriptionService = $this->getService();

        $subscriptionService->setClientListener("UNDEFINED STRING");
    }

    /**
     * Checks restrictions of admin user, of course without subscriptions
     */
    public function testAdminUserRestrictions()
    {
        $this->loginAdmin();
        $saleSitesSubscriptionService = $this->getService();

        $this->assertTrue($saleSitesSubscriptionService->isBuySubscribed());
        $this->assertTrue($saleSitesSubscriptionService->isSellSubscribed());
        $this->assertTrue($saleSitesSubscriptionService->isRepairSubscribed());
        $this->assertEquals(3, $saleSitesSubscriptionService->getSubscribedItemsCount());
    }

    /**
     * Checks restrictions for usual customer without subscriptions
     */
    public function testCustomerUserRestrictions()
    {
        $this->loginCustomer();
        $saleSitesSubscriptionService = $this->getService();

        $this->assertFalse($saleSitesSubscriptionService->isSellSubscribed());
        $this->assertFalse($saleSitesSubscriptionService->isRepairSubscribed());
    }

    /**
     * Checks behavior of subscription with three sites
     *
     * @throws \Exception
     */
    public function testCustomerUserWithSubscriptionInTrialWithAll()
    {
        $user = $this->loginCustomer();

        $customer = new Customer();

        $subscription = new Subscription();

        $subscription
            ->setStatus(SubscriptionInterface::STATUS_TRIAL)
            ->setCustomer($customer)
            ->setItems(
                [
                    SubscriptionInterface::ITEM_SELL => true,
                    SubscriptionInterface::ITEM_REPAIR => true,
                    SubscriptionInterface::ITEM_BUY => true,
                ]
            );
        $subscriptions = new ArrayCollection();
        $subscriptions->add($subscription);

        $trialEndDate = new DateTime();
        $trialEndDate->add(new \DateInterval('PT1H'));
        $customer
            ->setTrialEndedAt($trialEndDate)
            ->setSubscriptions($subscriptions);

        $user->setBillingCustomer($customer);

        $saleSitesSubscriptionService = $this->getService();


        $this->assertTrue($saleSitesSubscriptionService->isSellSubscribed());
        $this->assertTrue($saleSitesSubscriptionService->isBuySubscribed());
        $this->assertTrue($saleSitesSubscriptionService->isRepairSubscribed());
        $this->assertEquals(3, $saleSitesSubscriptionService->getSubscribedItemsCount());
    }

    /**
     * Checks behavior of subscription with two sites
     *
     * @throws \Exception
     */
    public function testCustomerUserWithSubscriptionInTrialWithTwo()
    {
        $user = $this->loginCustomer();

        $customer = new Customer();

        $subscription = new Subscription();

        $subscription
            ->setStatus(SubscriptionInterface::STATUS_TRIAL)
            ->setCustomer($customer)
            ->setItems(
                [
                    SubscriptionInterface::ITEM_SELL => true,
                    SubscriptionInterface::ITEM_REPAIR => true,
                    SubscriptionInterface::ITEM_BUY => false,
                ]
            );
        $subscriptions = new ArrayCollection();
        $subscriptions->add($subscription);

        $trialEndDate = new DateTime();
        $trialEndDate->add(new \DateInterval('PT1H'));
        $customer
            ->setTrialEndedAt($trialEndDate)
            ->setSubscriptions($subscriptions);

        $user->setBillingCustomer($customer);

        $saleSitesSubscriptionService = $this->getService();

        $this->assertTrue($saleSitesSubscriptionService->isSellSubscribed());
        $this->assertFalse($saleSitesSubscriptionService->isBuySubscribed());
        $this->assertTrue($saleSitesSubscriptionService->isRepairSubscribed());
        $this->assertEquals(2, $saleSitesSubscriptionService->getSubscribedItemsCount());
    }

    /**
     * Checks behavior of subscription with one site
     *
     * @throws \Exception
     */
    public function testCustomerUserWithSubscriptionInTrialWithOne()
    {
        $user = $this->loginCustomer();

        $customer = new Customer();

        $subscription = new Subscription();

        $subscription
            ->setStatus(SubscriptionInterface::STATUS_TRIAL)
            ->setCustomer($customer)
            ->setItems(
                [
                    SubscriptionInterface::ITEM_SELL => false,
                    SubscriptionInterface::ITEM_REPAIR => true,
                    SubscriptionInterface::ITEM_BUY => false,
                ]
            );
        $subscriptions = new ArrayCollection();
        $subscriptions->add($subscription);

        $trialEndDate = new DateTime();
        $trialEndDate->add(new \DateInterval('PT1H'));
        $customer
            ->setTrialEndedAt($trialEndDate)
            ->setSubscriptions($subscriptions);

        $user->setBillingCustomer($customer);

        $saleSitesSubscriptionService = $this->getService();

        $this->assertFalse($saleSitesSubscriptionService->isSellSubscribed());
        $this->assertFalse($saleSitesSubscriptionService->isBuySubscribed());
        $this->assertTrue($saleSitesSubscriptionService->isRepairSubscribed());
        $this->assertEquals(1, $saleSitesSubscriptionService->getSubscribedItemsCount());
    }

    /**
     * Checks that customer can't receive service from trial period if it passed out
     */
    public function testCustomerUserWithSubscriptionInTrialWhenTrialIsExpiredForCustomer()
    {
        $user = $this->loginCustomer();
        $customer = new Customer();
        $subscription = new Subscription();

        $subscription
            ->setStatus(SubscriptionInterface::STATUS_TRIAL)
            ->setCustomer($customer)
            ->setItems(
                [
                    SubscriptionInterface::ITEM_SELL => true,
                    SubscriptionInterface::ITEM_REPAIR => true,
                    SubscriptionInterface::ITEM_BUY => true,
                ]
            );

        $subscriptions = new ArrayCollection();
        $subscriptions->add($subscription);

        $trialEndDate = new DateTime(); //right now date, must be expired
        $customer
            ->setTrialEndedAt($trialEndDate)
            ->setSubscriptions($subscriptions);

        $user->setBillingCustomer($customer);

        $saleSitesSubscriptionService = $this->getService();

        $this->assertFalse($saleSitesSubscriptionService->isSellSubscribed());
        $this->assertFalse($saleSitesSubscriptionService->isBuySubscribed());
        $this->assertFalse($saleSitesSubscriptionService->isRepairSubscribed());
    }
}
