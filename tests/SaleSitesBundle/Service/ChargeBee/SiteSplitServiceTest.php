<?php

namespace SaleSitesBundle\Tests\Service\ChargeBee;

use DateInterval;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use ReflectionClass;
use SaleSitesBundle\Service\ChargeBee\AccessMatrixInterface;
use SaleSitesBundle\Service\ChargeBee\SiteSplitService;
use SaleSitesBundle\Service\ChargeBee\SubscriptionService;
use Symfony\Component\Yaml\Yaml;
use Tests\SaleSitesBundle\AbstractWebTestCase;
use ThreeWebOneEntityBundle\Entity\Billing\Customer;
use ThreeWebOneEntityBundle\Entity\Billing\Subscription;
use ThreeWebOneEntityBundle\Entity\Billing\SubscriptionInterface;

/**
 * Class SiteSplitServiceTest.
 */
class SiteSplitServiceTest extends AbstractWebTestCase
{
    public function testWrongServiceInstantiation()
    {
        $this->expectException(\ArgumentCountError::class);

        new SiteSplitService();
    }

    public function testNormalServiceInstantiation()
    {
        $this->loginAdmin();

        $siteSplitService = $this->getSiteSplitService();

        foreach (SiteSplitService::RESTRICTIONS as $RESTRICTION) {
            $this->assertTrue($siteSplitService->isAccessible($RESTRICTION));
        }
    }

    /**
     * @return SiteSplitService
     * @throws \Exception
     * @throws \ReflectionException
     */
    protected function getSiteSplitService(): SiteSplitService
    {
        $class = new ReflectionClass(SubscriptionService::class);
        $method = $class->getMethod('initialize');
        $method->setAccessible(true);

        $kernel = $this->get('kernel');
        $configPath = $kernel->locateResource('@SaleSitesBundle/Resources/config/split/permissions.yml');
        $rules = Yaml::parse(file_get_contents($configPath))['parameters']['site_split_rules'];
        $em = $this->get('doctrine.orm.entity_manager');
        $kernel = $this->get('kernel');

        $subscriptionService = new SubscriptionService($this->getClientListener(), $em, $kernel);

        $method->invoke($subscriptionService);

        return new SiteSplitService($subscriptionService, $rules);
    }

    public function testConstructorAcceptsOnlyValidVariable()
    {
        $this->expectExceptionMessage('SubscriptionService');
        new SiteSplitService(new DateTime());
    }

    public function testCaseWhenNothingSubscribed()
    {
        $siteSplitService = $this->getSiteSplitServiceWithItems(
            [
                SubscriptionInterface::ITEM_SELL => false,
                SubscriptionInterface::ITEM_REPAIR => false,
                SubscriptionInterface::ITEM_BUY => false,
            ]
        );

        foreach (SiteSplitService::RESTRICTIONS as $RESTRICTION) {
            $this->assertFalse($siteSplitService->isAccessible($RESTRICTION));
        }
    }

    protected function getSiteSplitServiceWithItems(array $items): SiteSplitService
    {
        $user = $this->loginCustomer();

        $customer = new Customer();

        $subscription = new Subscription();

        $subscription
            ->setStatus(SubscriptionInterface::STATUS_TRIAL)
            ->setCustomer($customer)
            ->setItems($items);
        $subscriptions = new ArrayCollection();
        $subscriptions->add($subscription);

        $trialEndDate = new DateTime();
        $trialEndDate->add(new DateInterval('PT1H'));
        $customer
            ->setTrialEndedAt($trialEndDate)
            ->setSubscriptions($subscriptions);

        $user->setBillingCustomer($customer);

        $subscriptionServiceReflection = new ReflectionClass(SubscriptionService::class);
        $method = $subscriptionServiceReflection->getMethod('initialize');
        $method->setAccessible(true);

        $kernel = $this->get('kernel');
        $configPath = $kernel->locateResource('@SaleSitesBundle/Resources/config/split/permissions.yml');
        $rules = Yaml::parse(file_get_contents($configPath))['parameters']['site_split_rules'];

        $em = $this->get('doctrine.orm.entity_manager');
        $kernel = $this->get('kernel');
        $subscriptionService = new SubscriptionService($this->getClientListener(), $em, $kernel);

        $method->invoke($subscriptionService);

        return new SiteSplitService($subscriptionService, $rules);
    }

    public function testCaseWhenOnlyRepairSubscribed()
    {
        $siteSplitService = $this->getSiteSplitServiceWithItems(
            [
                SubscriptionInterface::ITEM_SELL => false,
                SubscriptionInterface::ITEM_REPAIR => true,
                SubscriptionInterface::ITEM_BUY => false,
            ]
        );

        //landing
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::MULTI_WEBSITE_LANDING_PAGE));

        //landing title
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::MULTI_WEBSITE_LANDING_TITLE));

        //title&description
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_SALE));

        //slider
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_SALE));

        //offers
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::OFFERS));

        //social
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SOCIAL));

        //seo
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SEO_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SEO_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SEO_SALE));

        //inventory
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::INVENTORY));

        //inventory website
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::INVENTORY_WEBSITE_LISTENING_FORM));

        //order
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::ORDER_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::ORDER_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::ORDER_SALE));

        //website
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::WEBSITE_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::WEBSITE_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::WEBSITE_SALE));
    }

    public function testCaseWhenOnlyBuySubscribed()
    {
        $siteSplitService = $this->getSiteSplitServiceWithItems(
            [
                SubscriptionInterface::ITEM_SELL => false,
                SubscriptionInterface::ITEM_REPAIR => false,
                SubscriptionInterface::ITEM_BUY => true,
            ]
        );

        //landing
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::MULTI_WEBSITE_LANDING_PAGE));

        //landing title
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::MULTI_WEBSITE_LANDING_TITLE));

        //title&description
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_SALE));

        //slider
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_SALE));

        //offers
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::OFFERS));

        //social
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SOCIAL));

        //seo
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SEO_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SEO_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SEO_SALE));

        //inventory
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::INVENTORY));

        //inventory website
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::INVENTORY_WEBSITE_LISTENING_FORM));

        //order
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::ORDER_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::ORDER_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::ORDER_SALE));

        //website
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::WEBSITE_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::WEBSITE_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::WEBSITE_SALE));
    }

    public function testCaseWhenOnlySellSubscribed()
    {
        $siteSplitService = $this->getSiteSplitServiceWithItems(
            [
                SubscriptionInterface::ITEM_SELL => true,
                SubscriptionInterface::ITEM_REPAIR => false,
                SubscriptionInterface::ITEM_BUY => false,
            ]
        );

        //landing
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::MULTI_WEBSITE_LANDING_PAGE));

        //landing title
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::MULTI_WEBSITE_LANDING_TITLE));

        //title&description
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_SALE));

        //slider
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_SALE));

        //offers
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::OFFERS));

        //social
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SOCIAL));

        //seo
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SEO_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SEO_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SEO_SALE));

        //inventory
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::INVENTORY));

        //inventory website
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::INVENTORY_WEBSITE_LISTENING_FORM));

        //order
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::ORDER_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::ORDER_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::ORDER_SALE));

        //website
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::WEBSITE_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::WEBSITE_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::WEBSITE_SALE));
    }

    public function testCaseWhenSellRepairSubscribed()
    {
        $siteSplitService = $this->getSiteSplitServiceWithItems(
            [
                SubscriptionInterface::ITEM_SELL => true,
                SubscriptionInterface::ITEM_REPAIR => true,
                SubscriptionInterface::ITEM_BUY => false,
            ]
        );

        //landing
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::MULTI_WEBSITE_LANDING_PAGE));

        //landing title
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::MULTI_WEBSITE_LANDING_TITLE));

        //title&description
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_SALE));

        //slider
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_SALE));

        //offers
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::OFFERS));

        //social
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SOCIAL));

        //seo
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SEO_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SEO_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SEO_SALE));

        //inventory
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::INVENTORY));

        //inventory website
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::INVENTORY_WEBSITE_LISTENING_FORM));

        //order
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::ORDER_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::ORDER_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::ORDER_SALE));

        //website
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::WEBSITE_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::WEBSITE_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::WEBSITE_SALE));
    }

    public function testCaseWhenEverythingSubscribed()
    {
        $siteSplitService = $this->getSiteSplitServiceWithItems(
            [
                SubscriptionInterface::ITEM_SELL => true,
                SubscriptionInterface::ITEM_REPAIR => true,
                SubscriptionInterface::ITEM_BUY => true,
            ]
        );

        foreach (SiteSplitService::RESTRICTIONS as $RESTRICTION) {
            $this->assertTrue($siteSplitService->isAccessible($RESTRICTION));
        }
    }

    public function testCaseWhenBuyRepairSubscribed()
    {
        $siteSplitService = $this->getSiteSplitServiceWithItems(
            [
                SubscriptionInterface::ITEM_SELL => false,
                SubscriptionInterface::ITEM_REPAIR => true,
                SubscriptionInterface::ITEM_BUY => true,
            ]
        );

        //landing
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::MULTI_WEBSITE_LANDING_PAGE));

        //landing title
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::MULTI_WEBSITE_LANDING_TITLE));

        //title&description
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_SALE));

        //slider
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_SALE));

        //offers
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::OFFERS));

        //social
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SOCIAL));

        //seo
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SEO_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SEO_REPAIR));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SEO_SALE));

        //inventory
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::INVENTORY));

        //inventory website
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::INVENTORY_WEBSITE_LISTENING_FORM));

        //order
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::ORDER_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::ORDER_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::ORDER_SALE));

        //website
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::WEBSITE_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::WEBSITE_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::WEBSITE_SALE));
    }

    public function testCaseWhenSellBuySubscribed()
    {
        $siteSplitService = $this->getSiteSplitServiceWithItems(
            [
                SubscriptionInterface::ITEM_SELL => true,
                SubscriptionInterface::ITEM_REPAIR => false,
                SubscriptionInterface::ITEM_BUY => true,
            ]
        );

        //landing
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::MULTI_WEBSITE_LANDING_PAGE));

        //landing title
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::MULTI_WEBSITE_LANDING_TITLE));

        //title&description
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::TITLE_DESCRIPTION_SALE));

        //slider
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SLIDER_DISPLAY_SALE));

        //offers
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::OFFERS));

        //social
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SOCIAL));

        //seo
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SEO_BUY));
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::SEO_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::SEO_SALE));

        //inventory
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::INVENTORY));

        //inventory website
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::INVENTORY_WEBSITE_LISTENING_FORM));

        //order
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::ORDER_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::ORDER_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::ORDER_SALE));

        //website
        $this->assertFalse($siteSplitService->isAccessible(SiteSplitService::WEBSITE_REPAIR));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::WEBSITE_BUY));
        $this->assertTrue($siteSplitService->isAccessible(SiteSplitService::WEBSITE_SALE));
    }
}
