<?php

namespace Tests\PaypalExpressBundle\Service;

use PaypalExpressBundle\Packet\Payment\Create;
use PaypalExpressBundle\Service\PaypalExpressService;
use Symfony\Component\Validator\Exception\ValidatorException;
use Tests\PaypalExpressBundle\AbstractWebTestCase;

class PaymentExpressServiceTest extends AbstractWebTestCase
{
    public function testSanity()
    {
        $s = $this->getService();

        $this->assertTrue($s->isEnabled());
    }

    /**
     * @return PaypalExpressService
     */
    protected function getService(): PaypalExpressService
    {
        return $this->get('paypal_express_service');
    }

    public function testGetAuthToken()
    {
        $s = $this->getService();

        $this->assertNotEmpty($s->getAccessTocken());
    }

    public function testCreatePayment()
    {
        $this->expectException(ValidatorException::class);
        $s = $this->getService();

        $createCommand = new Create();

        $s->createPayment($createCommand);
    }
}
