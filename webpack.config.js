var Encore = require('@symfony/webpack-encore');
var sites = ['buy', 'repair', 'sell'];
var mainColors = ['blue', 'green', 'orange', 'red'];
var colors = ['blue', 'green', 'light-blue', 'light-green', 'light-orange', 'light-red',
    'medium-blue', 'medium-green', 'medium-orange', 'medium-red', 'orange', 'red'];
var fonts = ['crillee_regular', 'furore_regular', 'Helvetica round', 'bitter_regular',
    'raleway_regular', 'slicker_regular', 'myriad_pro_regular', 'virtue_regular',
    'orbitron_regular', 'gayatri_regular', 'duke_regular', 'montserrat_regular',
    'neo_sans_bold', 'charlie_brown_m54_regular', 'quicksand_regular', 'Arial narrow'];

// ANGULAR JS APP
Encore
    .setOutputPath('web/sites/build')
    .setPublicPath('/sites/build')
    .setManifestKeyPrefix('build/')
    .cleanupOutputBeforeBuild()
    .addEntry('app', [
        './src/SaleSitesBundle/Resources/public/app/app.js',
        './src/SaleSitesBundle/Resources/public/app/controllers/header-cart.controller.js',
        './src/SaleSitesBundle/Resources/public/app/factories/request.factory.js',
        './src/SaleSitesBundle/Resources/public/app/services/router-helper.service.js',
        './src/SaleSitesBundle/Resources/public/app/services/site-context-helper.service.js',
        './src/SaleSitesBundle/Resources/public/app/services/cart.service.js',
        './src/SaleSitesBundle/Resources/public/app/controllers/message.controller.js',
        './src/SaleSitesBundle/Resources/public/app/directives/message.directive.js',
        './src/SaleSitesBundle/Resources/public/app/services/message.service.js'
    ])
    .addEntry('app.cart.controller', [
        './src/SaleSitesBundle/Resources/public/app/controllers/cart.controller.js'
    ])
    .addEntry('app.checkout.controller', [
        './src/SaleSitesBundle/Resources/public/app/controllers/checkout.controller.js'
    ])
    .addEntry('em-zoom', [
        './node_modules/ez-plus/src/jquery.ez-plus.js',
        './src/SaleSitesBundle/Resources/public/js/zoom.js'
    ])
    .addEntry('app.cart-forms.controller', [
        './src/SaleSitesBundle/Resources/public/app/controllers/cart-forms.controller.js'
    ])
    .addEntry('app.contacts.controller', [
        './src/SaleSitesBundle/Resources/public/app/controllers/contacts.controller.js'
    ])
    .addEntry('app.selling-category', [
        './src/SaleSitesBundle/Resources/public/app/services/pager.service.js',
        './src/SaleSitesBundle/Resources/public/app/factories/selling-category-request.factory.js',
        './src/SaleSitesBundle/Resources/public/app/controllers/selling-category.controller.js'
    ])
    .addEntry(
        'paypal-express',
        [
            './src/PaypalExpressBundle/Resources/public/js/paypal-express.js',
            './src/PaypalExpressBundle/Resources/public/js/components/paypal-express-button.component.js'
        ]
    )
    .enableSourceMaps(!Encore.isProduction());


// SCSS
Encore
    .addStyleEntry('css/register', [
        './src/SaleSitesBundle/Resources/public/scss/register/register.scss'
    ])
    .addStyleEntry('css/login', [
        './src/SaleSitesBundle/Resources/public/scss/login/login.scss'
    ])
    .addStyleEntry('css/reset', [
        './src/SaleSitesBundle/Resources/public/scss/reset/reset.scss'
    ])
    .addStyleEntry('css/profile', [
        './src/SaleSitesBundle/Resources/public/scss/profile/profile.scss'
    ])
    .addStyleEntry('css/media-queries/index', [
        './src/SaleSitesBundle/Resources/public/scss/media-queries/index.scss'
    ])
    .addStyleEntry('css/media-queries/buy-page', [
        './src/SaleSitesBundle/Resources/public/scss/media-queries/buy-page.scss'
    ])
    .addStyleEntry('css/media-queries/sell-repair', [
        './src/SaleSitesBundle/Resources/public/scss/media-queries/sell-repair.scss'
    ])
    .addStyleEntry('css/media-queries/common', [
        './src/SaleSitesBundle/Resources/public/scss/media-queries/common.scss'
    ]);

// CUSTOM THEME STYLES
sites.forEach(function (site) {
    colors.forEach(function (color) {
        Encore
            .addStyleEntry(
                'css/theme/' + site + '-' + color,
                './src/SaleSitesBundle/Resources/public/css/theme/' + site + '/' + color + '.scss'
            )
    })
});

mainColors.forEach(function (color) {
    Encore
        .addStyleEntry(
            'css/theme/' + color,
            './src/SaleSitesBundle/Resources/public/css/theme/' + color + '.scss'
        )
});

fonts.forEach(function (font) {
    Encore
        .addStyleEntry(
            'css/font/' + font,
            './src/SaleSitesBundle/Resources/public/css/fonts/' + font + '.css'
        )
});

// CSS
Encore
    .addStyleEntry('css/layout',
        [
            './src/SaleSitesBundle/Resources/public/css/header.css',
            './src/SaleSitesBundle/Resources/public/css/common.css',
            './src/SaleSitesBundle/Resources/public/css/footer.css',
            './src/SaleSitesBundle/Resources/public/css/color.css'
        ]
    )
    .addStyleEntry('css/index',
        [
            './src/SaleSitesBundle/Resources/public/css/index.css'
        ]
    )
    .addStyleEntry('css/sell-list',
        [
            './src/SaleSitesBundle/Resources/public/css/buy-list.css'
        ]
    )
    .addStyleEntry('css/sites-index',
        [
            './src/SaleSitesBundle/Resources/public/css/buy-sell-repair.css',
            './src/SaleSitesBundle/Resources/public/css/brand.css'
        ]
    )
    .addStyleEntry('css/buying-page',
        [
            './src/SaleSitesBundle/Resources/public/css/buy-sell-repair.css',
            './src/SaleSitesBundle/Resources/public/css/brand.css'
        ]
    )
    .addStyleEntry('css/product',
        [
            './src/SaleSitesBundle/Resources/public/css/item.css'
        ]
    )
    .addStyleEntry('css/buy-product',
        [
            './src/SaleSitesBundle/Resources/public/css/sell-item.css'
        ]
    )
    .addStyleEntry('css/checkout',
        [
            './src/SaleSitesBundle/Resources/public/css/checkout.css'
        ]
    )
    .addStyleEntry('css/repair-product',
        [
            './src/SaleSitesBundle/Resources/public/css/sell-item.css',
            './src/SaleSitesBundle/Resources/public/css/repair-item.css'
        ]
    )
    .addStyleEntry('css/sell-index',
        [
            './src/SaleSitesBundle/Resources/public/css/buy-sell-repair.css'
        ]
    )
    .addStyleEntry('css/contacts',
        [
            './src/SaleSitesBundle/Resources/public/css/contact.css'
        ]
    )
    .addStyleEntry('css/faq-page',
        [
            './src/SaleSitesBundle/Resources/public/css/faq.css'
        ]
    )
;

// VANILLA JS / JQUERY / JS LIBS
Encore
    .addEntry(
        'selling-items-list',
        [
            './src/SaleSitesBundle/Resources/public/js/script.js',
        ]
    )
    .addEntry(
        'checkout-page',
        [
            './src/SaleSitesBundle/Resources/public/js/script.js',
        ]
    )
    .addEntry(
        'buying-index',
        [
            './src/SaleSitesBundle/Resources/public/js/buy-index.js',
        ]
    )
    .addEntry(
        'buy-product',
        [
            './src/SaleSitesBundle/Resources/public/js/product.js',
        ]
    )
    .addEntry(
        'repair-product',
        [
            './src/SaleSitesBundle/Resources/public/js/product.js',
        ]
    )
    .addEntry(
        'contact-page',
        [
            './src/SaleSitesBundle/Resources/public/js/faq.js',
        ]
    )
    .addEntry(
        'faq-page',
        [
            './src/SaleSitesBundle/Resources/public/js/faq.js',
        ]
    )
    .addEntry(
        'sell-page',
        [
            './src/SaleSitesBundle/Resources/public/js/sell-index.js',
        ]
    )
;

// ENCORE MODULES
Encore
    .enableSassLoader()
    .autoProvidejQuery()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning()
;

// ENCORE SET CDN FOR PROD ENV
 if (Encore.isProduction()) {
     Encore.setPublicPath('https://s.3w1.website/sites/build');
}

// export the final configuration
module.exports = Encore.getWebpackConfig();
