<?php

namespace SaleSitesBundle\Security;

use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Security\UserProvider;
use ThreeWebOneEntityBundle\Entity\Customer\Customer;
use SaleSitesBundle\EventListener\ClientSubscriber;

class ShopEmailUserProvider extends UserProvider
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var ClientSubscriber
     */
    private $client;

    /**
     * Constructor.
     *
     * @param UserManagerInterface $userManager
     * @param EntityManager $entityManager
     * @param ClientSubscriber $client
     */
    public function __construct(
        UserManagerInterface $userManager,
        EntityManager $entityManager,
        ClientSubscriber $client)
    {
        parent::__construct($userManager);
        $this->entityManager = $entityManager;
        $this->client = $client;
    }

    /**
     * {@inheritdoc}
     */
    protected function findUser($username)
    {
        return $this->entityManager->getRepository(Customer::class)->getCustomerByEmailAndOwnerId($username, $this->client->getUserId());
    }
}
