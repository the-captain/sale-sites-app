<?php

namespace SaleSitesBundle\Twig;

use Doctrine\ORM\EntityManager;
use SaleSitesBundle\EventListener\ClientSubscriber;
use SaleSitesBundle\Service\ChargeBee\SiteSplitService;
use SaleSitesBundle\Service\ChargeBee\SubscriptionService;
use ThreeWebOneEntityBundle\Entity\PopularSearch;

/**
 * Site-related Twig functions
 */
class Site extends \Twig_Extension
{
    /**
     * @var ClientSubscriber
     */
    private $client;

    /**
     * @var SubscriptionService
     */
    private $siteSplitService;

    /**
     * Site constructor.
     *
     * @param ClientSubscriber   $client
     * @param SiteSplitService $siteSplitService
     */
    public function __construct(ClientSubscriber $client, SiteSplitService $siteSplitService)
    {
        $this->client = $client;
        $this->siteSplitService = $siteSplitService;
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'client';
    }

    /**
     * {@inheritDoc}
     */
    public function getFunctions()
    {
        return [
            'site_path' => new \Twig_SimpleFunction(
                'site_path',
                [$this->client, 'getPath']
            ),
            'site_user' => new \Twig_SimpleFunction(
                'site_user',
                [$this->client, 'getUser']
            ),
            'site_config' => new \Twig_SimpleFunction(
                'site_config',
                [$this->client, 'getConfig']
            ),
            'site_testimonials' => new \Twig_SimpleFunction(
                'site_testimonials',
                [$this->client, 'getTestimonials']
            ),
            'site_seo' => new \Twig_SimpleFunction(
                'site_seo',
                [$this->client, 'getSeoMeta']
            ),
            'site_location_main' => new \Twig_SimpleFunction(
                'site_location_main',
                [$this->client, 'getSiteMainLocation']
            ),
            'is_sell_subscribed' => new \Twig_SimpleFunction(
                'is_sell_subscribed',
                [$this, 'isSiteFeatureSaleGranted']
            ),
            'is_buy_subscribed' => new \Twig_SimpleFunction(
                'is_buy_subscribed',
                [$this, 'isSiteFeatureBuyGranted']
            ),
            'is_repair_subscribed' => new \Twig_SimpleFunction(
                'is_repair_subscribed',
                [$this, 'isSiteFeatureRepairGranted']
            ),
            'get_subscribed_items_count' => new \Twig_SimpleFunction(
                'get_subscribed_items_count',
                [$this->siteSplitService, 'getSubscribedItemsCount']
            ),
            'get_logo_size' => new \Twig_SimpleFunction(
                'get_logo_size',
                [$this->client, 'getLogoSize']
            ),
            'get_popular_searches' => new \Twig_SimpleFunction(
                'get_popular_searches',
                [$this->client, 'getPopularSearches']
            ),
            'get_first_theme_name_for_two_sites' => new \Twig_SimpleFunction(
                'get_first_theme_name_for_two_sites',
                [$this, 'getFirstThemeNameForTwoSubscribedSites']
            ),
            'get_second_theme_name_for_two_sites' => new \Twig_SimpleFunction(
                'get_second_theme_name_for_two_sites',
                [$this, 'getSecondThemeNameForTwoSubscribedSites']
            ),
            'get_single_theme_name' => new \Twig_SimpleFunction(
                'get_single_theme_name',
                [$this, 'getSingleThemeName']
            ),
        ];
    }

    /**
     * @return bool
     */
    public function isSiteFeatureSaleGranted(): bool
    {
        return $this->siteSplitService->isAccessible(SiteSplitService::WEBSITE_SALE);
    }

    /**
     * @return bool
     */
    public function isSiteFeatureBuyGranted(): bool
    {
        return $this->siteSplitService->isAccessible(SiteSplitService::WEBSITE_BUY);
    }

    /**
     * @return bool
     */
    public function isSiteFeatureRepairGranted(): bool
    {
        return $this->siteSplitService->isAccessible(SiteSplitService::WEBSITE_REPAIR);
    }

    /**
     * @return string
     */
    public function getFirstThemeNameForTwoSubscribedSites(): string
    {
        if ($this->isSiteFeatureSaleGranted()) {
            return 'buy';
        }

        return 'sell';
    }

    /**
     * @return string
     */
    public function getSecondThemeNameForTwoSubscribedSites(): string
    {
        if ($this->isSiteFeatureBuyGranted()) {
            return 'sell';
        }

        return 'repair';
    }

    /**
     * @return string
     */
    public function getSingleThemeName(): string
    {
        if ($this->isSiteFeatureSaleGranted()) {
            return 'buy';
        }

        if ($this->isSiteFeatureBuyGranted()) {
            return 'sell';
        }

        return 'repair';
    }
}
