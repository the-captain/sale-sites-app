<?php

namespace SaleSitesBundle\Twig;

class QueryParamsFilterExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('queryParamFilter', [$this, 'queryParamFilter']),
        ];
    }

    /**
     * Edit query params based on value
     *
     * @param array $queryParam
     * @param $section
     * @param $value
     *
     * @return array
     */
    public function queryParamFilter(array $queryParam, $section, $value): array
    {
        if ($section != 'sorting') {
            if (array_key_exists($section, $queryParam)) {
                if (!is_array($queryParam[$section])) {
                    $queryParam[$section] = [];
                }
                $key = array_search($value, $queryParam[$section]);
                if ($key !== false) {
                    unset($queryParam[$section][$key]);
                } else {
                    $queryParam[$section][] = $value;
                }
            } else {
                $queryParam[$section][] = $value;
            }
        } else {
            if (in_array($value, ['ASC', 'DESC'])) {
                $queryParam[$section] = $value;
            } else {
                if (array_key_exists($section, $queryParam)) {
                    unset($queryParam[$section]);
                }
            }
        }

        //set page to first on filtering
        if (array_key_exists('page', $queryParam)) {
            unset($queryParam['page']);
        }

        return $queryParam;
    }
}
