<?php

namespace SaleSitesBundle\Twig;

class PriceExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('format_price', [$this, 'formatPrice']),
        ];
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function formatPrice($value)
    {
        return $value/100;
    }
}
