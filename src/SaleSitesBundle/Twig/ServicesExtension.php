<?php

namespace SaleSitesBundle\Twig;

use ThreeWebOneEntityBundle\Entity\UserConfig\Location;

class ServicesExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('showServices', [$this, 'showServices']),
        ];
    }

    /**
     * @param $services
     *
     * @return mixed
     */
    public function showServices($services) : string
    {
        $result = '';
        foreach ($services as $item) {
            if ($item == Location::SALE) {
                $result .= 'Sale';
            } elseif ($item == Location::BUYBACK) {
                $result .= 'Buy';
            } elseif ($item == Location::REPAIR) {
                $result .= 'Repair';
            }
            if ($item != end($services)) {
                $result .= ', ';
            }
        }

        return $result;
    }
}
