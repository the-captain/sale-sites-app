<?php

namespace SaleSitesBundle\Twig;

use ThreeWebOneEntityBundle\Entity\Helper\SiteTypeInterface;
use ThreeWebOneEntityBundle\Entity\Order\OrderStatusInterface;
use Symfony\Component\Translation\TranslatorInterface;

class OrderInfoExtension extends \Twig_Extension
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * OrderInfoExtension constructor.
     *
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * {@inheritDoc}
     */
    public function getFunctions()
    {
        return [
            'get_site_type_name' => new \Twig_SimpleFunction(
                'get_site_type_name',
                [$this, 'getSiteTypeName']
            ),
            'get_status_name' => new \Twig_SimpleFunction(
                'get_status_name',
                [$this, 'getStatusName']
            ),
        ];
    }

    /**
     * Get SiteType Name
     *
     * @param int $siteType
     * @return String
     */
    public function getSiteTypeName(int $siteType): String
    {
        if ($siteType === SiteTypeInterface::SALE) {
            return $this->translator->trans('buy order');
        }

        if ($siteType === SiteTypeInterface::BUYBACK) {
            return $this->translator->trans('sell order');
        }

        if ($siteType === SiteTypeInterface::REPAIR) {
            return $this->translator->trans('repair order');
        }

        return '';
    }

    /**
     * Get Status Name
     *
     * @param int $status
     * @return String
     */
    public function getStatusName(int $status): String
    {
        if ($status === OrderStatusInterface::NEW_ORDER) {
            return $this->translator->trans('new order');
        }

        if ($status === OrderStatusInterface::IN_PROGRESS) {
            return $this->translator->trans('in progress');
        }

        if ($status === OrderStatusInterface::COMPLETED) {
            return $this->translator->trans('completed');
        }

        if ($status === OrderStatusInterface::REJECTED) {
            return $this->translator->trans('rejected');
        }

        return '';
    }
}
