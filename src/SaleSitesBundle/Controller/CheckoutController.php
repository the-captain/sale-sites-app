<?php

namespace SaleSitesBundle\Controller;

use PaypalExpressBundle\Service\PaypalExpressService;
use SaleSitesBundle\Form\Type\OrderType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use ThreeWebOneEntityBundle\Entity\Customer\Customer;
use ThreeWebOneEntityBundle\Entity\Helper\SiteTypeInterface;
use ThreeWebOneEntityBundle\Entity\Order\Order;

class CheckoutController extends BaseController
{
    const SITE_TYPES = [
        'sell' => SiteTypeInterface::SALE,
        'buyback' => SiteTypeInterface::BUYBACK,
        'repair' => SiteTypeInterface::REPAIR,
    ];

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function checkoutAction()
    {
        /** @var PaypalExpressService $ppExpress */
        $ppExpress = $this->get('paypal_express_service');

        $sellOrder = $this->createForm(OrderType::class, new Order(), ['site_type' => SiteTypeInterface::SALE, 'user' => $this->getUser()]);
        $buyBackOrder = $this->createForm(OrderType::class, new Order(), ['site_type' => SiteTypeInterface::BUYBACK, 'user' => $this->getUser()]);
        $repairOrder = $this->createForm(OrderType::class, new Order(), ['site_type' => SiteTypeInterface::REPAIR, 'user' => $this->getUser()]);

        return $this->render(
            'SaleSitesBundle:Checkout:checkout.html.twig',
            [
                'is_paypal_express_enabled' => $ppExpress->isEnabled(),
                'paypal_express_environment' => $ppExpress->getEnvironment(),
                'sell_order' => $sellOrder->createView(),
                'buyback_order' => $buyBackOrder->createView(),
                'repair_order' => $repairOrder->createView(),
            ]
        );
    }

    /**
     * @param Request $request
     * @param $site
     * @return RedirectResponse
     */
    public function checkoutSiteCompleteAction(Request $request, $site)
    {
        $data = $this->get('sale_sites.cart.items')->getSiteCartItems(self::SITE_TYPES[$site]);
        $owner = $this->get('sale_sites.site_subscriber')->getUser();

        if (empty($data['items'])) {
            $this->addFlash('order_fail', 'Your have no items to make ' . $site . ' site order');
            $this->redirectToRoute(
                'sale_sites_checkout_page',
                ['userId' => $owner->getId()]
            );
        }

        $owner = $this->container->get('sale_sites.site_subscriber')->getUser();
        $order = (new Order())
            ->setOwner($owner);

        $sellOrderForm = $this->createForm(OrderType::class, $order);

        $sellOrderForm->handleRequest($request);

        if ($sellOrderForm->isValid() && $sellOrderForm->isSubmitted()) {

            $customerService = $this->get('sale_sites.service.customer');
            if (
                !($this->getUser() instanceof Customer) &&
                $customerService->customerExist($order->getEmail(), $owner->getId())
            ) {
                return $this->redirectToLogin();
            }

            $shippingAddress = $sellOrderForm->get('address1')->getData() . '; ';
            $shippingAddress .= $sellOrderForm->get('address2')->getData() . '; ';
            $shippingAddress .= $sellOrderForm->get('city')->getData() . '; ';
            $shippingAddress .= $sellOrderForm->get('state')->getData() . '; ';
            $shippingAddress .= $sellOrderForm->get('zipCode')->getData() . '; ';
            $shippingAddress .= $sellOrderForm->get('country')->getData() . '; ';
            $order->setShippingAddress($shippingAddress);

            $customerService->createAndLogin($order);
            $order->setSiteType(self::SITE_TYPES[$site]);
            $this->get('sale_sites.order')->createOrder($data, $order);
            $this->addFlash('order_success', $order->getId());
        }

        $response = new RedirectResponse(
            $this->generateUrl(
                'sale_sites_checkout_page',
                ['userId' => $owner->getId()]
            )
        );

        $cart = $this->get('sale_sites.cart');
        $cart->clearSiteCart(self::SITE_TYPES[$site]);
        $response->headers->setCookie($cart->generateCookies());

        return $response;
    }

    /**
     * @param string $type
     * @param string $message
     */
    protected function addFlash($type, $message)
    {
        $flashbag = $this->get('session')->getFlashBag();
        $flashbag->add($type, $message);
    }

    /**
     * Redirect To Login
     *
     * @return RedirectResponse
     */
    private function redirectToLogin(): RedirectResponse
    {
        $this->addFlash('checkout_error', 'You have the account in our platform please login!');
        return new RedirectResponse (
            $this->generateUrl(
                'fos_user_security_login',
                [
                    '_target_path' => $this->generateUrl('sale_sites_checkout_page')
                ]
            )
        );
    }
}
