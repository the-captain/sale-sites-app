<?php

namespace SaleSitesBundle\Controller;

use Symfony\Component\HttpFoundation\Response;

class HealthCheckController
{
    /**
     * @return Response
     */
    public function healthCheckAction()
    {
        return new Response('Health: ok');
    }
}
