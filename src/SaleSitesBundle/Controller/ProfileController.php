<?php

namespace SaleSitesBundle\Controller;

use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\UserInterface;
use Knp\Component\Pager\Paginator;
use SaleSitesBundle\EventListener\ClientSubscriber;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use ThreeWebOneEntityBundle\Entity\Order\Order;
use ThreeWebOneEntityBundle\Entity\User;

class ProfileController
{
    const ORDER_PAGE_LIMIT = 5;

    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var \Twig_Environment
     */
    private $twig;
    /**
     * @var ClientSubscriber
     */
    private $ClientSubscriber;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var Paginator
     */
    private $paginator;

    /**
     * ProfileController constructor.
     *
     * @param RequestStack $requestStack
     * @param TokenStorageInterface $tokenStorage
     * @param EntityManager $entityManager
     * @param \Twig_Environment $twig
     * @param ClientSubscriber $ClientSubscriber
     * @param Paginator $paginator
     */
    public function __construct(
        RequestStack $requestStack,
        TokenStorageInterface $tokenStorage,
        EntityManager $entityManager,
        \Twig_Environment $twig,
        ClientSubscriber $ClientSubscriber,
        Paginator $paginator
    )
    {
        $this->requestStack = $requestStack;
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $entityManager;
        $this->twig = $twig;
        $this->ClientSubscriber = $ClientSubscriber;
        $this->request = $requestStack->getCurrentRequest();
        $this->paginator = $paginator;
    }

    /**
     * Show Action
     *
     * @return Response
     */
    public function showAction(): Response
    {
        /**@var User $owner */
        $owner = $this->ClientSubscriber->getUser();
        $orderRepo = $this->entityManager->getRepository(Order::class);

        $user = $this->tokenStorage->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $page = $this->request->query->getInt('page', 1);

        $pagination = $this->paginator->paginate(
            $orderRepo->getAllOrdersByOwnerAndCustomerIdsQuery($owner->getId(), $user->getId()),
            $page,
            self::ORDER_PAGE_LIMIT
        );

        return new Response($this->twig->render(
            '@FOSUser/Profile/show_content.html.twig',
            [
                'user' => $user,
                'orders' => $orderRepo->getAllOrdersByOwnerAndCustomerIds(
                    $owner->getId(),
                    $user->getId(),
                    self::ORDER_PAGE_LIMIT,
                    $this->getOffset($page - 1)
                ),
                'pagination' => $pagination
            ]
        ));
    }

    /**
     * Get Offset
     *
     * @param $page
     * @return int
     */
    private function getOffset($page): int
    {
        return $page * self::ORDER_PAGE_LIMIT;
    }
}
