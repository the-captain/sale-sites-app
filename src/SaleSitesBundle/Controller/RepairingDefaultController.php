<?php

namespace SaleSitesBundle\Controller;

use SaleSitesBundle\Service\ChargeBee\SiteSplitService;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\PriceType;

/**
 * Class RepairingDefaultController.
 */
class RepairingDefaultController extends BaseController
{
    /**
     * Shows the Index Page of Selling 3Web site
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        if ($this->isSiteFeatureGranted(SiteSplitService::WEBSITE_REPAIR) === false) {
            return $this->getUnpaidResponse();
        }

        $user = $this->getClientSubscriber()->getUser();
        $categories = $this->getRepo(Category::class)
            ->getParentCategoryThatHasPriceBuybackOrRepair($user, PriceType::REPAIR);

        return $this->render(
            'SaleSitesBundle:Repairing:index.html.twig',
            [
                'body_class' => 'green',
                'categories' => $categories,
                'site' => 'repair',
            ]
        );
    }
}
