<?php

namespace SaleSitesBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use ThreeWebOneEntityBundle\Entity\Page\Page;
use ThreeWebOneEntityBundle\Entity\Page\PageTypeInterface;

/**
 * Class TermsController.
 */
class TermsController extends BaseController
{
    /**
     * Shows terms and conditions page
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws EntityNotFoundException
     */
    public function indexAction()
    {
        $page = $this->getRepo(Page::class)->getUserPageByPageType(
            $this->getClientSubscriber()->getUser(),
            PageTypeInterface::TERMS
        );

        if (!$page) {
            throw new EntityNotFoundException('Page Entity not found!');
        }

        return $this->render(
            'SaleSitesBundle:Page:page.html.twig',
            [
                'page' => $page,
            ]
        );
    }
}
