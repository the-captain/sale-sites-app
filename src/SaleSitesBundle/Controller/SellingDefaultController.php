<?php

namespace SaleSitesBundle\Controller;

use SaleSitesBundle\Service\ChargeBee\SiteSplitService;
use ThreeWebOneEntityBundle\Entity\Category;

/**
 * Class SellingDefaultController.
 */
class SellingDefaultController extends BaseController
{
    /**
     * Shows the Index Page of Selling 3Web site
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        if ($this->isSiteFeatureGranted(SiteSplitService::WEBSITE_SALE) === false) {
            return $this->getUnpaidResponse();
        }

        $user = $this->getClientSubscriber()->getUser();
        $categories = $this->getRepo(Category::class)
            ->getUsersCategoriesHavingPrices($user, Category::TYPE_CATEGORY);

        return $this->render(
            'SaleSitesBundle:Selling:index.html.twig',
            [
                'body_class' => 'green',
                'categories' => $categories,
                'site' => 'sell',
            ]
        );
    }
}
