<?php

namespace SaleSitesBundle\Controller;

use SaleSitesBundle\Service\ChargeBee\SiteSplitService;
use Symfony\Component\HttpFoundation\Request;
use ThreeWebOneEntityBundle\Entity\Model;
use ThreeWebOneEntityBundle\Entity\Price;
use ThreeWebOneEntityBundle\Entity\PriceType;

/**
 * Class RepairingProductController.
 */
class RepairingProductController extends BaseController
{
    /**
     * Shows a product of Category Products
     *
     * @param Model $model
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Model $model, Request $request)
    {
        if ($this->isSiteFeatureGranted(SiteSplitService::WEBSITE_REPAIR) === false) {
            return $this->getUnpaidResponse();
        }

        $response = $this->getSlugService()->checkSlug($model, $request);
        $user = $this->getClientSubscriber()->getUser();
        if ($response) {
            return $response;
        }

        return $this->render(
            'SaleSitesBundle:Products:repair-product.html.twig',
            [
                'body_class' => 'buy-list',
                'model' => $model,
                'prices' => $this->getRepo(Price::class)
                    ->getPricesByModelUserAndSite($model, PriceType::REPAIR, $user),
            ]
        );
    }
}
