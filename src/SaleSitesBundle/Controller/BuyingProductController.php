<?php

namespace SaleSitesBundle\Controller;

use SaleSitesBundle\Service\ChargeBee\SiteSplitService;
use Symfony\Component\HttpFoundation\Request;
use ThreeWebOneEntityBundle\Entity\Model;
use ThreeWebOneEntityBundle\Entity\Price;
use ThreeWebOneEntityBundle\Entity\PriceType;

/**
 * Class BuyingProductController.
 */
class BuyingProductController extends BaseController
{
    /**
     * Shows a product of Category Products
     *
     * @param Model $model
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function indexAction(Model $model, Request $request)
    {
        if ($this->isSiteFeatureGranted(SiteSplitService::WEBSITE_BUY) === false) {
            return $this->getUnpaidResponse();
        }

        $response = $this->getSlugService()->checkSlug($model, $request);
        $user = $this->getClientSubscriber()->getUser();
        if ($response) {
            return $response;
        }

        return $this->render(
            'SaleSitesBundle:Products:buy-product.html.twig',
            [
                'body_class' => 'green',
                'model' => $model,
                'prices' => $this->getRepo(Price::class)
                    ->getPricesByModelUserAndSite($model, PriceType::BUYBACK, $user),
            ]
        );
    }
}
