<?php

namespace SaleSitesBundle\Controller;

use SaleSitesBundle\Service\ChargeBee\SiteSplitService;

/**
 * Class DefaultController.
 */
class DefaultController extends BaseController
{
    /**
     * Shows the Index Page of 3Web site
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function indexAction()
    {
        if ($this->isSiteFeatureGranted(SiteSplitService::MULTI_WEBSITE_LANDING_PAGE)) {
            return $this->render(
                'SaleSitesBundle:Default:index.html.twig',
                [
                    'body_class' => 'green',
                ]
            );
        } else {
            $subscriptionToStateMap = [
                'selling_site_homepage' => $this->isSiteFeatureGranted(SiteSplitService::WEBSITE_SALE),
                'buying_site_homepage' => $this->isSiteFeatureGranted(SiteSplitService::WEBSITE_BUY),
                'repairing_site_homepage' => $this->isSiteFeatureGranted(SiteSplitService::WEBSITE_REPAIR),
            ];

            foreach ($subscriptionToStateMap as $route => $isSubscribed) {
                if ($isSubscribed === true) {
                    return $this->redirect(
                        $this->generateUrl($route, ['userId' => $this->getClientSubscriber()->getUserId()])
                    );
                }
            }

            return $this->getUnpaidResponse();
        }
    }
}
