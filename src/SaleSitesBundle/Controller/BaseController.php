<?php

namespace SaleSitesBundle\Controller;

use SaleSitesBundle\EventListener\ClientSubscriber;
use SaleSitesBundle\Service\ChargeBee\SiteSplitService;
use SaleSitesBundle\Service\ChargeBee\SubscriptionService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BaseController.
 */
class BaseController extends Controller
{
    /**
     * @param string $featureName
     * @return bool
     */
    public function isSiteFeatureGranted(string $featureName): bool
    {
        return $this->getSiteSplitService()->isAccessible($featureName);
    }

    /**
     * @param string $entityClass
     *
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getRepo($entityClass)
    {
        return $this->getDoctrine()->getRepository($entityClass);
    }

    /**
     * @return \Knp\Component\Pager\Paginator|object
     */
    public function getKnpPaginator()
    {
        return $this->get('knp_paginator');
    }

    /**
     * @return \SaleSitesBundle\Service\FilterService
     */
    public function getFilterService()
    {
        return $this->get('sale_sites.service.filter');
    }

    /**
     * @return \SaleSitesBundle\Service\SlugService
     */
    public function getSlugService()
    {
        return $this->get('sale_sites.service.slug');
    }

    /**
     * @return SubscriptionService
     */
    public function getSubscriptionService(): SubscriptionService
    {
        return $this->get('sale_sites.charge_bee.subscription_service');
    }

    /**
     * @return SiteSplitService
     */
    public function getSiteSplitService(): SiteSplitService
    {
        return $this->get('sale_sites.charge_bee.site_split_service');
    }

    /**
     * @return ClientSubscriber
     */
    public function getClientSubscriber() : ClientSubscriber
    {
        return $this->get('sale_sites.site_subscriber');
    }

    /**
     * @return Response
     * @throws \Exception
     */
    public function getUnpaidResponse(): Response
    {
        return $this->redirect(
            $this->generateUrl('service_unpaid', ['userId' => $this->getClientSubscriber()->getUser()->getId()])
        );
    }
}
