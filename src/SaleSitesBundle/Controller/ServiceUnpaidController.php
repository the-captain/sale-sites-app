<?php

namespace SaleSitesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class ServiceUnpaidController.
 */
class ServiceUnpaidController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('SaleSitesBundle:ServiceUnpaid:index.html.twig');
    }
}
