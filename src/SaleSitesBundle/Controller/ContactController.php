<?php

namespace SaleSitesBundle\Controller;

use SaleSitesBundle\EventListener\ClientSubscriber;
use Symfony\Component\HttpFoundation\Response;
use Twig_Environment;

/**
 * Class ContactController.
 */
class ContactController extends BaseController
{
    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var ClientSubscriber
     */
    private $ClientSubscriber;

    /**
     * ContactController constructor.
     *
     * @param Twig_Environment $twig
     * @param ClientSubscriber $ClientSubscriber
     */
    public function __construct(Twig_Environment $twig, ClientSubscriber $ClientSubscriber)
    {
        $this->twig = $twig;
        $this->ClientSubscriber = $ClientSubscriber;
    }

    /**
     * @return Response
     */
    public function contactPageAction()
    {
        return new Response($this->twig->render(
            'SaleSitesBundle:FindOutMore:contact.html.twig',
            [
                'locations' => $this->ClientSubscriber->getUser()->getLocations()
            ]
        ));
    }
}
