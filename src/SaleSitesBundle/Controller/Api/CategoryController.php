<?php

namespace SaleSitesBundle\Controller\Api;

use SaleSitesBundle\Controller\BaseController;
use ThreeWebOneEntityBundle\Entity\Category;

class CategoryController extends BaseController
{
    /**
     * Get children categories by User and parent Category
     *
     * @param Category $category
     * @param string $site
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getChildrenCategoriesAction(Category $category, string $site)
    {
        $user = $this->get('sale_sites.site_subscriber')->getUser();
        $categories = $this->getRepo(Category::class)->getUserChildrenCategoriesByCategory($category, $user);
        $templateName = ($site == 'sell') ? 'SaleSitesBundle:Api:sell-categories.html.twig' :
            'SaleSitesBundle:Api:ajax-items-list.html.twig';

        return $this->render(
            $templateName,
            [
                'path' => 'api_get_products',
                'target' => 'products',
                'header' => 'Choose the category',
                'collapse' => '2',
                'items' => $categories,
                'site' => $site,
            ]
        );
    }
}
