<?php

namespace SaleSitesBundle\Controller\Api;

use SaleSitesBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\Filter;
use ThreeWebOneEntityBundle\Entity\Model;
use ThreeWebOneEntityBundle\Entity\PriceType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class ProductController extends BaseController
{
    /**
     * Get user's products by vategory
     *
     * @param Category $category
     * @param string $site
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getUserProductsByCategoryAction(Category $category, string $site)
    {
        $siteType = $site == 'buy' ? PriceType::BUYBACK : PriceType::REPAIR;
        $user = $this->get('sale_sites.site_subscriber')->getUser();
        $products = $this->getRepo(Category::class)
            ->getUserProducts($user, $category, $siteType);

        return $this->render(
            'SaleSitesBundle:Api:ajax-items-list.html.twig',
            [
                'path' => 'api_get_carriers',
                'target' => 'carriers',
                'header' => 'Choose the model you have',
                'collapse' => '1',
                'items' => $products,
                'site' => $site,
                'category' => $category,
            ]
        );
    }
}
