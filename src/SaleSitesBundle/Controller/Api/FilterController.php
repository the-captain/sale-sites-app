<?php

namespace SaleSitesBundle\Controller\Api;

use SaleSitesBundle\Controller\BaseController;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\Filter;
use ThreeWebOneEntityBundle\Entity\PriceType;

class FilterController extends BaseController
{
    /**
     * Get carrier by ajax
     *
     * @param Category $category
     * @param Category $product
     * @param string $site
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getCarrierByProductAction(Category $category, Category $product, string $site)
    {
        $siteType = $site == 'buy' ? PriceType::BUYBACK : PriceType::REPAIR;
        $user = $this->get('sale_sites.site_subscriber')->getUser();
        $carriers = $this->getRepo(Category::class)
            ->getUserProviders($user, $category, $product, $siteType);

        return $this->render(
            'SaleSitesBundle:Api:ajax-items-list.html.twig',
            [
                'path' => 'api_get_models',
                'target' => 'models',
                'header' => 'Choose your device cell carrier',
                'collapse' => '0',
                'items' => $carriers,
                'category' => $category,
                'product_id' => $product->getId(),
                'site' => $site,
            ]
        );
    }
}
