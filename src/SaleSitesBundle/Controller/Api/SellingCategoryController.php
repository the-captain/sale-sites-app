<?php

namespace SaleSitesBundle\Controller\Api;

use SaleSitesBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ThreeWebOneEntityBundle\Entity\Category;

class SellingCategoryController extends BaseController
{
    /**
     * @param Request $request
     * @param Category $category
     * @return JsonResponse
     * @throws \Exception
     */
    public function indexAction(Request $request, Category $category) :JsonResponse
    {
        return new JsonResponse(
            $this->get('sale_sites.service.selling_items')->getSellingItems($category, $request),
            Response::HTTP_OK
        );
    }
}
