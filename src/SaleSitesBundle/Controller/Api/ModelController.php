<?php

namespace SaleSitesBundle\Controller\Api;

use SaleSitesBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\Filter;
use ThreeWebOneEntityBundle\Entity\Model;
use ThreeWebOneEntityBundle\Entity\PriceType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class ModelController extends BaseController
{
    /**
     * Get available models by ajax
     *
     * @param Category $category
     * @param Category $product
     * @param Category $provider
     * @param string $site
     * @ParamConverter("category", options={"id": "categoryId"})
     * @ParamConverter("product", options={"id": "productId"})
     * @ParamConverter("provider", options={"id": "filterId"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getModelsByProductAndFilterAction(
        Category $category,
        Category $product,
        Category $provider,
        string $site
    ) {
        $priceType = ($site == 'repair') ? PriceType::REPAIR : PriceType::BUYBACK;
        $user = $this->get('sale_sites.site_subscriber')->getUser();
        $models = $this->getRepo(Model::class)
            ->getModelsByCategoryProductAndProvider($category, $product, $provider, $priceType, $user);

        return $this->render(
            'SaleSitesBundle:Api:models.html.twig',
            [
                'models' => $models,
                'path' => $priceType == 3 ? 'repairing_category_product' : 'buying_category_product',
            ]
        );
    }
}
