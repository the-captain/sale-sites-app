<?php

namespace SaleSitesBundle\Controller\Api;

use SaleSitesBundle\Service\LocationService;
use SaleSitesBundle\Service\SiteTypeService;
use Symfony\Component\HttpFoundation\JsonResponse;

class LocationController
{
    /**
     * @var LocationService
     */
    private $locationService;

    /**
     * @var SiteTypeService
     */
    private $siteTypeService;

    /**
     * LocationController constructor.
     * @param LocationService $locationService
     * @param SiteTypeService $siteTypeService
     */
    public function __construct(LocationService $locationService, SiteTypeService $siteTypeService)
    {
        $this->locationService = $locationService;
        $this->siteTypeService = $siteTypeService;
    }

    /**
     * @param string $site
     * @return JsonResponse
     */
    public function getSiteTypeLocationsAction(string $site) : JsonResponse
    {
        $siteType = $this->siteTypeService->getSiteTypeByString($site);

        return new JsonResponse($this->locationService->getLocationsBySite($siteType), 200);
    }

    /**
     * Get Locations Action
     *
     * @return JsonResponse
     */
    public function getLocationsAction() : JsonResponse
    {
        return new JsonResponse($this->locationService->getUserLocationsInArrayFormat(), 200);
    }
}
