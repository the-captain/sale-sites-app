<?php

namespace SaleSitesBundle\Controller\Api;

use Doctrine\ORM\EntityManager;
use SaleSitesBundle\Service\CartItemsService;
use SaleSitesBundle\Service\CartService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemImage;
use ThreeWebOneEntityBundle\Entity\Price;
use ThreeWebOneEntityBundle\Entity\PriceType;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class CartController
{
    const REMOVED_ITEM_MESSAGE = 'Due to unavailability some items were removed from your cart';

    /**
     * @var CartService
     */
    private $cart;

    /**
     * @var CartItemsService
     */
    private $cartItemsService;

    /**
     * @var UploaderHelper
     */
    private $uploadHelper;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * CartController constructor.
     * @param CartService $cart
     * @param CartItemsService $cartItemsService
     * @param UploaderHelper $uploaderHelper
     * @param EntityManager $entityManager
     */
    public function __construct(
        CartService $cart,
        CartItemsService $cartItemsService,
        UploaderHelper $uploaderHelper,
        EntityManager $entityManager
    ) {
        $this->cart = $cart;
        $this->cartItemsService = $cartItemsService;
        $this->uploadHelper = $uploaderHelper;
        $this->entityManager = $entityManager;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getCartAction()
    {
        $data = $this->cartItemsService->getPricesFromSession();
        $items = [];
        $message = null;

        foreach ($data['items'] as $key => $price) {
            $add = true;

            $image = null;
            $site = $price->getPriceType()->getSite();
            if ($site == PriceType::SALE) {
                if ($price->getInventoryItem()->getImage()) {
                    $image = $this->uploadHelper->asset($price->getInventoryItem()->getImage(), 'imageFile', InventoryItemImage::class);
                }
                $title = $price->getInventoryItem()->getTitle();
            } else {
                $image = $this->uploadHelper->asset($price->getModel()->getImage(), 'imageFile');
                $title = $price->getModel()->getTitle();
            }
            $item = [
                'id' => $price->getId(),
                'image' => $image,
                'title' => $title,
                'site' => $site,
                'price' => $price->getValue(),
                'condition' => $price->getPriceType()->getTitle(),
                'quantity' => $data['counts'][$key],
                'shippingPrice' => $price->getInventoryItem() ? $price->getInventoryItem()->getShippingPrice() : 0,
            ];
            $inventoryItem = $price->getInventoryItem();
            if ($inventoryItem) {
                $item['shippingPrice'] = $inventoryItem->getShippingPrice() ?? 0;
                $item['realQuantity'] = $inventoryItem->getRealQuantity();
            }
            if (isset($item['realQuantity'])) {
                if ($item['realQuantity'] == 0) {
                    $this->cart->removeItem($price->getId());
                    $add = false;
                    $message = self::REMOVED_ITEM_MESSAGE;
                }

                if ($item['realQuantity'] < $item['quantity']) {
                    $item['quantity'] = $item['realQuantity'];
                    $message = self::REMOVED_ITEM_MESSAGE;
                }
            }

            if ($add) {
                $items[] = $item;
            }
        }
        $data['items'] = $items;

        $response = new JsonResponse(['cart_data' => $data, 'message' => $message]);
        $response->headers->setCookie($this->cart->generateCookies());

        return $response;
    }

    /**
     * @param Price $price
     * @param $quantity
     *
     * @return JsonResponse
     */
    public function addToCartAction(Price $price, $quantity)
    {
        $response = $this->cart->addItem($price, $quantity, $price->getPriceType()->getSite());

        return $this->generateResponse(true, $response[0], $response[1]);
    }

    /**
     * @param Price $price
     * @param $quantity
     *
     * @return JsonResponse
     */
    public function removeFromCartAction(Price $price, $quantity)
    {
        $status = $this->cart->removeItem($price->getId(), $quantity);

        return $this->generateResponse($status, 'removed');
    }

    /**
     * @param $id
     *
     * @return JsonResponse
     */
    public function deleteFromCartAction($id)
    {
        return $this->generateResponse($this->cart->deleteItem($id), 'deleted');
    }

    /**
     * @param $priceId
     *
     * @return Price
     */
    private function getItem($priceId)
    {
        $price = $this->entityManager->getRepository('ThreeWebOneEntityBundle:Price')->findOneById($priceId);

        if (!$price) {
            throw new NotFoundHttpException(sprintf('Price %s not found', $priceId));
        }

        return $price;
    }

    /**
     * @param bool $value
     * @param string $key
     * @param int $code
     *
     * @return JsonResponse
     */
    private function generateResponse($value = true, $key = 'added', $code = Response::HTTP_OK)
    {
        $response = new JsonResponse([$key => $value], $code);
        $response->headers->setCookie($this->cart->generateCookies());

        return $response;
    }


    /**
     * Add Array of Price ids to Cart
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function addArrayToCartAction(Request $request)
    {
        if ($request->request->has('ids')) {
            foreach ($request->request->get('ids') as $id) {
                //todo: remove later
                $this->cart->addItem($this->getItem($id)->getId());
            }

            $isAdded = true;
        } else {
            $isAdded = false;
        }

        return $this->generateResponse($isAdded);
    }
}
