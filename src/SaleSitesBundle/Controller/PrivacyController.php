<?php

namespace SaleSitesBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use ThreeWebOneEntityBundle\Entity\Page\Page;
use ThreeWebOneEntityBundle\Entity\Page\PageTypeInterface;

/**
 * Class PrivacyController.
 */
class PrivacyController extends BaseController
{
    /**
     * Shows Privacy Page
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws EntityNotFoundException
     */
    public function indexAction()
    {
        $page = $this->getRepo(Page::class)->getUserPageByPageType(
            $this->getClientSubscriber()->getUser(),
            PageTypeInterface::PRIVACY
        );

        if (!$page) {
            throw new EntityNotFoundException('Page Entity not found!');
        }

        return $this->render(
            'SaleSitesBundle:Page:page.html.twig',
            [
                'page' => $page,
            ]
        );
    }
}
