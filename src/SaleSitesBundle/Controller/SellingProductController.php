<?php

namespace SaleSitesBundle\Controller;

use SaleSitesBundle\Service\ChargeBee\SiteSplitService;
use Symfony\Component\HttpFoundation\Request;
use ThreeWebOneEntityBundle\Entity\Price;

/**
 * Class SellingProductController
 */
class SellingProductController extends BaseController
{
    /**
     * Shows a product of Category Products
     *
     * @param Price $price
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Price $price, Request $request)
    {
        if ($this->isSiteFeatureGranted(SiteSplitService::WEBSITE_SALE) === false) {
            return $this->getUnpaidResponse();
        }

        $response = $this->getSlugService()->checkSlug($price->getInventoryItem(), $request);
        if ($response) {
            return $response;
        }

        return $this->render(
            'SaleSitesBundle:Products:product.html.twig',
            [
                'body_class' => 'buy-list',
                'price' => $price,
                'multiple' => $this->getFilterService()->groupMultipleModelFilters($price->getInventoryItem()),
                'selectedFilters' => $request->query->all(),
            ]
        );
    }
}
