<?php

namespace SaleSitesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class FaqController.
 */
class FaqController extends BaseController
{
    /**
     * Shows FAQ page
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $questionComposite = [];
        foreach ($this->get('sale_sites.site_subscriber')->getUser()->getQuestions() as $question) {
            $questionComposite[$question->getcategory()][] = $question;
        }

        return $this->render(
            'SaleSitesBundle:FindOutMore:faq.html.twig',
            [
                'questionsComposite' => $questionComposite,
            ]
        );
    }
}
