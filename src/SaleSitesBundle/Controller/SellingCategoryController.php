<?php

namespace SaleSitesBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\Price;

class SellingCategoryController extends BaseController
{
    /**
     * Shows the List of Category Products
     *
     * @param Request $request
     * @param Category $category
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, Category $category)
    {
        $response = $this->getSlugService()->checkSlug($category, $request);
        if ($response) {
            return $response;
        }

        $user = $this->get('sale_sites.site_subscriber')->getUser();
        $filtersArray = $this->getFilterService()->getFilters($category, $user);

        return $this->render(
            'SaleSitesBundle:Categories:selling-items-list.html.twig',
            [
                'body_class' => 'buy-list',
                'category' => $category,
                'filtersArray' => $filtersArray['filters'],
                'grades' => $filtersArray['grade']
            ]
        );
    }
}
