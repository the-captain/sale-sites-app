<?php

namespace SaleSitesBundle\Doctrine;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\Filter;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Entity\Model;
use ThreeWebOneEntityBundle\Entity\PriceType;
use ThreeWebOneEntityBundle\Status\StatusInterface;

class ActiveFilter extends SQLFilter
{
    /**
     * @var array Entities that implement status behaviour
     */
    private $classesWithStatus = [
        Category::class,
        Model::class,
        Filter::class,
        PriceType::class,
        InventoryItem::class,
    ];

    /**
     * @param ClassMetadata $targetEntity
     * @param $targetTableAlias
     *
     * @return string
     */
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        if (!in_array($targetEntity->getReflectionClass()->name, $this->classesWithStatus)) {
            return '';
        }

        return sprintf('%s.status = %d', $targetTableAlias, StatusInterface::STATUS_ACTIVE);
    }
}
