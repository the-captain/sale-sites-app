<?php

namespace SaleSitesBundle\EventListener;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Transforms the body of a json request to POST parameters.
 */
class JsonRequestTransformerSubscriber implements EventSubscriberInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [
                // 128 is busy for saving request data to session
                ['onKernelRequest', 127],
            ],
        ];
    }

    /**
     * @param GetResponseEvent $event
     *
     * @return bool
     */
    public function onKernelRequest(GetResponseEvent $event) : bool
    {
        $request = $event->getRequest();

        if (!$event->isMasterRequest()) {
            return false;
        }

        if (!$request->isMethod('POST')) {
            return false;
        }

        if (!$this->isJsonRequest($request)) {
            return false;
        }

        $content = $request->getContent();

        if (empty($content)) {
            return false;
        }

        if (!$this->transformJsonBody($request)) {
            $response = Response::create('Unable to parse request.', 400);
            $event->setResponse($response);
        }

        return true;
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    private function isJsonRequest(Request $request) : bool
    {
        return 'json' === $request->getContentType();
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    private function transformJsonBody(Request $request) : bool
    {
        $data = json_decode($request->getContent(), true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            return false;
        }

        if ($data === null) {
            return true;
        }

        $request->request->replace($data);
        return true;
    }
}
