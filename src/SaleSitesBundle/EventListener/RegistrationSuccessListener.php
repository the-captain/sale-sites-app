<?php

namespace SaleSitesBundle\EventListener;

use FOS\UserBundle\Event\FormEvent;
use ThreeWebOneEntityBundle\Entity\Customer\Customer;

class RegistrationSuccessListener
{
    /**
     * @var ClientSubscriber
     */
    private $client;

    /**
     * RegistrationType constructor.
     *
     * @param ClientSubscriber $client
     */
    public function __construct(ClientSubscriber $client)
    {
        $this->client = $client;
    }

    /**
     * @param FormEvent $event
     */
    public function onRegistrationSuccess(FormEvent $event)
    {
        /**@var Customer $customer*/
        $customer = $event->getForm()->getData();
        $owner = $this->client->getUser();
        $customer->setOwner($owner);
    }
}
