<?php

namespace SaleSitesBundle\EventListener;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Exception;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\KernelInterface;
use ThreeWebOneEntityBundle\Entity\Domain\ClientDomain;
use ThreeWebOneEntityBundle\Entity\PopularSearch;
use ThreeWebOneEntityBundle\Entity\User;
use ThreeWebOneEntityBundle\Entity\UserConfig\Config;
use ThreeWebOneEntityBundle\Entity\UserConfig\ConfigLogo;
use ThreeWebOneEntityBundle\Entity\UserConfig\Location;
use ThreeWebOneEntityBundle\Entity\UserConfig\Seo;
use ThreeWebOneEntityBundle\Repository\UserRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Listener that provides information about current client
 */
class ClientSubscriber implements EventSubscriberInterface
{
    const ROUTE_HEALTHCHECK = 'sale_sites__healthcheck';

    /**
     * @var string
     */
    private $userId;

    /**
     * @var int
     */
    private $defaultUserId;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * ClientSubscriber constructor.
     *
     * @param Router          $router
     * @param EntityManager   $em
     * @param KernelInterface $kernel
     * @param int             $defaultUserId
     */
    public function __construct(Router $router, EntityManager $em, KernelInterface $kernel, int $defaultUserId)
    {
        $this->router = $router;
        $this->em = $em;
        $this->kernel = $kernel;
        $this->defaultUserId = $defaultUserId;
    }

    /**
     * @inheritdoc
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [
                // 9 because we need user before run FirewallListener
                ['onKernelRequest', 9],
            ],
        ];
    }

    /**
     * @return int|null
     */
    public function getDefaultUserId(): ?int
    {
        return $this->defaultUserId;
    }

    /**
     * @return bool
     */
    public function hasDefaultUserId(): bool
    {
        return !is_null($this->defaultUserId);
    }

    /**
     * Executed when a request is received
     *
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $route = $event->getRequest()->attributes->get('_route');
        // Only the master request should be used.
        // Internal requests should be skipped.
        if ($event->isMasterRequest() && self::ROUTE_HEALTHCHECK != $route) {
            // Do not load a user during kernel.request event

            $host = $event->getRequest()->getHost();

            /** @var UserRepository $userRepository */
            $clientDomainRepo = $this->em->getRepository(ClientDomain::class);
            $user = $clientDomainRepo->getUserByDomain($this->prepareDomainName($host));

            if (!is_null($user)) {
                $this->userId = $user->getId();
            } elseif (in_array($this->kernel->getEnvironment(), ['dev', 'test', 'staging'])) {
                $this->userId = $this->defaultUserId;
            } else {
                throw new NotFoundHttpException();
            }
        }
    }

    /**
     * @return Config
     *
     * @throws Exception
     */
    public function getConfig()
    {
        return $this->getUser()->getConfig();
    }

    /**
     * @return User
     *
     * @throws Exception
     */
    public function getUser()
    {
        if (!$this->user) {
            if (null === $this->userId) {
                throw new RuntimeException('Method called at the wrong stage, userId is unknown.');
            }

            $this->user = $this->em->getRepository(User::class)->find($this->userId);
            if (null === $this->user) {
                throw new NotFoundHttpException('Unfortunately user is not found in our database.');
            }
        }

        return $this->user;
    }

    /**
     * @return Collection
     *
     * @throws Exception
     */
    public function getTestimonials()
    {
        return $this->getUser()->getTestimonials();
    }

    /**
     * @param string $site
     *
     * @return null|Seo
     * @throws Exception
     */
    public function getSeoMeta(string $site)
    {
        return $this->getUser()->getSeoMetaBySite($site);
    }

    /**
     * @return null|Location
     * @throws Exception
     */
    public function getSiteMainLocation()
    {
        $locations = $this->getUser()->getLocations();
        $location = $locations[0] ?? null;
        foreach ($locations as $item) {
            if ($item->getIsMain()) {
                $location = $item;
            }
        }

        return $location;
    }

    /**
     * Generate URL for the current user
     *
     * @param  string $name
     * @param  array  $parameters
     * @param  int    $referenceType The type of reference to be generated (one of the constants)
     *
     * @return string
     */
    public function getPath($name, $parameters = [], $referenceType = Router::ABSOLUTE_PATH)
    {
        return $this->router->generate($name, $parameters, $referenceType);
    }

    /**
     * Get id of current project
     *
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return int|null
     * @throws Exception
     */
    public function getLogoSize()
    {
        return $this->getUser()->getConfig()->getConfigLogo()->getLogoSize() ?? ConfigLogo::MEDIUM;
    }

    /**
     * @return array
     */
    public function getPopularSearches(): array
    {
        return $this->em->getRepository(PopularSearch::class)->getOrderedPopularSearches($this->getUser());
    }

    /**
     * @param string $domainName
     * @return string
     */
    protected function prepareDomainName(string $domainName): string
    {
        return (string)str_replace('www.', '', $domainName);
    }
}
