$(document).ready(function () {
    var allModels = $("#allmodels");
    if (allModels.length > 0) {
        $("#show-more").click(function () {
            allModels.finish();
            allModels.slideToggle("slow");
            $(this).find("b").first().css({"display": "none"})
        });
    }
    $(".panel-title").click(function () {
        $(this).find(".toggle-elem").toggle();
    });
    var allCarier = $("#allcarier");
    if (allCarier.length > 0) {
        $("#show-carier").click(function () {
            allCarier.finish();
            allCarier.slideToggle("slow");
        });
    }
    var allCellphone = $("#allcellphone");
    if (allCellphone.length > 0) {
        $("#show-model").click(function () {
            allCellphone.finish();
            allCellphone.slideToggle("slow");
        });
    }
});