$(document).ready(function () {
    var active = '.active';
    $(".panel-title").click(function () {
        if (!$(this).hasClass('active')) {
            $(active).find(".toggle-elem").toggle();
            $(active).toggleClass("active");
        }
        $(this).find(".toggle-elem").toggle();
        $(this).toggleClass("active") ;
    });
});