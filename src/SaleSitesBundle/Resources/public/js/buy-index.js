$( document ).ready( function() {
    //check if element toggled if yes hide it
    var urlParams = new URLSearchParams(window.location.search);
    var id = urlParams.get('id');
    var target = $('#category_' + id);
    if (target.length) {
        ajaxGetItems(target);
    }

    function toggleElement(element) {
        if (element.css('display') == 'block') {
            element.slideToggle("slow");
        }
    }

    //toggle elements
    function closeCollapse(level) {
        var cc = $("#child-categories");
        var models = $("#models");
        var products = $("#products");
        var carriers = $("#carriers");
        if (level > '1') {
            toggleElement(cc);
            toggleElement(products);
        }
        if (level > '0') {
            toggleElement(carriers);
        }
        toggleElement(models);
    }

    function ajaxGetItems(trigger) {
        var target = $('#' + $(trigger).data('target'));
        closeCollapse($(trigger).data('collapse'));
        $.get($(trigger).data('path'), function (response) {
            target.html(response);
            target.slideToggle("slow");
            location.href = $(trigger).attr('href');
        });
    }

    $( document ).on(
        'click',
        '.show-more, .show-products, .show-carriers, .show-models',
        function (event) {
            event.preventDefault();
            ajaxGetItems(this);
        });
});