$( document ).ready( function() {
    function ajaxGetItems(trigger) {
        var cc = $("#child-categories");
        if (cc.css('display') == 'block') {
            cc.slideToggle("slow");
        }
        var target = $('#' + $(trigger).data('target'));
        $.get($(trigger).data('path'), function (response) {
            target.html(response);
            target.slideToggle("slow");
            location.href = $(trigger).attr('href');
        });
    }

    $( document ).on(
        'click',
        '.show-more',
        function (event) {
            event.preventDefault();
            ajaxGetItems(this);
        });

    var allModels = $("#allmodels");
    if (allModels.length > 0) {
        $("#show-more").click(function () {
            allModels.finish();
            allModels.slideToggle("slow");
            $(this).find("b").first().css({"display": "none"})
        });
    }
});