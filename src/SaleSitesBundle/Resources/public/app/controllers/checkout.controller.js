(function () {

    'use strict';

    angular
        .module('app.controllers')
        .controller('CheckoutController', CheckoutController);

    /**
     * @type {[string,string]}
     */
    CheckoutController.$inject = [
        '$scope',
        '$http',
        'CartService',
        'SiteContextHelper',
        '$rootScope'
    ];

    /**
     * CheckoutController
     *
     * @param CartService
     * @param SiteContextHelper
     * @param $scope
     * @constructor
     */
    function CheckoutController($scope,
                                $http,
                                CartService,
                                SiteContextHelper,
                                $rootScope) {
        var vm = this;

        vm.priceId = '';
        vm.isSuccess = false;
        vm.cart = CartService.getCart;

        vm.addItem = CartService.addCartItem;
        vm.removeItem = CartService.removeCartItem;
        vm.deleteItem = CartService.deleteCartItem;

        vm.SiteContextHelper = SiteContextHelper;

        vm.hasSellItems = false;
        vm.hasBuyBackItems = false;
        vm.hasRepairItems = false;
        vm.paymentType = 2;
        vm.deliveryType = 2;
        vm.valid = false;
        vm.firstName = '';
        vm.lastName = '';
        vm.email = '';
        vm.phone = '';
        vm.submitted = false;
        vm.pickup = '';

        vm.hideSalePay = false;
        vm.hideBuybackPay = false;
        vm.hideRepairkPay = false;

        vm.changePayVisability = function (section) {
            if (section === 'sale') {
                vm.hideSalePay = !vm.hideSalePay;
            }

            if (section === 'buyback') {
                vm.hideBuybackPay = !vm.hideBuybackPay;
            }

            if (section === 'repair') {
                vm.hideRepairkPay = !vm.hideRepairkPay;
            }
        };

        /**
         * @param actions
         */
        vm.validForm = function (actions) {
            actions.disable();
            $scope.$watch('vm.valid', function (newValue) {
                if (newValue) {
                    actions.enable();
                } else {
                    actions.disable();
                }
            });
        };

        $rootScope.$on('paypal_submit', function (ev, data) {
            vm.submitted = true;
            $scope.$apply();
        });

        vm.checkFormValidity = function() {
          if (vm.firstName && vm.lastName && vm.email && vm.phone) {
              vm.valid = true;
          } else {
              vm.valid = false;
          }
        };

        vm.onPayment = function () {
            return new paypal.Promise(function (resolve, reject) {
                $http({
                    method: 'POST',
                    url: '/paypal-express/create-payment'
                }).then(function successCallback(response) {
                    var data = response.data;
                    if (data.id) {
                        resolve(data.id);
                    } else {
                        reject('improper response');
                    }
                }, function errorCallback(response) {
                    reject(response);
                });
            });
        };

        vm.onAuthorize = function (data, actions) {
            data['userEntered'] = {
                firstName: vm.firstName,
                lastName: vm.lastName,
                email: vm.email,
                phone: vm.phone,
                delivery: vm.deliveryType,
                payment: vm.paymentType,
                pickup: vm.pickup
            };
            $http({
                method: 'POST',
                url: '/paypal-express/execute-payment',
                data: data
            }).then(function successCallback(response) {
                window.location.href = response.data.redirect;
            }, function errorCallback(response) {
                // DO SMTHN WITH ERRO
            });
        };

        vm.onCancel = function (data, actions) {
        };

        $scope.$watch(
            function () {
                return CartService.getCart();
            },
            function () {
                vm.hasSellItems = hasContextItems(SiteContextHelper.isSellingContext);
                vm.hasBuyBackItems = hasContextItems(SiteContextHelper.isBuyBackContext);
                vm.hasRepairItems = hasContextItems(SiteContextHelper.isRepairContext);
            }
        );

        /**
         * @param value
         */
        vm.changeDelivery = function (value) {
            vm.deliveryType = value;
            if (value == 2) {
                vm.paymentType = 2;
            }
        };

        /**
         * @param value
         */
        vm.changePayment = function (value) {
            vm.paymentType = value;
        };

        /**
         * Checks if we have items of different site types in our Cart
         *
         * @param context SiteContextHelper
         * @returns {boolean}
         */
        function hasContextItems(context) {
            var items = vm.cart().items;

            for (var item in items) {
                if (context(items[item].site)) {
                    return true;
                }
            }

            return false;
        }
    }
})();
