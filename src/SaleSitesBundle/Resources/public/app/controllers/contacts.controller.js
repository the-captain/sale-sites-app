(function () {

    'use strict';

    angular
        .module('app.controllers')
        .controller('ContactsController', ContactsController);

    /**
     * @type {string[]}
     */
    ContactsController.$inject = [
        '$scope',
        'requestsFactory',
        'SiteContextHelper',
    ];

    /**
     * Contacts Controller
     * @param $scope
     * @param requestsFactory
     * @param SiteContextHelper
     * @constructor
     */
    function ContactsController($scope,
                                requestsFactory,
                                SiteContextHelper) {
        var vm = this;
        vm.locations = [];
        vm.location = '';
        vm.selectedLocation = null;

        requestsFactory.getLocations().then(function (response) {
            vm.locations = response.data;
            if (Object.keys(vm.locations).length !== 0 && vm.locations[0].hasOwnProperty('id')) {
                vm.location = vm.locations[0].id.toString();
            }
        });

        $scope.$watch(
            function () {
                return vm.location
            },
            function () {
                selectLocation(parseInt(vm.location));
            }
        );

        /**
         * Get Service Names
         * @param {array} services
         * @returns {string}
         */
        vm.getServiceNames = function (services) {
            var result = [];
            for (var key in services) {
                result.push(SiteContextHelper.getSiteNameByTypeId(services[key]));
            }

            return result.join(', ');
        };

        /**
         * Select Location
         * @param {number} locationId
         * @returns {boolean}
         */
        function selectLocation(locationId) {
            if (Object.keys(vm.locations).length === 0) {
                return false;
            }

            for (var key in vm.locations) {
                if (vm.locations.hasOwnProperty(key) && vm.locations[key].hasOwnProperty('id')) {
                    if (vm.locations[key].id === locationId) {
                        vm.selectedLocation = vm.locations[key];
                    }
                } else {
                    throw new Error('Locations has wrong format');
                }
            }

            return true;
        }
    }
})();
