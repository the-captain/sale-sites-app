(function () {

    'use strict';

    angular
        .module('app.controllers')
        .controller('messageController', messageController);

    /**
     * @type {[string]}
     */
    messageController.$inject = ['MessageService'];

    /**
     * @param MessageService
     */
    function messageController(MessageService) {
        var vm = this;
        vm.getMessages = getMessages;
        vm.messages = [];

        function getMessages() {
            return MessageService.getMessages();
        }
    }
})();