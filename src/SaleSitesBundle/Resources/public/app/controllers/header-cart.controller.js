(function() {

    'use strict';

    angular
        .module('app.controllers')
        .controller('HeaderCartController', HeaderCartController);

    /**
     * @type {string[]}
     */
    HeaderCartController.$inject = [
        'CartService'
    ];

    /**
     * HeaderCartController
     *
     * @param CartService
     * @constructor
     */
    function HeaderCartController(
        CartService
    ) {
        var vm = this;
        vm.checkoutSum = CartService.getCheckoutSum;
    }
})();
