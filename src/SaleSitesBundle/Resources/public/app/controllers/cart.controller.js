(function () {

    'use strict';

    angular
        .module('app.controllers')
        .controller('CartController', CartController);

    /**
     * @type {[string]}
     */
    CartController.$inject = [
        'CartService'
    ];

    /**
     * CartControler
     *
     * @param CartService
     * @constructor
     */
    function CartController(CartService) {
        var vm = this;
        vm.priceId = '';
        vm.activePriceId = '';
        vm.quantity = 1;
        vm.aQuantity = 0;
        vm.addItem = CartService.addCartItem;
        vm.isSuccess = CartService.getIsSuccess;
        vm.setActivePriceId = setActivePriceId;

        vm.init = function (aQuantity) {
            vm.aQuantity = aQuantity;
        };

        /**
         * Sets active Price id Of Product Model
         *
         * @param priceId
         */
        function setActivePriceId(priceId) {
            vm.activePriceId = priceId;
        }

        /**
         * Prevent quantity be bigger than available quantity
         */
        vm.changeQuantity = function () {
            if (vm.quantity > vm.aQuantity) {
                vm.quantity = Number(vm.aQuantity);
            }
        }
    }
})();
