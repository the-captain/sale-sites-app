(function () {

    'use strict';

    angular
        .module('app.controllers')
        .controller('CartFormsController', CartFormsController);

    /**
     * @type {[string,string]}
     */
    CartFormsController.$inject = [
        '$scope',
        'requestsFactory',
        'CartService',
    ];

    /**
     * @param $scope
     * @param requestsFactory
     * @param CartService
     * @constructor
     */
    function CartFormsController($scope,
                                 requestsFactory,
                                 CartService) {
        var vm = this;
        vm.locations = null;
        vm.location = null;
        vm.selectedLocation = null;
        vm.price = 0;
        vm.delivery = 2;
        vm.showCreditOptions = true;

        /**
         * @param {string} site
         */
        vm.init = function (site) {
            vm.siteType = site;
            vm.price = CartService.getSumPriceBySiteType(site);
            vm.shippingPrice = CartService.getSumShippingPrice();
            vm.totalPrice = vm.price + vm.shippingPrice;
            requestsFactory.getLocationsBySiteType(vm.siteType).then(function (response) {
                vm.locations = response.data;
            });

            if (vm.creditCardEnabled !== undefined && vm.creditCardEnabled === 0) {
                vm.delivery = 1;
                vm.showCreditOptions = false;
                vm.paymentType = 1;
            };
        };

        $scope.$watch(
            function () {
                return vm.location
            },
            function () {
                selectLocation(vm.location);
            }
        );

        $scope.$watch(
            function () {
                return vm.delivery
            },
            function () {
                if (vm.delivery == 2) {
                    vm.paymentType = 2;
                }
            }
        );

        $scope.$watchCollection(
            function () {
                return CartService.getCart()
            },
            function () {
                vm.price = CartService.getSumPriceBySiteType(vm.siteType);
                vm.shippingPrice = CartService.getSumShippingPrice();
                vm.totalPrice = vm.price + vm.shippingPrice;
            }
        );

        /**
         * @param location
         * @returns {boolean}
         */
        function selectLocation(location) {
            if (vm.locations === undefined) {
                return false;
            }

            vm.selectedLocation = null;
            for (var item in vm.locations) {
                if (vm.locations[item].id == location) {
                    vm.selectedLocation = vm.locations[item];
                }
            }

            return true;
        }
    }
})();
