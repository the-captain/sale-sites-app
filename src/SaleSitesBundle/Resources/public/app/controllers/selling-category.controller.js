(function () {

    'use strict';

    angular
        .module('app.controllers')
        .controller('SellingCategoryController', SellingCategoryController).config(function($locationProvider) {
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false,
            rewriteLinks: false
        });
    });

    /**
     * @type {string[]}
     */
    SellingCategoryController.$inject = [
        '$scope',
        'sellingCategoryRequestFactory',
        'pagerService',
        '$location'
    ];

    /**
     * @param $scope
     * @param sellingCategoryRequestFactory
     * @param pagerService
     * @constructor
     */
    function SellingCategoryController($scope,
                                       sellingCategoryRequestFactory,
                                       pagerService,
                                       $location) {
        var vm = this;
        vm.categoryId = null;
        vm.items = '';
        vm.categoryId = '';
        vm.parameters = {};
        vm.sort = '';

        vm.canFetchData = 0;
        vm.pager = {};
        vm.pagerItems = 0;
        vm.setPage = setPage;

        vm.init = function(categoryId) {
            vm.categoryId = categoryId;
            vm.parameters = $location.search();
            sellingCategoryRequestFactory.getSellItemsForCategory(vm.categoryId, vm.parameters).then(function (response) {
                vm.items = response.data.data;
                vm.addQueryToItems();
                reinitPager(response);
            })
        };

        /**
         * @param {string} filterName
         * @param {string} value
         */
        vm.addFilter = function (filterName, value) {
            if (Array.isArray(vm.parameters[filterName])) {
                var index = vm.parameters[filterName].indexOf(value);
                if (index !== -1) {
                    vm.parameters[filterName].splice(index, 1);
                } else {
                    vm.parameters[filterName].push(value);
                }
            } else {
                vm.parameters[filterName] = [value];
            }

            vm.canFetchData = false;
            $location.search(vm.parameters);
            sellingCategoryRequestFactory.getSellItemsForCategory(vm.categoryId, vm.parameters).then(function (response) {
                vm.items = response.data.data;
                vm.addQueryToItems();
                reinitPager(response);
            })
        };

        vm.sortChange = function () {
            vm.parameters['sorting'] = vm.sort;
            sellingCategoryRequestFactory.getSellItemsForCategory(vm.categoryId, vm.parameters).then(function (response) {
                vm.items = response.data.data;
                reinitPager(response);
            })
        };

        /**
         * Set pagination page
         *
         * @param {number} page
         */
        function setPage(page) {
            if (page < 1 || page > vm.pager.totalPages) {
                return;
            }

            if (vm.pager.currentPage == page) {
                return;
            }

            // get pager object from service
            vm.pager = pagerService.GetPager(vm.pagerItems.length, page, 1);

            // get current page of items
            vm.items = vm.pagerItems.slice(vm.pager.startIndex, vm.pager.endIndex + 1);

            if (vm.canFetchData && vm.pager.currentPage) {
                vm.parameters['page'] = vm.pager.currentPage;
                sellingCategoryRequestFactory.getSellItemsForCategory(vm.categoryId, vm.parameters).then(function (response) {
                    vm.items = response.data.data;
                })
            }
        }

        vm.getCheckboxState = function(filterName, key) {
            if (vm.parameters[filterName] && (key === vm.parameters[filterName] || vm.parameters[filterName].indexOf(key) >= 0)) {
                return true;
            } else {
                return false;
            }
        };

        /**
         * @param {object} response
         */
        function reinitPager(response) {
            vm.canFetchData = false;
            /// ReInit Pager
            vm.pagerItems = _.range(1, parseInt(response.data.count) + 1);
            vm.pager = pagerService.GetPager(vm.pagerItems.length, 1, 9);
            vm.setPage(1);

            if (!vm.canFetchData) {
                vm.canFetchData = 1;
            }
        }

        vm.addQueryToItems = function() {
            if (vm.parameters) {
                var queryString = '';
                Object.getOwnPropertyNames(vm.parameters).forEach(function(val, idx, array) {
                    if (vm.parameters[val].length) {
                        val = val.split('[')[0];
                        queryString += 'filters[]=' + val + '&';
                    }
                });
                vm.items.forEach(function (item) {
                    item.link = item.link + '?' + queryString;
                });
            }
        }
    }
})();
