(function () {

    'use strict';

    angular
        .module('app.services')
        .service('CartService', CartService);

    /**
     * @type {[string,string,string]}
     */
    CartService.$inject = [
        'requestsFactory',
        '$q',
        'SiteContextHelper',
        'MessageService',
        'SELL_SITE_NAME',
        'BUYBACK_SITE_NAME',
        'REPAIR_SITE_NAME',
    ];

    /**
     * Cart Service
     *
     * @param requestsFactory
     * @param $q
     * @param SiteContextHelper
     * @param MessageService
     * @param SELL_SITE_NAME
     * @param BUYBACK_SITE_NAME
     * @param REPAIR_SITE_NAME
     * @returns {{addCartItem: addItem, removeCartItem: removeItem, deleteCartItem: deleteItem, getCart: getCart, getCheckoutSum: getCheckoutSum, getIsSuccess: getIsSuccess}}
     * @constructor
     */
    function CartService(requestsFactory,
                         $q,
                         SiteContextHelper,
                         MessageService,
                         SELL_SITE_NAME,
                         BUYBACK_SITE_NAME,
                         REPAIR_SITE_NAME) {

        var vm = this;

        vm.cart = [];
        vm.isSuccess = false;
        var checkoutSum = 0;
        updateCart();

        return {
            addCartItem: addItem,
            removeCartItem: removeItem,
            deleteCartItem: deleteItem,
            getCart: getCart,
            getCheckoutSum: getCheckoutSum,
            getIsSuccess: getIsSuccess,
            getSumPriceBySiteType: getSumPriceBySiteType,
            getSumShippingPrice: getSumShippingPrice,
        };

        /**
         * Gets checkout Sum
         *
         * @return {number}
         */
        function getCheckoutSum() {
            return checkoutSum;
        }

        /**
         * gets Success flag for Message
         */
        function getIsSuccess() {
            return vm.isSuccess;
        }

        /**
         * Adds Item to user cart
         *
         * @param priceId
         * @param quantity
         */
        function addItem(priceId, quantity) {
            if (quantity === undefined) {
                quantity = 1;
            }
            MessageService.clearMessages();
            requestsFactory.addCartItem(priceId, quantity).then(function (data) {
                MessageService.setSuccessCartAddMessage();
                var cart_item = findItem(vm.cart.items, priceId);
                if (!cart_item) {
                    updateCart();
                } else {
                    cart_item.quantity += quantity;
                    increaseItemsCartSum(cart_item, quantity);
                }
            }, function (message) {
                MessageService.setNotEnoughtCartAddMessage();
                console.info(message);
            });
        }

        /**
         * Removes Item from User Cart
         *
         * @param priceId
         * @param quantity
         */
        function removeItem(priceId, quantity) {
            if (quantity === undefined) {
                quantity = 1;
            }
            requestsFactory.removeCartItem(priceId, 1).then(function (data) {
                vm.isSuccess = true;
                var cart_item = findItem(vm.cart.items, priceId);

                if (cart_item.quantity > 1) {
                    cart_item.quantity -= 1;
                } else {
                    deleteItemById(vm.cart.items, priceId);
                }
                decreaseItemsCartSum(cart_item, quantity);
            }, function (message) {
                console.info(message);
            });
        }

        /**
         * Deletes Item From User Cart
         *
         * @param priceId
         */
        function deleteItem(priceId) {
            requestsFactory.deleteCartItem(priceId).then(function (data) {
                vm.isSuccess = true;
                var cart_item = findItem(vm.cart.items, priceId);
                decreaseItemsCartSum(cart_item, cart_item.quantity);
                deleteItemById(vm.cart.items, priceId);
            }, function (message) {
                console.info(message);
            });
        }

        /**
         * Gets Cart
         *
         * @return {Array|*}
         */
        function getCart() {
            return vm.cart;
        }

        /**
         * Sends Request for fresh Cart data
         */
        function updateCart() {
            var deferred = $q.defer();
            requestsFactory.getCart().then(function (data) {
                vm.cart = data.data.cart_data;
                if (data.data.message) {
                    MessageService.setReceivedMessage(data.data.message)
                }
                checkoutSum = vm.cart.repairSum + vm.cart.sellSum;
                deferred.resolve(data.data.cart_data);
            }, function (message) {
                deferred.reject(message);
            });
        }

        /**
         * Finds Item in Cart Array By Id
         *
         * @param itemArray
         * @param id
         * @return {*}
         */
        function findItem(itemArray, id) {
            return $.grep(itemArray, function (item) {
                return item.id == id;
            })[0];
        }

        /**
         * Finds Item and returns its key in array
         *
         * @param itemArray
         * @param id
         * @return {*}
         */
        function findItemByKey(itemArray, id) {
            for (var i = 0; i < itemArray.length; i++) {
                if (itemArray[i].id === id) {

                    return i;
                }
            }

            return false;
        }

        /**
         * Deletes Item from array by its key
         * @param itemArray
         * @param index
         */
        function deleteItemByKey(itemArray, index) {
            return itemArray.splice(index, 1);
        }

        /**
         * Deletes Item from array by its Id
         *
         * @param itemArray
         * @param priceId
         */
        function deleteItemById(itemArray, priceId) {
            return deleteItemByKey(itemArray, findItemByKey(itemArray, priceId));
        }

        /**
         * @param {Object} item
         * @param {number} quantity
         */
        function increaseItemsCartSum(item, quantity) {
            try {
                var priceValue = parseFloat(item.price) * quantity;

                if (SiteContextHelper.isSellingContext(item.site)) {
                    vm.cart.sellSum += priceValue;
                    checkoutSum += priceValue;
                    if (item.shippingPrice) {
                        vm.cart.shippingSum += parseFloat(item.shippingPrice) * quantity
                    }
                }

                if (SiteContextHelper.isRepairContext(item.site)) {
                    vm.cart.repairSum += priceValue;
                    checkoutSum += priceValue;
                }

                if (SiteContextHelper.isBuyBackContext(item.site)) {
                    vm.cart.incomeSum += priceValue;
                }
            } catch (e) {
                console.error(e);
            }
        }

        /**
         * @param {Object} item
         * @param {number} quantity
         */
        function decreaseItemsCartSum(item, quantity) {
            try {
                var priceValue = parseFloat(item.price) * quantity;

                if (SiteContextHelper.isSellingContext(item.site)) {
                    vm.cart.sellSum -= priceValue;
                    checkoutSum -= priceValue;
                    if (item.shippingPrice) {
                        vm.cart.shippingSum -= parseFloat(item.shippingPrice) * quantity
                    }
                }

                if (SiteContextHelper.isRepairContext(item.site)) {
                    vm.cart.repairSum -= priceValue;
                    checkoutSum -= priceValue;
                }

                if (SiteContextHelper.isBuyBackContext(item.site)) {
                    vm.cart.incomeSum -= priceValue;
                }
            } catch (e) {
                console.error(e);
            }
        }

        /**
         * Get Price By Site Type
         *
         * @param {string} site
         * @returns {number}
         */
        function getSumPriceBySiteType(site) {
            if (site === SELL_SITE_NAME) {
                return vm.cart.sellSum;
            }

            if (site === REPAIR_SITE_NAME) {
                return vm.cart.repairSum;
            }

            if (site === BUYBACK_SITE_NAME) {
                return vm.cart.incomeSum;
            }
        }

        /**
         * Get sum of shipping prices
         * @returns {number}
         */
        function getSumShippingPrice() {
            return vm.cart.shippingSum
        }
    }
})();
