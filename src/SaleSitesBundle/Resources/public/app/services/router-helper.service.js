(function () {

    'use strict';

    angular
        .module('app.services')
        .service('routeHelper', routeHelper);
    /**
     * @type {[string]}
     */
    routeHelper.$inject = [
        'BASE_PATH',
        'BASE_API_PATH',
    ];

    /**
     * Route Helper
     * @param BASE_PATH
     * @param BASE_API_PATH
     */
    function routeHelper(BASE_PATH, BASE_API_PATH) {
        /**
         * Returns Url to Add Item to Cart
         *
         * @param item_id
         * @param quantity
         * @return {string}
         */
        this.addCartItemUrl = function(item_id, quantity) {
            return BASE_PATH + 'cart/add/' + item_id + '/' + quantity;
        };

        /**
         * Returns Url to Remove Item from Cart
         *
         * @param item_id
         * @param quantity
         * @return {string}
         */
        this.removeCartItemUrl = function(item_id, quantity) {
            return BASE_PATH + 'cart/remove/' + item_id + '/' + quantity;
        };

        /**
         * Returns Url to Delete Item from Cart
         * @param item_id
         * @return {string}
         */
        this.deleteCartItemUrl = function(item_id) {
            return BASE_PATH + 'cart/delete/' + item_id;
        };

        /**
         * Returns Url to get Cart data
         * @return {string}
         */
        this.getCartUrl = function() {
            return BASE_PATH + 'cart/get-cart';
        };

        /**
         * Returns Url to get Locations by site data
         * @param {string} site
         * @returns {string}
         */
        this.getLocationsUrlBySiteType = function (site) {
            return BASE_API_PATH + 'locations/' + site;
        };

        /**
         * Returns Url to get all Locations data
         * @returns {string}
         */
        this.getLocationsUrl = function () {
            return BASE_API_PATH + 'locations/all';
        };
    }
})();
