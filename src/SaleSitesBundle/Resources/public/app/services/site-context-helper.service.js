(function () {

    'use strict';

    angular
        .module('app.services')
        .service('SiteContextHelper', SiteContextHelper);

    /**
     *
     * @type {[string,string,string,string,string,string]}
     */
    SiteContextHelper.$inject = [
        'SELL_SITE_ID',
        'BUYBACK_SITE_ID',
        'REPAIR_SITE_ID',
        'SELL_SITE_NAME',
        'BUYBACK_SITE_NAME',
        'REPAIR_SITE_NAME',
    ];

    /**
     * SiteContextHelper
     * @param SELL_SITE_ID
     * @param BUYBACK_SITE_ID
     * @param REPAIR_SITE_ID
     * @param SELL_SITE_NAME
     * @param BUYBACK_SITE_NAME
     * @param REPAIR_SITE_NAME
     * @constructor
     */
    function SiteContextHelper(SELL_SITE_ID,
                               BUYBACK_SITE_ID,
                               REPAIR_SITE_ID,
                               SELL_SITE_NAME,
                               BUYBACK_SITE_NAME,
                               REPAIR_SITE_NAME) {
        /**
         * If it is Selling Site
         *
         * @param site_id
         * @return {boolean}
         */
        this.isSellingContext = function (site_id) {
            return site_id == SELL_SITE_ID;
        };

        /**
         * If it is BuyBack Site
         *
         * @param site_id
         * @return {boolean}
         */
        this.isBuyBackContext = function (site_id) {
            return site_id == BUYBACK_SITE_ID;
        };

        /**
         * If it is Repair Site
         *
         * @param site_id
         * @return {boolean}
         */
        this.isRepairContext = function (site_id) {
            return site_id == REPAIR_SITE_ID;
        };

        /**
         * Get site name
         * @param {number} typeId
         * @returns {string}
         */
        this.getSiteNameByTypeId = function (typeId) {
            if (typeId === SELL_SITE_ID) {
                return SELL_SITE_NAME;
            }

            if (typeId === BUYBACK_SITE_ID) {
                return BUYBACK_SITE_NAME;
            }

            if (typeId === REPAIR_SITE_ID) {
                return REPAIR_SITE_NAME;
            }

            throw new Error('TypeId is wrong');
        }
    }
})();
