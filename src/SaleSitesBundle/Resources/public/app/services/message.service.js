(function () {

    'use strict';

    angular
        .module('app.services')
        .service('MessageService', MessageService);

    /**
     * @type {Array}
     */
    MessageService.$inject = [];

    /**
     * @return {{getMessages: getMessages, clearMessages: clearMessages, addMessage: addMessage, setSuccessCartAddMessage: setSuccessCartAddMessage}}
     * @constructor
     */
    function MessageService() {

        var vm = this;
        vm.messages = [];

        return {
            getMessages: getMessages,
            clearMessages: clearMessages,
            addMessage: addMessage,
            setSuccessCartAddMessage: setSuccessCartAddMessage,
            setNotEnoughtCartAddMessage: setNotEnoughtCartAddMessage,
            setReceivedMessage: setReceivedMessage
        };

        /**
         * Sets successful message when item is added to cart
         */
        function setSuccessCartAddMessage() {
            setMessage([
                {
                    messageClass: 'alert-info',
                    messageText: 'This item was successfully added to your cart!'
                }
            ]);
        }

        /**
         * Sets successful message when item is added to cart
         */
        function setNotEnoughtCartAddMessage() {
            setMessage([
                {
                    messageClass: 'alert-info',
                    messageText: 'There is no available items'
                }
            ]);
        }

        /**
         * Sets successful message when item is added to cart
         */
        function setReceivedMessage(message) {
            setMessage([
                {
                    messageClass: 'alert-info',
                    messageText:  message
                }
            ]);
        }

        /**
         * Gets messages
         *
         * @return {Array|*}
         */
        function getMessages() {
            return vm.messages;
        }

        /**
         * Sets messages
         *
         * @param messageArray
         */
        function setMessage(messageArray) {
            vm.messages = messageArray;
        }

        /**
         * Adds message
         *
         * @param messageClass
         * @param messageText
         */
        function addMessage(messageClass, messageText) {
            vm.messages.push(
                {
                    messageClass: messageClass,
                    messageText: messageText
                }
            );
        }

        /**
         * Cleares messages
         */
        function clearMessages() {
            vm.messages = [];
        }
    }
})();
