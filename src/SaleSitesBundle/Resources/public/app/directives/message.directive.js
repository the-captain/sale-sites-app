(function () {

    'use strict';

    angular
        .module('app.directives')
        .directive('messageBlock', messageBlock);

    /**
     * @type {Array}
     */
    messageBlock.$inject = [];

    /**
     * @return {{restrict: string, controller: string, controllerAs: string, templateUrl: string, bindToController: boolean}}
     */
    function messageBlock() {
        return {
            restrict: 'EA',
            controller: 'messageController',
            controllerAs: 'vmMessage',
            templateUrl: 'message-template.html',
            bindToController: true
        };
    }
})();
