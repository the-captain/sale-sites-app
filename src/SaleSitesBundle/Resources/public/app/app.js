(function () {

    'use strict';

    angular
        .module('SaleSitesApp', [
            'app.controllers',
            'app.services',
            'app.factories',
            'app.directives',
            // 'app.components',
            'PaypalExpress'
        ]);

    angular.module('app.common', [
        'ngAnimate',
        'ngMessages'
    ]);

    angular.module('app.controllers', ['app.common']);
    angular.module('app.services', ['app.common']);
    angular.module('app.factories', ['app.common']);
    angular.module('app.directives', ['app.common']);
    // angular.module('app.components', ['app.common']);

    angular.module('app.common').constant('BASE_PATH', '/');
    angular.module('app.common').constant('BASE_API_PATH', '/api/');
    angular.module('app.common').constant('SELL_SITE_ID', 1);
    angular.module('app.common').constant('BUYBACK_SITE_ID', 2);
    angular.module('app.common').constant('REPAIR_SITE_ID', 3);
    angular.module('app.common').constant('SELL_SITE_NAME', 'sell');
    angular.module('app.common').constant('BUYBACK_SITE_NAME', 'buyback');
    angular.module('app.common').constant('REPAIR_SITE_NAME', 'repair');
})();

(function () {

'use strict';

    angular.module('paypal-express.common', [
        'ngAnimate',
        'ngMessages'
    ]);

    angular.module('paypal-express.directives', [
        'paypal-express.common'
    ]);

    angular
        .module('PaypalExpressApp', [
           'paypal-express.directives'
    ]);

})();