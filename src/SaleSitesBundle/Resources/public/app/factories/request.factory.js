(function() {

    'use strict';

    angular
        .module('app.factories')
        .factory('requestsFactory', requestsFactory);

    /**
     * @type {[string,string,string]}
     */
    requestsFactory.$inject = [
        '$http',
        '$q',
        'routeHelper'
    ];

    /**
     * requestsFactory
     *
     * @param $http
     * @param $q
     * @param routeHelper
     * @return {{addCartItem: addCartItemRequest, removeCartItem: removeCartItemRequest, deleteCartItem: deleteCartItemRequest, getCart: getCartRequest}}
     */
    function requestsFactory(
        $http,
        $q,
        routeHelper
    ) {
        var actionError = 'An error occurred during get/add/delete/remove action, please try again later';

        return {
            addCartItem: addCartItemRequest,
            removeCartItem: removeCartItemRequest,
            deleteCartItem: deleteCartItemRequest,
            getCart: getCartRequest,
            getLocations: getLocations,
            getLocationsBySiteType: getLocationsBySiteType,
        };

        /**
         * Request For adding item to user cart
         *
         * @param priceId
         * @param quantity
         */
        function addCartItemRequest(priceId, quantity) {
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: routeHelper.addCartItemUrl(priceId, quantity),
            }).then(function(data){
                deferred.resolve(data);
            }, function(data){
                deferred.reject({
                    message: actionError,
                    code: data.status
                });
            });

            return deferred.promise;
        }

        /**
         * Request For removing item from user cart
         *
         * @param priceId
         * @param quantity
         */
        function removeCartItemRequest(priceId, quantity) {
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: routeHelper.removeCartItemUrl(priceId, quantity),
            }).then(function(data){
                deferred.resolve(data);
            }, function(){
                deferred.reject({
                    message: actionError
                });
            });

            return deferred.promise;
        }

        /**
         * Request For deleting item from user cart
         *
         * @param priceId
         */
        function deleteCartItemRequest(priceId) {
            var deferred = $q.defer();

            $http({
                method: 'DELETE',
                url: routeHelper.deleteCartItemUrl(priceId),
            }).then(function(data){
                deferred.resolve(data);
            }, function(){
                deferred.reject({
                    message: actionError
                });
            });

            return deferred.promise;
        }

        /**
         * Request to get User Cart
         */
        function getCartRequest() {
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: routeHelper.getCartUrl()
            }).then(function(data){
                deferred.resolve(data);
            }, function(){
                deferred.reject({
                    message: actionError
                });
            });

            return deferred.promise;
        }

        /**
         * Request to get User locations by site type
         *
         * @param {string} site
         */
        function getLocationsBySiteType(site) {
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: routeHelper.getLocationsUrlBySiteType(site),
            }).then(function(data){
                deferred.resolve(data);
            }, function(){
                deferred.reject({
                    message: actionError
                });
            });

            return deferred.promise;
        }

        /**
         * Request to get all User locations
         */
        function getLocations() {
            var deferred = $q.defer();

            $http({
                method: 'POST',
                url: routeHelper.getLocationsUrl(),
            }).then(function(data){
                deferred.resolve(data);
            }, function(){
                deferred.reject({
                    message: actionError
                });
            });

            return deferred.promise;
        }
    }
})();
