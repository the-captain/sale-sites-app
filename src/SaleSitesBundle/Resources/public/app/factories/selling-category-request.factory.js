(function() {

    'use strict';

    angular
        .module('app.factories')
        .factory('sellingCategoryRequestFactory', sellingCategoryRequestFactory);

    /**
     * @type {[string]}
     */
    sellingCategoryRequestFactory.$inject = [
        '$http'
    ];

    /**
     * requestsFactory
     *
     * @param $http
     * @return {{getSellItemsForCategory: getSellItemsForCategory}}
     */
    function sellingCategoryRequestFactory(
        $http
    ) {
        return {
            getSellItemsForCategory: getSellItemsForCategory
        };

        /**
         * @param category
         * @param filters
         *
         * @returns {*}
         */
        function getSellItemsForCategory(category, filters) {
            return $http({
                method: 'GET',
                url: '/api/prices/' + category,
                params: filters ? filters : {}
            });
        }
    }
})();
