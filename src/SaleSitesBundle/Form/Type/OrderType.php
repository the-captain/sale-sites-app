<?php

namespace SaleSitesBundle\Form\Type;

use SaleSitesBundle\Service\LocationService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use ThreeWebOneEntityBundle\Entity\Helper\SiteTypeInterface;
use ThreeWebOneEntityBundle\Entity\Order\OrderDeliveryTypeInterface;
use ThreeWebOneEntityBundle\Entity\Order\OrderPaymentTypeInterface;
use ThreeWebOneEntityBundle\Entity\UserConfig\Location;
use ThreeWebOneEntityBundle\Entity\User;

class OrderType extends AbstractType
{
    /**
     * @var LocationService
     */
    private $locationService;

    /**
     * @var int
     */
    private $siteType;

    /**
     * @var User
     */
    private $user;

    /**
     * OrderType constructor.
     * @param LocationService $locationService
     */
    public function __construct(LocationService $locationService)
    {
        $this->locationService = $locationService;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->siteType = $options['site_type'];
        $this->user = $options['user'];

        if ($this->siteType == SiteTypeInterface::SALE) {
            $deliveryTypeChoice = OrderDeliveryTypeInterface::SHIPPING;
            $paymentTypeChoice = OrderPaymentTypeInterface::CREDIT_CARD;
        } else {
            $deliveryTypeChoice = OrderDeliveryTypeInterface::PICK_UP;
            $paymentTypeChoice = OrderPaymentTypeInterface::CASH;
        }

        $builder
            ->add('firstName',
                TextType::class,
                [
                    'data' => $this->user ? $this->user->getFirstname() : '',
                ]
            )
            ->add('lastName', TextType::class,
                [
                    'data' => $this->user ? $this->user->getLastname() : '',
                ]
            )
            ->add(
                'email',
                EmailType::class,
                [
                    'data' => $this->user ? $this->user->getEmail() : '',
                ]
            )
            ->add('phone', TextType::class,
                [
                    'data' => $this->user ? $this->user->getPhone() : '',
                ]
            )
            ->add(
                'deliveryType',
                ChoiceType::class,
                [
                    'choices' => [
                        'Pick Up' => OrderDeliveryTypeInterface::PICK_UP,
                        'Shipping' => OrderDeliveryTypeInterface::SHIPPING,
                    ],
                    'data' => $deliveryTypeChoice,
                    'required' => true,
                    'label' => false,
                    'expanded' => true,
                    'multiple' => false,
                ]
            )
            ->add(
                'paymentType',
                ChoiceType::class,
                [
                    'choices' => [
                        'Cash' => OrderPaymentTypeInterface::CASH,
                        'Credit Card' => OrderPaymentTypeInterface::CREDIT_CARD,
                    ],
                    'data' => $paymentTypeChoice,
                    'required' => true,
                    'label' => false,
                    'expanded' => true,
                    'multiple' => false,
                ]
            )
            ->add(
                'pickupLocation',
                EntityType::class,
                [
                    'class' => Location::class,
                    'choices' => $this->locationService->getLocationEntitiesBySite($this->siteType),
                    'required' => false,
                    'choice_label' => 'name',
                ]
            )
            ->add(
                'address1',
                TextType::class,
                [
                    'required' => false,
                    'mapped' => false,
                ]
            )
            ->add(
                'address2',
                TextType::class,
                [
                    'required' => false,
                    'mapped' => false,
                ]
            )
            ->add(
                'city',
                TextType::class,
                [
                    'required' => false,
                    'mapped' => false,
                ]
            )
            ->add(
                'state',
                TextType::class,
                [
                    'required' => false,
                    'mapped' => false,
                ]
            )
            ->add(
                'zipCode',
                TextType::class,
                [
                    'required' => false,
                    'mapped' => false,
                ]
            )
            ->add(
                'country',
                TextType::class,
                [
                    'required' => false,
                    'mapped' => false,
                ]
            )
            ->add(
                'shippingAddress',
                TextType::class,
                [
                    'required' => false,
                ]
            )
        ->add(
            'termsChecked',
            CheckboxType::class,
            [
                'required' => true,
            ]
        );

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'ThreeWebOneEntityBundle\Entity\Order\Order',
                'csrf_protection' => false,
                'site_type' => null,
                'user' => null
            ]
        );
    }
}
