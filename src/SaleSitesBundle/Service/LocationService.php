<?php

namespace SaleSitesBundle\Service;

use Doctrine\ORM\EntityManager;
use SaleSitesBundle\EventListener\ClientSubscriber;
use ThreeWebOneEntityBundle\Entity\UserConfig\Location;

class LocationService
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var ClientSubscriber
     */
    private $siteListener;

    /**
     * LocationService constructor.
     * @param EntityManager $entityManager
     * @param ClientSubscriber $siteListener
     */
    public function __construct(EntityManager $entityManager, ClientSubscriber $siteListener)
    {
        $this->em = $entityManager;
        $this->siteListener = $siteListener;
    }

    /**
     * @param int $siteType
     * @return array
     */
    public function getLocationsBySite(int $siteType) : array
    {
        $locations = $this->getUserLocations();
        $result = [];
        /** @var Location $location */
        foreach ($locations as $key => $location) {
            if (in_array($siteType, $location->getServices())) {
                $result[$key] = $this->locationToArrayFormat($location);
            }
        }

        return $result;
    }

    /**
     * @param int|null $siteType
     * @return array
     */
    public function getLocationEntitiesBySite(?int $siteType) : array
    {
        $locations = $this->getUserLocations();
        if (!$siteType) {
            return $locations;
        }

        $result = [];
        /** @var Location $location */
        foreach ($locations as $key => $location) {
            if (in_array($siteType, $location->getServices())) {
                $result[] = $location;
            }
        }

        return $result;
    }

    /**
     * Get User Locations In Array Format
     *
     * @return array
     */
    public function getUserLocationsInArrayFormat() : array
    {
        $locations = $this->getUserLocations();

        $result = [];
        foreach ($locations as $key => $location) {
            $result[$key] = $this->locationToArrayFormat($location);
        }

        return $result;
    }

    /**
     * @return Location[]
     */
    private function getUserLocations() : array
    {
        $user = $this->siteListener->getUser();
        $locationRepo = $this->em->getRepository(Location::class);

        return $locationRepo->getUserLocations($user);
    }

    /**
     * @param Location $location
     * @return array
     */
    private function locationToArrayFormat(Location $location) : array
    {
        return [
            'id' => $location->getId(),
            'name' => $location->getName(),
            'description' => $location->getDescription(),
            'hours' => $location->getHours(),
            'phone' => $location->getPhone(),
            'services' => $location->getServices(),
        ];
    }
}
