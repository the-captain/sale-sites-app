<?php

namespace SaleSitesBundle\Service;

use ThreeWebOneEntityBundle\Entity\Helper\SiteTypeInterface;

class SiteTypeService
{
    /**
     * @param string $site
     * @return int
     * @throws \InvalidArgumentException
     */
    public function getSiteTypeByString(string $site) : int
    {
        if (array_key_exists($site, SiteTypeInterface::SITE_TYPE_ARRAY)) {
            return SiteTypeInterface::SITE_TYPE_ARRAY[$site];
        }

        throw new \InvalidArgumentException(sprintf('Site type \'%s\' doesn\'t exist', $site));
    }
}
