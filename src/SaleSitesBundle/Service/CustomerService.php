<?php

namespace SaleSitesBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use ThreeWebOneEntityBundle\Entity\Customer\Customer;
use ThreeWebOneEntityBundle\Entity\Order\Order;

class CustomerService
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var TraceableEventDispatcher
     */
    private $eventDispatcher;

    /**
     * @var TwigEmailService
     */
    private $mailer;

    /**
     * CustomerService constructor.
     *
     * @param RequestStack $requestStack
     * @param EntityManager $entityManager
     * @param TokenStorageInterface $tokenStorage
     * @param TraceableEventDispatcher $eventDispatcher
     * @param TwigEmailService $mailer
     */
    public function __construct(
        RequestStack $requestStack,
        EntityManager $entityManager,
        TokenStorageInterface $tokenStorage,
        TraceableEventDispatcher $eventDispatcher,
        TwigEmailService $mailer
    )
    {
        $this->requestStack = $requestStack;
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
        $this->eventDispatcher = $eventDispatcher;
        $this->mailer = $mailer;
    }

    /**
     * Create And Login
     *
     * @param Order $order
     * @return Customer
     */
    public function createAndLogin(Order $order): Customer
    {
        $customer = $this->tokenStorage->getToken()->getUser();

        // check customer authentication
        if ($customer instanceof Customer) {
            return $customer;
        }

        $customer = new Customer();
        $customer->setEmail($order->getEmail())
            ->setUsername($order->getFirstName())
            ->setFirstname($order->getFirstName())
            ->setLastname($order->getLastName())
            ->setPhone($order->getPhone())
            ->setEnabled(Customer::ENABLED)
            ->setOwner($order->getOwner());

        $password = $this->createPassword($order->getEmail());
        $customer->setPassword($password);

        $this->entityManager->persist($customer);
        $this->entityManager->flush();

        $this->mailer->sendRegistrationEmail($order->getEmail(), $password);
        $this->customLogin($customer);

        return $customer;
    }

    /**
     * Custom Login
     *
     * @param Customer $customer
     */
    public function customLogin(Customer $customer)
    {
        $token = new UsernamePasswordToken($customer, $customer->getPassword(), "public", $customer->getRoles());
        $this->tokenStorage->setToken($token);

        $request = $this->requestStack->getCurrentRequest();
        $event = new InteractiveLoginEvent($request, $token);
        $this->eventDispatcher->dispatch("security.interactive_login", $event);
    }

    /**
     * Customer Exist
     *
     * @param string $email
     * @param int $ownerId
     * @return Customer|null
     */
    public function customerExist(string $email, int $ownerId): ?customer
    {
        $customerRepo = $this->entityManager->getRepository(Customer::class);

        return $customerRepo->getCustomerByEmailAndOwnerId($email, $ownerId);
    }

    /**
     * Create Password
     *
     * @param String $email
     * @return string
     */
    private function createPassword(String $email): String
    {
        return substr(str_shuffle(md5($email)), 0, 7) ?? '';
    }
}
