<?php

namespace SaleSitesBundle\Service;

use Doctrine\ORM\EntityManager;
use SaleSitesBundle\EventListener\ClientSubscriber;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\Filter;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemImage;
use ThreeWebOneEntityBundle\Entity\Price;
use ThreeWebOneEntityBundle\Entity\PriceType;
use ThreeWebOneEntityBundle\Entity\User;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class SellingItemsService
{
    const LIMIT = 9;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var ClientSubscriber
     */
    private $clientSubscriber;

    /**
     * @var UploaderHelper
     */
    private $uploaderHelper;

    /**
     * @var Router
     */
    private $router;

    /**
     * SellingItemsService constructor.
     *
     * @param EntityManager $em
     * @param ClientSubscriber $clientSubscriber
     * @param UploaderHelper $uploaderHelper
     * @param Router $router
     */
    public function __construct(
        EntityManager $em,
        ClientSubscriber $clientSubscriber,
        UploaderHelper $uploaderHelper,
        Router $router
    ) {
        $this->em = $em;
        $this->clientSubscriber = $clientSubscriber;
        $this->uploaderHelper = $uploaderHelper;
        $this->router = $router;
    }

    /**
     * @param Category $category
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function getSellingItems(Category $category, Request $request) : array
    {
        $offset = $request->query->getInt('page', 1) - 1;

        $user = $this->clientSubscriber->getUser();
        $priceRepo = $this->em->getRepository(Price::class);
        $data = $priceRepo
            ->getPricesByUserCategory(
                $user,
                $category,
                $request->query->all(),
                self::LIMIT,
                self::LIMIT * $offset
            );
        $count = $priceRepo->getCountPricesByUserCategory(
            $user,
            $category,
            $request->query->all()
        );

        return [
            'data' => $this->prepareItems($data),
            'count' => $count,
        ];
    }

    /**
     * @param array $items
     * @return array
     */
    public function prepareItems(array $items): array
    {
        $result = [];
        /** @var Price $item */
        foreach ($items as $item) {
            $id = $item->getId();
            $inventoryItem = $item->getInventoryItem();
            $image = null;
            if ($inventoryItem->getImage()) {
                $image = $this->uploaderHelper
                    ->asset($inventoryItem->getImage(), 'imageFile', InventoryItemImage::class);
            }
            $result[] = [
                'id' => $id,
                'link' => $this->router->generate(
                    'selling_category_product',
                    [
                        'id' => $id,
                        'slug' => $inventoryItem->getSlug()
                    ]
                ),
                'image' => $image,
                'title' => $inventoryItem->getTitle(),
                'grade' => $item->getPriceType()->getTitle(),
                'value' => $item->getValue() / 100,
            ];
        }

        return $result;
    }
}
