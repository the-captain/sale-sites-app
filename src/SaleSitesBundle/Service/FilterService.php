<?php

namespace SaleSitesBundle\Service;

use Doctrine\ORM\EntityManager;
use ThreeWebOneEntityBundle\Entity\Category;
use ThreeWebOneEntityBundle\Entity\Filter;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Entity\PriceType;
use ThreeWebOneEntityBundle\Entity\User;

class FilterService
{
    /**
     * Filter grade
     */
    const GRADE = 'grade';

    /**
     * @var  EntityManager
     */
    private $em;

    /**
     * FilterService constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param Category $category
     * @param User $user
     *
     * @return array
     */
    public function getFilters(Category $category, User $user): array
    {
        $filters = $this->em->getRepository(Filter::class)
            ->getUserCategoryModelsFilters($user, $category);
        $grades = $this->em->getRepository(PriceType::class)
            ->getUserCategoryModelsPriceTypes($user, $category);

        $filtersArray = [];
        $filtersArray['filters'] = $this->prepareFilter($filters);
        $filtersArray = array_merge($filtersArray, $this->prepareFilter($grades));

        return $filtersArray;
    }

    /**
     * @param array $entities
     *
     * @return array
     */
    public function prepareFilter(array $entities): array
    {

        $filters = [];
        foreach ($entities as $entity) {
            if ($entity instanceof Filter && $entity->getParent()) {
                if (isset($filters[$entity->getParent()->getId()])) {
                    array_push($filters[$entity->getParent()->getId()]['filters'], $entity);
                } else {
                    $filters[$entity->getParent()->getId()]['filters'] = [$entity];
                    $filters[$entity->getParent()->getId()]['parent'] = $entity->getParent();
                }
            }
            if ($entity instanceof PriceType) {
                $filters[self::GRADE][$entity->getId()] = $entity->getTitle();
            }
        }

        return $filters;
    }

    /**
     * @param InventoryItem $inventoryItem
     * @return array
     */
    public function groupMultipleModelFilters(InventoryItem $inventoryItem)
    {
        $filters = [];
        /** @var Filter $entity */
        foreach ($inventoryItem->getFilters() as $entity) {
            if ($entity->getParent() && $entity->getParent()->getIsMultiSelect()) {
                if (isset($filters[$entity->getParent()->getId()])) {
                    array_push($filters[$entity->getParent()->getId()]['filters'], $entity);
                } else {
                    $filters[$entity->getParent()->getId()]['filters'] = [$entity];
                    $filters[$entity->getParent()->getId()]['parent'] = $entity->getParent();
                }
            }
        }

        return $filters;
    }
}
