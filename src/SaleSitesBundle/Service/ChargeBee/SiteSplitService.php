<?php

namespace SaleSitesBundle\Service\ChargeBee;

use InvalidArgumentException;

/**
 * Class SiteSplitService
 */
class SiteSplitService
{
    const MULTI_WEBSITE_LANDING_PAGE = 'multi_website_landing_page';

    const MULTI_WEBSITE_LANDING_TITLE = 'multi_website_landing_title';

    const INVENTORY_WEBSITE_LISTENING_FORM = 'inventory_website_listening_form';

    const TITLE_DESCRIPTION_BUY = 'title_description_buy';

    const TITLE_DESCRIPTION_SALE = 'title_description_sale';

    const TITLE_DESCRIPTION_REPAIR = 'title_description_repair';

    const SLIDER_DISPLAY_BUY = 'slider_display_buy';

    const SLIDER_DISPLAY_SALE = 'slider_display_sale';

    const SLIDER_DISPLAY_REPAIR = 'slider_display_repair';

    const OFFERS = 'offers';

    const SOCIAL = 'social';

    const SEO_BUY = 'seo_buy';

    const SEO_SALE = 'seo_sale';

    const SEO_REPAIR = 'seo_repair';

    const INVENTORY = 'inventory';

    const ORDER_BUY = 'order_buy';

    const ORDER_SALE = 'order_sale';

    const ORDER_REPAIR = 'order_repair';

    const WEBSITE_SALE = 'website_sale';

    const WEBSITE_BUY = 'website_buy';

    const WEBSITE_REPAIR = 'website_repair';

    const RESTRICTIONS = [
        self::MULTI_WEBSITE_LANDING_PAGE,
        self::MULTI_WEBSITE_LANDING_TITLE,
        self::TITLE_DESCRIPTION_BUY,
        self::TITLE_DESCRIPTION_SALE,
        self::TITLE_DESCRIPTION_REPAIR,
        self::SLIDER_DISPLAY_BUY,
        self::SLIDER_DISPLAY_SALE,
        self::SLIDER_DISPLAY_REPAIR,
        self::OFFERS,
        self::SOCIAL,
        self::SEO_BUY,
        self::SEO_SALE,
        self::SEO_REPAIR,
        self::INVENTORY,
        self::INVENTORY_WEBSITE_LISTENING_FORM,
        self::ORDER_BUY,
        self::ORDER_SALE,
        self::ORDER_REPAIR,
        self::WEBSITE_SALE,
        self::WEBSITE_BUY,
        self::WEBSITE_REPAIR,
    ];

    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * @var array
     */
    private $rules;

    /**
     * SiteSplitService constructor.
     *
     * @param SubscriptionService $subscriptionService
     * @param array               $rules
     */
    public function __construct(SubscriptionService $subscriptionService, array $rules)
    {
        $this->subscriptionService = $subscriptionService;

        $this->rules = $rules;
    }

    /**
     * @param string $restriction
     *
     * @return bool
     */
    public function isAccessible(string $restriction): bool
    {
        $restriction = strtolower($restriction);

        if (in_array($restriction, static::RESTRICTIONS) === false) {
            throw new InvalidArgumentException('unknown restriction: <![CDATA[' . $restriction . ']]>');
        }

        if (isset($this->rules[$restriction]) === false) {
            throw new \RuntimeException('missed restriction in config file: <![CDATA[' . $restriction . ']]>');
        }

        $rulesSet = $this->rules[$restriction];

        foreach ($rulesSet as $rule) {
            $ruleResult = true;
            foreach ($rule as $subscribedRestriction => $expectedSubscriptionStatus) {
                if ($expectedSubscriptionStatus !== $this->getNamedRestrictionValue($subscribedRestriction)) {
                    $ruleResult = false;
                    continue;
                }
            }

            if ($ruleResult === true) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    protected function getNamedRestrictionValue(string $name): bool
    {
        $name = strtolower($name);
        if ($name === 'buy') {
            return $this->subscriptionService->isBuySubscribed();
        }

        if ($name === 'sale') {
            return $this->subscriptionService->isSellSubscribed();
        }

        if ($name === 'repair') {
            return $this->subscriptionService->isRepairSubscribed();
        }

        throw new InvalidArgumentException('unknown named restriction <![CDATA[' . $name . ']]>');
    }

    /**
     * @return int
     */
    public function getSubscribedItemsCount()
    {
        return $this->subscriptionService->getSubscribedItemsCount();
    }
}
