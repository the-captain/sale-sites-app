<?php

namespace SaleSitesBundle\Service\ChargeBee;

use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Model\UserInterface;
use RuntimeException;
use SaleSitesBundle\EventListener\ClientSubscriber;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelInterface;
use ThreeWebOneEntityBundle\Entity\Billing\Subscription;
use ThreeWebOneEntityBundle\Entity\Billing\SubscriptionInterface;
use ThreeWebOneEntityBundle\Entity\Domain\ClientDomain;
use ThreeWebOneEntityBundle\Repository\Domain\ClientDomainRepository;

/**
 * Class SubscriptionService.
 */
class SubscriptionService
{
    /**
     * @var ClientSubscriber
     */
    protected $ClientSubscriber;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var KernelInterface
     */
    protected $kernel;

    /**
     * @var Subscription
     */
    private $activeSubscription;

    /**
     * @var bool
     */
    private $buySubscribed = false;

    /**
     * @var bool
     */
    private $repairSubscribed = false;

    /**
     * @var bool
     */
    private $sellSubscribed = false;

    /**
     * @var int
     */
    private $subscribedItemsCount = 0;

    /**
     * SubscriptionService constructor.
     *
     * @param ClientSubscriber         $ClientSubscriber
     *
     * @param EntityManagerInterface $entityManager
     * @param KernelInterface        $kernel
     */
    public function __construct(
        ClientSubscriber $ClientSubscriber,
        EntityManagerInterface $entityManager,
        KernelInterface $kernel
    ) {
        $this->ClientSubscriber = $ClientSubscriber;
        $this->entityManager = $entityManager;
        $this->kernel = $kernel;

        $this->setAllFeaturesUnsubscribed();
    }

    /**
     * Sets all features as disabled
     */
    private function setAllFeaturesUnsubscribed()
    {
        $this->setSubscribedItemsCount(0);
        $this->setSellSubscribed(false);
        $this->setBuySubscribed(false);
        $this->setRepairSubscribed(false);
    }

    /**
     * @return int
     */
    public function getSubscribedItemsCount(): int
    {
        return $this->subscribedItemsCount;
    }

    /**
     * @return bool
     */
    public function hasSubscribedItemsCount(): bool
    {
        return null !== $this->subscribedItemsCount;
    }

    /**
     * @param int $subscribedItemsCount
     *
     * @return SubscriptionService
     */
    public function setSubscribedItemsCount(int $subscribedItemsCount): self
    {
        $this->subscribedItemsCount = $subscribedItemsCount;

        return $this;
    }

    /**
     * @return bool
     */
    public function isBuySubscribed(): bool
    {
        return $this->buySubscribed;
    }

    /**
     * @return bool
     */
    public function hasBuySubscribed(): bool
    {
        return null !== $this->buySubscribed;
    }

    /**
     * @param bool $buySubscribed
     *
     * @return SubscriptionService
     */
    public function setBuySubscribed(bool $buySubscribed): self
    {
        $this->buySubscribed = $buySubscribed;

        return $this;
    }

    /**
     * @return bool
     */
    public function isRepairSubscribed(): bool
    {
        return $this->repairSubscribed;
    }

    /**
     * @return bool
     */
    public function hasRepairSubscribed(): bool
    {
        return null != $this->repairSubscribed;
    }

    /**
     * @param bool $repairSubscribed
     *
     * @return SubscriptionService
     */
    public function setRepairSubscribed(bool $repairSubscribed): self
    {
        $this->repairSubscribed = $repairSubscribed;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSellSubscribed(): bool
    {
        return $this->sellSubscribed;
    }

    /**
     * @return bool
     */
    public function hasSellSubscribed(): bool
    {
        return null !== $this->sellSubscribed;
    }

    /**
     * @param bool $sellSubscribed
     *
     * @return SubscriptionService
     */
    public function setSellSubscribed(bool $sellSubscribed): self
    {
        $this->sellSubscribed = $sellSubscribed;

        return $this;
    }

    /**
     * Executed when a request is received
     *
     * @param GetResponseEvent $event
     * @throws \Exception
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $route = $event->getRequest()->attributes->get('_route');

        if ($event->isMasterRequest() && $route != ClientSubscriber::ROUTE_HEALTHCHECK) {
            $host = $event->getRequest()->getHost();

            /** @var ClientDomainRepository $domainRepository */
            $domainRepository = $this->entityManager->getRepository(ClientDomain::class);
            $user = $domainRepository->getUserByDomain($host);

            if (!is_null($user)) {
                $userId = $user->getId();
            } elseif (in_array($this->kernel->getEnvironment(), ['dev', 'staging'])) {
                $userId = $this->getClientSubscriber()->getDefaultUserId();
            }

            if (null !== $userId) {
                //initialize only when correct request
                $this->initialize();
            }
        }
    }

    /**
     * Initialize service.
     *
     * @throws \Exception
     */
    protected function initialize()
    {
        $user = $this->getClientSubscriber()->getUser();
        if (null !== $user) {
            if (true === $user->hasRole(UserInterface::ROLE_DEFAULT) &&
                false === $user->hasRole(UserInterface::ROLE_SUPER_ADMIN)
            ) {
                $subscriptions = [];
                if ($user->hasBillingCustomer()) {
                    $subscriptions = $user->getBillingCustomer()->getSubscriptions();
                }

                /** @var Subscription $subscription */
                foreach ($subscriptions as $subscription) {
                    if (SubscriptionInterface::STATUS_CANCELLED != $subscription->getStatus()) {
                        if ($subscription->getStatus() === SubscriptionInterface::STATUS_TRIAL &&
                            $user->getBillingCustomer()->isTrialPeriodAvailable()) {
                            $this->setActiveSubscription($subscription);
                        } elseif ($subscription->getStatus() !== SubscriptionInterface::STATUS_TRIAL) {
                            $this->setActiveSubscription($subscription);
                        }
                    }
                }

                if ($this->hasActiveSubscription()) {
                    $this->applySubscriptionState($this->getActiveSubscription());
                }
            } elseif ($user->hasRole(UserInterface::ROLE_SUPER_ADMIN)) {
                //this type of user does not need any subscription
                $this->setAllFeaturesSubscribed();
            }
        } else {
            throw new RuntimeException('wrong context, there is no user provided');
        }
    }

    /**
     * @return ClientSubscriber
     */
    public function getClientSubscriber(): ClientSubscriber
    {
        return $this->ClientSubscriber;
    }

    /**
     * @return bool
     */
    public function hasClientSubscriber(): bool
    {
        return null !== $this->ClientSubscriber;
    }

    /**
     * @param ClientSubscriber|null $ClientSubscriber
     *
     * @return SubscriptionService
     */
    public function setClientSubscriber(ClientSubscriber $ClientSubscriber = null): SubscriptionService
    {
        $this->ClientSubscriber = $ClientSubscriber;

        return $this;
    }

    /**
     * @param Subscription $subscription
     *
     * @return void
     */
    private function applySubscriptionState(Subscription $subscription)
    {
        $subscribedItems = $subscription->getItems();
        $subscribedItemsCount = 0;

        if ($subscribedItems[SubscriptionInterface::ITEM_SELL]) {
            $this->setSellSubscribed(true);
            ++$subscribedItemsCount;
        }

        if ($subscribedItems[SubscriptionInterface::ITEM_BUY]) {
            $this->setBuySubscribed(true);
            ++$subscribedItemsCount;
        }

        if ($subscribedItems[SubscriptionInterface::ITEM_REPAIR]) {
            $this->setRepairSubscribed(true);
            ++$subscribedItemsCount;
        }

        $this->setSubscribedItemsCount($subscribedItemsCount);
    }

    /**
     * @return Subscription
     */
    public function getActiveSubscription(): Subscription
    {
        return $this->activeSubscription;
    }

    /**
     * @return bool
     */
    public function hasActiveSubscription(): bool
    {
        return !empty($this->activeSubscription);
    }

    /**
     * @param Subscription $activeSubscription
     *
     * @return SubscriptionService
     */
    public function setActiveSubscription(Subscription $activeSubscription): self
    {
        $this->activeSubscription = $activeSubscription;

        return $this;
    }

    /**
     * Sets all features as enabled
     *
     * Usually called for admin user
     */
    private function setAllFeaturesSubscribed()
    {
        $this->setSubscribedItemsCount(3);
        $this->setSellSubscribed(true);
        $this->setBuySubscribed(true);
        $this->setRepairSubscribed(true);
    }
}
