<?php

namespace SaleSitesBundle\Service;

use Doctrine\ORM\EntityManager;
use SaleSitesBundle\EventListener\ClientSubscriber;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use ThreeWebOneEntityBundle\Entity\Customer\Customer;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItem;
use ThreeWebOneEntityBundle\Entity\Inventory\InventoryItemBarcode;
use ThreeWebOneEntityBundle\Entity\Order\Order;
use ThreeWebOneEntityBundle\Entity\Order\OrderItem;
use ThreeWebOneEntityBundle\Entity\Order\OrderStatusInterface;
use ThreeWebOneEntityBundle\Entity\Price;

class OrderService
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var ClientSubscriber
     */
    private $ClientSubscriber;

    /**
     * OrderService constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     * @param EntityManager $entityManager
     * @param ClientSubscriber $ClientSubscriber
     */
    public function __construct(
        TokenStorageInterface $tokenStorage,
        EntityManager $entityManager,
        ClientSubscriber $ClientSubscriber
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $entityManager;
        $this->ClientSubscriber = $ClientSubscriber;
    }

    /**
     * Create Order
     *
     * @param array $data
     * @param Order $order
     * @param Customer $customer
     * @return Order
     */
    public function createOrder($data, Order $order, Customer $customer = null): Order
    {
        $order->setStatus(Order::IN_PROGRESS)
            ->setOwner($this->ClientSubscriber->getUser());

        $askedQuantity = [];
        foreach ($data['items'] as $key => $price) {
            $this->manageOrderItems($price, $data['counts'][$key], $order);
            $askedQuantity[$price->getId()] = $data['counts'][$key] ?? 0;
        }
        $order->setAskedQuantity($askedQuantity);

        if (is_object($this->getUser())) {
            $order->setCustomer($this->getUser());
        }
        if ($customer instanceof Customer) {
            $order->setCustomer($customer);
        }

        $this->entityManager->persist($order);
        $this->entityManager->flush();

        return $order;
    }

    /**
     * Manage Order Items
     *
     * @param Price $price
     * @param int $quantity
     * @param Order $order
     */
    private function manageOrderItems(Price $price, int $quantity, Order $order)
    {
        $type = $order->getSiteType();
        if ($type == Order::SALE) {
            $inventoryItem = $price->getInventoryItem();
            $count = 0;
            /** @var InventoryItemBarcode $barcode */
            foreach ($inventoryItem->getInventoryItemBarcodes() as $barcode) {
                if ($count == $quantity) {
                    break;
                }
                if ($barcode->getStatus() === InventoryItemBarcode::NEW_BARCODE) {
                    $orderItem = (new OrderItem())->setBarcode($barcode);
                    $order->addPurchaseBarcode($barcode);
                    $barcode->setSellPrice($price->getValue());
                    $this->entityManager->persist($orderItem);
                    $count += 1;
                }
            }
        }

        if (in_array($type, [Order::BUYBACK, Order::REPAIR])) {
            $inventoryItem = (new InventoryItem())
                ->setStatus(InventoryItem::STATUS_PREPARED)
                ->setPriceType($price->getPriceType())
                ->setPrice($price)
                ->setPurchasePrice($price->getValue())
                ->setQuantity($quantity)
                ->setOwner($order->getOwner());
            $orderItem = (new OrderItem())->setInventoryItem($inventoryItem);
            $order->addItem($inventoryItem);
            $this->entityManager->persist($orderItem);
            $this->entityManager->persist($inventoryItem);
        }
    }

    /**
     * Get User
     *
     * @return Customer
     */
    public function getUser()
    {
        return $this->tokenStorage->getToken()->getUser();
    }
}
