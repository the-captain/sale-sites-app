<?php

namespace SaleSitesBundle\Service;

use Doctrine\ORM\EntityManager;
use ThreeWebOneEntityBundle\Entity\Price;
use ThreeWebOneEntityBundle\Entity\PriceType;

class CartItemsService
{
    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var array
     */
    private $items = [];

    /**
     * @var array
     */
    private $counts = [];

    /**
     * @var int
     */
    private $incomeSum = 0;

    /**
     * @var int
     */
    private $sellSum = 0;

    /**
     * @var int
     */
    private $shippingSum = 0;

    /**
     * @var int
     */
    private $repairSum = 0;

    /**
     * @param CartService $cartService
     * @param EntityManager $entityManager
     */
    public function __construct(
        CartService $cartService,
        EntityManager $entityManager
    ) {
        $this->cartService = $cartService;
        $this->entityManager = $entityManager;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getPricesFromSession()
    {
        $cartItemsArray = $this->cartService->getCartItemsArray();
        $cartItems = $this->parseCartArrayItems($cartItemsArray);

        foreach ($cartItemsArray as $key => $item) {
            $price = $this->findItemById($cartItems, $item['id']);

            if ($price->getPriceType()->getSite() !== $item ['siteId']) {
                $this->cartService->deleteItem($price->getId());

                throw new \Exception(
                    sprintf('Item %s SiteType id doesn\'t match with cookie one and will be deleted', $price->getId())
                );
            }

            $quantity = (!empty($item['quan']) ? $item['quan'] : 1);
            $this->items[] = $price;
            $this->counts[] = $quantity;

            if ($price->getPriceType()->getSite() == PriceType::SALE) {
                $this->sellSum += $price->getValue() * $quantity;
                $inventoryItem = $price->getInventoryItem();
                if ($inventoryItem) {
                    $this->shippingSum += $inventoryItem->getShippingPrice() * $quantity;
                }
            }

            if ($price->getPriceType()->getSite() == PriceType::REPAIR) {
                $this->repairSum += $price->getValue() * $quantity;
            }

            if ($price->getPriceType()->getSite() == PriceType::BUYBACK) {
                $this->incomeSum += $price->getValue() * $quantity;
            }
        }

        return [
            'items' => $this->items,
            'counts' => $this->counts,
            'sellSum'=> $this->sellSum,
            'shippingSum'=> $this->shippingSum,
            'repairSum'=> $this->repairSum,
            'incomeSum' => $this->incomeSum,
        ];
    }

    /**
     * @param $siteId
     * @return array
     * @throws \Exception
     */
    public function getSiteCartItems($siteId)
    {
        $cartItemsArray = $this->cartService->getCartSiteItemsArray($siteId);
        $cartItems = $this->parseCartArrayItems($cartItemsArray);

        $items = [];
        $counts = [];

        foreach ($cartItemsArray as $key => $item) {
            $price = $this->findItemById($cartItems, $item['id']);

            if ($price->getPriceType()->getSite() !== $siteId) {
                $this->cartService->deleteItem($price->getId());

                throw new \Exception(
                    sprintf('Item %s SiteType id doesn\'t match with cookie one and will be deleted', $price->getId())
                );
            } else {
                $items[] = $price;
                $counts[] = (!empty($item['quan']) ? $item['quan'] : 1);
            }
        }

        return [
            'items' => $items,
            'counts' => $counts,
        ];
    }

    /**
     * Parses CartArrayItems and returns Prices array
     *
     * @param array $cartItemsArray
     *
     * @return array
     */
    private function parseCartArrayItems(array $cartItemsArray)
    {
        $priceIds = [];
        foreach ($cartItemsArray as $key => $item) {
            $priceIds [] = $item['id'];
        }

        $items = $this->entityManager
            ->getRepository('ThreeWebOneEntityBundle:Price')
            ->getCartPrices($priceIds);

        return $items;
    }

    /**
     * Finds Item in array
     *
     * @param $array
     * @param $id
     *
     * @return Price
     *
     * @throws \Exception
     */
    private function findItemById($array, $id)
    {
        foreach ($array as $price) {
            if ($id == $price->getId()) {
                return $price;
            }
        }

        $this->cartService->deleteItem($id);

        throw new \Exception(sprintf('Item %s doesn\'t exist and will be deleted', $id));
    }
}
