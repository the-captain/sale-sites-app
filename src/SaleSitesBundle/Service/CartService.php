<?php

namespace SaleSitesBundle\Service;

use Doctrine\ORM\EntityManager;
use SaleSitesBundle\EventListener\ClientSubscriber;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use ThreeWebOneEntityBundle\Entity\Customer\Customer;
use ThreeWebOneEntityBundle\Entity\Helper\SiteTypeInterface;
use ThreeWebOneEntityBundle\Entity\Price;

/**
 * Class CartService
 * @package SaleSitesBundle\Service
 */
class CartService
{
    /**
     * Cookie name
     */
    const COOKIE_NAME = 'items_array';

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var array
     */
    private $items = [];

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var ClientSubscriber
     */
    private $ClientSubscriber;

    /**
     * CartService constructor.
     *
     * @param RequestStack $requestStack
     * @param EntityManager $entityManager
     * @param TokenStorageInterface $tokenStorage
     * @param ClientSubscriber $ClientSubscriber
     */
    public function __construct(
        RequestStack $requestStack,
        EntityManager $entityManager,
        TokenStorageInterface $tokenStorage,
        ClientSubscriber $ClientSubscriber
    ) {
        $this->requestStack = $requestStack;
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
        $this->ClientSubscriber = $ClientSubscriber;
    }

    /**
     * @return array
     */
    public function getCartItemsArray()
    {
        $this->parseCookies();

        return $this->items;
    }

    /**
     * @param $siteId
     * @return array
     */
    public function getCartSiteItemsArray($siteId)
    {
        $this->parseCookies();

        $siteItems = [];

        foreach ($this->items as $item) {
            if ($item['siteId'] == $siteId) {
                $siteItems[] = $item;
            }
        }

        return $siteItems;
    }

    /**
     * Parse cookies and set the items array.
     */
    private function parseCookies()
    {
        if (!$this->items) {
            $user = $this->getUser();

            if ($user instanceof Customer && $user->getCart() && !empty($user->getCart()['items'])) {
                foreach ($this->getUser()->getCart()['items'] as $item) {
                    $this->items[] = $item;
                }
            } else {
                $request = $this->requestStack->getCurrentRequest();
                if ($request->cookies->has($this->getItemCookie())) {
                    $data = json_decode($request->cookies->get($this->getItemCookie()), true);
                    if (!empty($data['items'])) {
                        foreach ($data['items'] as $item) {
                            $this->items[] = $item;
                        }
                    }
                }
            }
        }
    }

    /**
     * @param Price $price
     * @param int $quan
     * @param int $siteId
     * @return array
     */
    public function addItem(Price $price, int $quan = 1, int $siteId)
    {
        $this->parseCookies();

        $id = $price->getId();
        $aQuantity = $price->getQuantity();
        $code = Response::HTTP_OK;

        if (empty($this->items)) {
            $this->items[] = ['id' => $id, 'quan' => $quan, 'siteId' => $siteId];

            return ['new_cart', $code];
        }

        foreach ($this->items as $key => $item) {
            if ($item['id'] == $id) {
                $newQuantity = $this->items[$key]['quan'] + $quan;
                if ($newQuantity > $aQuantity && $price->getPriceType()->getSite() == SiteTypeInterface::SALE) {
                    $this->items[$key]['quan'] = $aQuantity;
                    $code = Response::HTTP_BAD_REQUEST;
                }
                $this->items[$key]['quan'] = $newQuantity <= $aQuantity ? $newQuantity : $aQuantity;

                return ['added', $code];
            }
        }

        if ($quan > $aQuantity) {
            $quan = $aQuantity;
            $code = Response::HTTP_BAD_REQUEST;
        }

        $this->items[] = ['id' => $id, 'quan' => $quan, 'siteId' => $siteId];

        return ['new_item', $code];
    }

    /**
     * @param int $id
     * @param int $quan
     * @return int
     */
    public function removeItem(int $id, int $quan = 1): int
    {
        $this->parseCookies();

        if (empty($this->items)) {
            return 'empty_cart';
        }

        foreach ($this->items as $key => $item) {
            if ($item['id'] == $id && $item['quan'] <= $quan) {
                $this->deleteItem($item['id']);

                return true;
            }
            if ($item['id'] == $id && $item['quan'] > $quan) {
                $this->items[$key]['quan'] -= $quan;

                return true;
            }
        }

        return false;
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function deleteItem(int $id): bool
    {
        $this->parseCookies();
        foreach ($this->items as $key => $item) {
            if ($item['id'] == $id) {
                unset($this->items[$key]);

                return true;
            }
        }

        return false;
    }

    /**
     * @return Cookie
     */
    public function generateCookies()
    {
        $data = ['items' => $this->items];
        if ($this->getUser() instanceof Customer) {
            $this->getUser()->setCart($data);
            $this->save($this->getUser());
        }

        return new Cookie(
            $this->getItemCookie(),
            json_encode($data, JSON_FORCE_OBJECT),
            new \DateTime('+1 year')
        );
    }

    /**
     * @return Cookie
     */
    public function clearCart()
    {
        $this->items = [];

        if ($this->getUser()) {
            $this->getUser()->setCart([]);
            $this->save($this->getUser());
        }

        return new Cookie(
            $this->getItemCookie(),
            json_encode(['items' => []]),
            new \DateTime('+1 year')
        );
    }

    /**
     * @param $siteId
     * @return bool
     */
    public function clearSiteCart($siteId)
    {
        $this->parseCookies();

        foreach ($this->items as $key => $item) {
            if ($item['siteId'] == $siteId) {
                unset($this->items[$key]);
            }
        }

        return true;
    }

    /**
     * @return bool|Customer
     */
    private function getUser()
    {
        $token = $this->tokenStorage->getToken();
        $user = null;
        if (!is_null($token)) {
            $user = $token->getUser();
        }
        if (!is_null($user)) {
            return $user;
        }

        return false;
    }

    /**
     * @param Customer $user
     */
    private function save(Customer $user)
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    /**
     * Gets Cart Items cookie name
     *
     * @return string
     */
    private function getItemCookie()
    {
        return self::COOKIE_NAME . '_' . $this->ClientSubscriber->getUserId();
    }
}
