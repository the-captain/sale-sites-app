<?php

namespace SaleSitesBundle\Service;

use SaleSitesBundle\EventListener\ClientSubscriber;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use ThreeWebOneEntityBundle\Sluggable\SluggableInterface;

class SlugService
{
    /**
     * @var ClientSubscriber
     */
    protected $listener;

    /**
     * SlugService constructor.
     *
     * @param ClientSubscriber $listener
     */
    public function __construct(ClientSubscriber $listener)
    {
        $this->listener = $listener;
    }

    /**
     * @param SluggableInterface $entity
     * @param Request $request
     *
     * @return null|RedirectResponse
     */
    public function checkSlug(SluggableInterface $entity, Request $request)
    {
        if ($entity->getSlug() !== $request->attributes->get('slug')) {
            return new RedirectResponse(
                $this->listener->getPath($request->get('_route'), [
                    'id' => $request->attributes->get('id'),
                    'slug' => $entity->getSlug(),
                ])
            );
        }

        return null;
    }
}
