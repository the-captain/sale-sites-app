<?php

namespace SaleSitesBundle\Service;

use Symfony\Component\Translation\TranslatorInterface;

class TwigEmailService
{
    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * TwigEmailService constructor.
     *
     * @param \Twig_Environment $twig
     * @param \Swift_Mailer $mailer
     * @param TranslatorInterface $translator
     */
    public function __construct(
        \Twig_Environment $twig,
        \Swift_Mailer $mailer,
        TranslatorInterface $translator
    ) {
        $this->twig = $twig;
        $this->mailer = $mailer;
        $this->translator = $translator;
    }

    /**
     * Send Registration Email
     *
     * @param String $email
     * @param String $password
     */
    public function sendRegistrationEmail(String $email, String $password)
    {
        $this->sendMessage(
            '@SaleSites/emails/custom_registration.html.twig',
            [
                'email' => $email,
                'password' => $password
            ],
            ['noreply@3web1.com' => '3web1'],
            $email
        );
    }

    /**
     * Send Message
     *
     * @param string $templateName
     * @param array  $context
     * @param array  $fromEmail
     * @param string $toEmail
     */
    protected function sendMessage($templateName, $context, $fromEmail, $toEmail)
    {
        $template = $this->twig->load($templateName);
        $subject = $template->renderBlock('subject', $context);
        $textBody = $template->renderBlock('body_text', $context);

        $htmlBody = '';

        if ($template->hasBlock('body_html', $context)) {
            $htmlBody = $template->renderBlock('body_html', $context);
        }

        $message = (new \Swift_Message())
            ->setSubject($subject)
            ->setFrom($fromEmail)
            ->setTo($toEmail);

        if (!empty($htmlBody)) {
            $message->setBody($htmlBody, 'text/html')
                ->addPart($textBody, 'text/plain');
        } else {
            $message->setBody($textBody);
        }

        $this->mailer->send($message);
    }
}
