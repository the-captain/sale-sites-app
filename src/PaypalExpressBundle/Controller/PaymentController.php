<?php

namespace PaypalExpressBundle\Controller;

use PaypalExpressBundle\Packet\Payment\Create;
use PaypalExpressBundle\Packet\Payment\Execute;
use PaypalExpressBundle\Packet\Payment\Shared\Address;
use PaypalExpressBundle\Packet\Payment\Shared\Payer;
use PaypalExpressBundle\Packet\Payment\Shared\RedirectUrls;
use PaypalExpressBundle\Packet\Payment\Shared\Transaction;
use PaypalExpressBundle\Service\PaypalExpressService;
use SaleSitesBundle\Service\CartItemsService;
use SaleSitesBundle\Service\CartService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGenerator;
use ThreeWebOneEntityBundle\Entity\Helper\SiteTypeInterface;
use ThreeWebOneEntityBundle\Entity\Order\Order;
use ThreeWebOneEntityBundle\Entity\Order\OrderPaymentTypeInterface;
use ThreeWebOneEntityBundle\Entity\Price;
use ThreeWebOneEntityBundle\Entity\UserConfig\Location;

/**
 * Class PaymentController.
 */
class PaymentController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     * @throws \Exception
     */
    public function createPaymentAction(Request $request): Response
    {
        /** @var CartService $cartService */
        $cartService = $this->get('sale_sites.cart');

        /** @var CartItemsService $cartItemsService */
        $cartItemsService = $this->get('sale_sites.cart.items');

        /** @var PaypalExpressService $ppExpress */
        $ppExpress = $this->get('paypal_express_service');

        $paymentData = new Create();

        $transactions = [];

        $cartItemsDescription = $cartItemsService->getPricesFromSession();
        $cartItemsDescriptions = [];
        /** @var Price $itemDescription */
        foreach ($cartItemsDescription['items'] as $itemDescription) {
            $cartItemsDescriptions[$itemDescription->getId()] = $itemDescription;
        }

        $totalAmount = $cartItemsDescription['sellSum'] / 100;
        $totalShipping = $cartItemsDescription['shippingSum'] / 100;

        $itemsList = new Transaction\ItemsList();
        $items = [];
        foreach ($cartService->getCartItemsArray() as $cartItem) {
            $itemId = $cartItem['id'];
            $itemQuantity = $cartItem['quan'];

            /** @var Price $cartItemDescription */
            $cartItemDescription = $cartItemsDescriptions[$itemId];

            $inventoryItem = $cartItemDescription->getInventoryItem();

            $transaction = new Transaction();

            $amount = new Transaction\Amount();

            $amountDetails = new Transaction\Amount\Details();
            $item = new Transaction\ItemsList\Item();

            $itemPrice = $inventoryItem->getPriceValue() / 100;
            $item
                ->setName($inventoryItem->getTitle())
                ->setDescription($inventoryItem->getDescription())
//                ->setTax('0.02')
                ->setQuantity($itemQuantity)
                ->setPrice($itemPrice)
                ->setCurrency('USD');

            $items[] = $item;
        }

        $itemsList->setItems($items);

        $amountDetails
            ->setShipping($totalShipping)
            ->setSubtotal($totalAmount)
            ->setGiftWrap('0.00')
            ->setHandlingFee('0.00')
            ->setInsurance('0.00')
            ->setTax('0.00')
            ->setShippingDiscount('0.00');

        $amount
            ->setCurrency('USD')
            ->setDetails($amountDetails)
            ->setTotal($totalShipping + $totalAmount);

        $paymentOptions = new Transaction\PaymentOptions();

        $paymentOptions->setAllowedPaymentMethod(Transaction\PaymentOptions::INSTANT_FUNDING_SOURCE);

        $transaction
            ->setAmount($amount)
            ->setItemsList($itemsList)
            ->setPaymentOptions($paymentOptions);

        $transactions[] = $transaction;

        $payer = (new Payer());

        $payer->setPaymentMethod(Payer::PAYMENT_METHOD_PAYPAL);

        $redirectUrl = new RedirectUrls();

        $redirectUrl
            ->setReturnUrl($this->generateUrl('paypal_express_return_payed', [], UrlGenerator::ABSOLUTE_URL))
            ->setCancelUrl($this->generateUrl('paypal_express_return_cancelled', [], UrlGenerator::ABSOLUTE_URL));

        $paymentData
            ->setPayer($payer)
            ->setIntent(Create::INTENT_SALE)
            ->setTransactions($transactions)
            ->setRedirectUrls($redirectUrl)
            ->setNotToPayer('Contact us for any questions on your order.');

        return new JsonResponse($ppExpress->createPayment($paymentData));
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function executePaymentAction(Request $request): Response
    {
        /** @var PaypalExpressService $ppExpress */
        $ppExpress = $this->get('paypal_express_service');

        $paymentData = json_decode($request->getContent());

        $executePaymentCommand = new Execute();

        $executePaymentCommand
            ->setPayerId($paymentData->payerID)
            ->setPaymentId($paymentData->paymentID);

        $paymentResult = $ppExpress->executePayment($executePaymentCommand);

        if ($paymentResult['state'] != 'approved') {
            throw new \LogicException('Impossible to complete payment');
        }

        return $this->createOrder($paymentResult, $paymentData->userEntered);
    }

    /**
     * @param $paymentData
     * @return Response
     * @throws \Exception
     */
    private function createOrder($paymentData, $payerEntered = null): Response
    {
        $payerInfo = $paymentData['payer']['payer_info'];
        $owner = $this->container->get('sale_sites.site_subscriber')->getUser();
        $site = 'sell';
        $data = $this->get('sale_sites.cart.items')->getSiteCartItems(SiteTypeInterface::SALE);
        $owner = $this->get('sale_sites.site_subscriber')->getUser();

        if (empty($data['items'])) {
            $this->addFlash('order_fail', 'Your have no items to make sale site order');
            $this->redirectToRoute(
                'sale_sites_checkout_page',
                ['userId' => $owner->getId()]
            );
        }

        $order = new Order();

        $order
            ->setFirstName($payerInfo['first_name'])
            ->setLastName($payerInfo['last_name'])
            ->setEmail($payerInfo['email'])
            ->setExternalId($paymentData['id'])
            ->setDeliveryType(Order::SHIPPING)
            ->setTermsChecked(true)
            ->setShippingAddress($this->createAddress($payerInfo['shipping_address']))
            ->setPaymentType(OrderPaymentTypeInterface::PAYPAL)
            ->setOwner($owner);

        if ($payerEntered) {
            $order->setFirstName($payerEntered->firstName)
                ->setLastName($payerEntered->lastName)
                ->setEmail($payerEntered->email)
                ->setPhone($payerEntered->phone)
                ->setDeliveryType($payerEntered->delivery == Order::SHIPPING ? Order::SHIPPING : Order::PICK_UP);

            if ($payerEntered->delivery == Order::PICK_UP && $payerEntered->pickup) {
                $location = $this->getDoctrine()->getRepository(Location::class)
                    ->find($payerEntered->pickup);
                $order->setPickupLocation($location);
            }
        }

        if (!empty($paymentData['transactions'])) {
            $paid = 0;
            foreach ($paymentData['transactions'] as $transaction) {
                $paid += $transaction['amount']['total'];
            }
            $order->setAmountPaid($paid * 100);
        }

        $customer = $this->get('sale_sites.service.customer')->customerExist($order->getEmail(), $owner->getId());
        if (!$customer) {
            $this->get('sale_sites.service.customer')->createAndLogin($order);
        }
        $order->setSiteType(SiteTypeInterface::SALE);
        $this->get('sale_sites.order')->createOrder($data, $order, $customer);
        $this->addFlash('order_success', $order->getId());

        if ($customer) {
            $response = new JsonResponse(
                [
                    'redirect' => $this->generateUrl(
                        'fos_user_security_login',
                        [
                            '_target_path' => $this->generateUrl('sale_sites_checkout_page')
                        ]
                    ),
                ]
            );
        } else {
            $response = new JsonResponse(
                [
                    'redirect' => $this->generateUrl(
                        'sale_sites_checkout_page',
                        ['userId' => $owner->getId()]
                    ),
                ]
            );
        }

        $cart = $this->get('sale_sites.cart');
        $cart->clearSiteCart(SiteTypeInterface::SALE);
        $response->headers->setCookie($cart->generateCookies());

        return $response;
    }

    /**
     * @param array $data
     * @return string
     */
    private function createAddress(array $data)
    {
        $result = '';
        foreach ($data as $key => $value) {
            $result .= sprintf('%s: %s;', $key, $value);
        }

        return $result;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function payedAction(Request $request): Response
    {
        return new JsonResponse([]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function cancelledAction(Request $request): Response
    {
        return new JsonResponse([]);
    }
}
