<?php

namespace PaypalExpressBundle\Packet\OAuthV2;

use PaypalExpressBundle\Contract\PacketInterface;

/**
 * Class GetAccessToken.
 */
class GetAccessToken implements PacketInterface
{
    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $secret;

    /**
     * @var string
     */
    private $language;

    /**
     * {@inheritdoc}
     */
    public function getHttpHeaders(): array
    {
        $headers = ['Accept: application/json'];

        if ($this->hasLanguage()) {
            $headers[] = 'Accept-Language: ' . $this->getLanguage();
        }

        return $headers;
    }

    /**
     * @return string|null
     */
    public function getLanguage(): ?string
    {
        return $this->language;
    }

    /**
     * @return bool
     */
    public function hasLanguage(): bool
    {
        return !is_null($this->language);
    }

    /**
     * @param string|null $language
     *
     * @return GetAccessToken
     */
    public function setLanguage(string $language = null): self
    {
        $this->language = $language;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getUserPassword(): string
    {
        $userPassword = '';
        if ($this->hasClientId()) {
            $userPassword = $this->getClientId();
        }
        if ($this->hasSecret()) {
            $userPassword = $userPassword . ':' . $this->getSecret();
        }

        return $userPassword;
    }

    /**
     * @return string|null
     */
    public function getClientId(): ?string
    {
        return $this->clientId;
    }

    /**
     * @return bool
     */
    public function hasClientId(): bool
    {
        return !is_null($this->clientId);
    }

    /**
     * @param string|null $clientId
     *
     * @return GetAccessToken
     */
    public function setClientId(string $clientId = null): self
    {
        $this->clientId = $clientId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSecret(): ?string
    {
        return $this->secret;
    }

    /**
     * @return bool
     */
    public function hasSecret(): bool
    {
        return !is_null($this->secret);
    }

    /**
     * @param string|null $secret
     *
     * @return GetAccessToken
     */
    public function setSecret(string $secret = null): self
    {
        $this->secret = $secret;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getData(): array
    {
        return [
            'grant_type' => 'client_credentials',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getHttpUri(): string
    {
        return 'oauth2/token';
    }

    /**
     * {@inheritdoc}
     */
    public function getHttpMethod(): string
    {
        return self::METHOD_AUTH;
    }
}
