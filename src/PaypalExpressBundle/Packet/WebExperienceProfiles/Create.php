<?php

namespace PaypalExpressBundle\Packet\WebExperienceProfiles;

use PaypalExpressBundle\Packet\AbstractAccessTokenPacket;

/**
 * Class Create.
 */
class Create extends AbstractAccessTokenPacket
{
    /**
     * {@inheritdoc}
     */
    public function getHttpHeaders(): array
    {
        return self::METHOD_POST;
    }

    /**
     * {@inheritdoc}
     */
    public function getData(): array
    {
        // TODO: Implement getBody() method.
    }

    public function getHttpUri(): string
    {
        // TODO: Implement getHttpUri() method.
    }

    public function getHttpMethod(): string
    {
        // TODO: Implement getHttpMethod() method.
    }
}
