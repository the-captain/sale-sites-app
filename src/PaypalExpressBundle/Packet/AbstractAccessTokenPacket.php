<?php

namespace PaypalExpressBundle\Packet;

use PaypalExpressBundle\Contract\PacketInterface;

/**
 * Class AbstractAccessTokenPacket.
 */
abstract class AbstractAccessTokenPacket implements PacketInterface
{
    /**
     * @var string
     */
    private $accessToken;

    /**
     * {@inheritdoc}
     */
    public function getUserPassword(): string
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function getHttpHeaders(): array
    {
        $headers = [];
        if ($this->hasAccessToken()) {
            $headers[] = 'Authorization: Bearer ' . $this->getAccessToken();
        }

        return $headers;
    }

    /**
     * @return string|null
     */
    public function getAccessToken(): ?string
    {
        return $this->accessToken;
    }

    /**
     * @return bool
     */
    public function hasAccessToken(): bool
    {
        return !is_null($this->accessToken);
    }

    /**
     * @param string|null $accessToken
     *
     * @return AbstractAccessTokenPacket
     */
    public function setAccessToken(string $accessToken = null): AbstractAccessTokenPacket
    {
        $this->accessToken = $accessToken;

        return $this;
    }
}
