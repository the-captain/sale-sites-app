<?php

namespace PaypalExpressBundle\Packet;

use PaypalExpressBundle\Contract\PacketResponseInterface;

class ResponseResolver implements PacketResponseInterface
{
    private $response = [];

    /**
     * ResponseResolver constructor.
     *
     * @param string $response
     */
    public function __construct(string $response)
    {
        if (!empty($response)) {
            $this->response = json_decode($response, true);
        }
    }

    /**
     * @return array
     */
    public function getResponseData(): array
    {
        return $this->response;
    }
}
