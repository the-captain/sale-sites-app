<?php

namespace PaypalExpressBundle\Packet\Payment;

use PaypalExpressBundle\Packet\AbstractAccessTokenPacket;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ShowDetails.
 */
class ShowDetails extends AbstractAccessTokenPacket
{
    /**
     * @var string
     * @Assert\Type(type="string")
     */
    private $paymentId;

    /**
     * @return array
     */
    public function getData(): array
    {
        return [];
    }

    /**
     * @return string
     */
    public function getHttpUri(): string
    {
        return sprintf("payments/payment/%s", $this->getPaymentId());
    }

    /**
     * @return string|null
     */
    public function getPaymentId(): ?string
    {
        return $this->paymentId;
    }

    /**
     * @return bool
     */
    public function hasPaymentId(): bool
    {
        return !is_null($this->paymentId);
    }

    /**
     * @param string|null $paymentId
     *
     * @return ShowDetails
     */
    public function setPaymentId(string $paymentId = null): ShowDetails
    {
        $this->paymentId = $paymentId;

        return $this;
    }

    /**
     * @return string
     */
    public function getHttpMethod(): string
    {
        return self::METHOD_GET;
    }
}
