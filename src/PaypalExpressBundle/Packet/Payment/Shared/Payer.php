<?php

namespace PaypalExpressBundle\Packet\Payment\Shared;

use PaypalExpressBundle\Contract\PacketDataInterface;
use PaypalExpressBundle\Packet\Payment\Shared\Payer\FundingInstrument;
use PaypalExpressBundle\Packet\Payment\Shared\Payer\PayerInfo;
use Symfony\Component\Validator\Constraints as Assert;

class Payer implements PacketDataInterface
{
    const PAYMENT_METHOD_PAYPAL = 'paypal';

    const STATUS_VERIFIED = 'VERIFIED';

    const STATUS_UNVERIFIED = 'UNVERIFIED';

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Choice(choices={Payer::PAYMENT_METHOD_PAYPAL})
     */
    private $paymentMethod;

    /**
     * @var string
     * @Assert\Choice(choices={Payer::STATUS_UNVERIFIED, Payer::STATUS_VERIFIED})
     */
    private $status;

    /**
     * @var FundingInstrument[]
     * @Assert\Valid()
     */
    private $fundingInstruments;

    /**
     * @var PayerInfo
     * @Assert\Valid()
     */
    private $payerInfo;

    /**
     * @return array
     */
    public function getData(): array
    {
        $data = [];

        if ($this->hasPaymentMethod()) {
            $data['payment_method'] = $this->getPaymentMethod();
        }

        if ($this->hasStatus()) {
            $data['status'] = $this->getStatus();
        }

        if ($this->hasFundingInstruments()) {
            foreach ($this->getFundingInstruments() as $fundingInstrument) {
                $data['funding_sources'][] = $fundingInstrument->getData();
            }
        }

        if ($this->hasPayerInfo()) {
            $data['payer_info'] = $this->getPayerInfo()->getData();
        }

        return $data;
    }

    /**
     * @return string|null
     */
    public function getPaymentMethod(): ?string
    {
        return $this->paymentMethod;
    }

    /**
     * @return bool
     */
    public function hasPaymentMethod(): bool
    {
        return !is_null($this->paymentMethod);
    }

    /**
     * @param string|null $paymentMethod
     *
     * @return Payer
     */
    public function setPaymentMethod(string $paymentMethod = null): Payer
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @return bool
     */
    public function hasStatus(): bool
    {
        return !is_null($this->status);
    }

    /**
     * @param string|null $status
     *
     * @return Payer
     */
    public function setStatus(string $status = null): Payer
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return FundingInstrument[]|null
     */
    public function getFundingInstruments(): ?array
    {
        return $this->fundingInstruments;
    }

    /**
     * @return bool
     */
    public function hasFundingInstruments(): bool
    {
        return !is_null($this->fundingInstruments);
    }

    /**
     * @param FundingInstrument[]|null $fundingInstruments
     *
     * @return Payer
     */
    public function setFundingInstruments(array $fundingInstruments = null): Payer
    {
        $this->fundingInstruments = $fundingInstruments;

        return $this;
    }

    /**
     * @return PayerInfo|null
     */
    public function getPayerInfo(): ?PayerInfo
    {
        return $this->payerInfo;
    }

    /**
     * @return bool
     */
    public function hasPayerInfo(): bool
    {
        return !is_null($this->payerInfo);
    }

    /**
     * @param PayerInfo|null $payerInfo
     *
     * @return Payer
     */
    public function setPayerInfo(PayerInfo $payerInfo = null): Payer
    {
        $this->payerInfo = $payerInfo;

        return $this;
    }
}
