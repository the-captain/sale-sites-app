<?php

namespace PaypalExpressBundle\Packet\Payment\Shared\Transaction\Payee;

use PaypalExpressBundle\Contract\PacketDataInterface;
use PaypalExpressBundle\Packet\Payment\Shared\Transaction\Payee\PayeeDisplayMetadata\DisplayPhone;
use Symfony\Component\Validator\Constraints as Assert;

class PayeeDisplayMetadata implements PacketDataInterface
{
    /**
     * @var string
     * @Assert\Email()
     */
    private $email;

    /**
     * @var DisplayPhone
     * @Assert\Valid()
     */
    private $displayPhone;

    /**
     * @var string
     * @Assert\Type(type="string")
     */
    private $brandName;

    /**
     * @return array
     */
    public function getData(): array
    {
        $data = [];

        if ($this->hasEmail()) {
            $data['email'] = $this->getEmail();
        }

        if ($this->hasDisplayPhone()) {
            $data['display_phone'] = $this->getDisplayPhone()->getData();
        }

        if ($this->hasBrandName()) {
            $data['brand_name'] = $this->getBrandName();
        }

        return $data;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return bool
     */
    public function hasEmail(): bool
    {
        return !is_null($this->email);
    }

    /**
     * @param string|null $email
     *
     * @return PayeeDisplayMetadata
     */
    public function setEmail(string $email = null): PayeeDisplayMetadata
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return DisplayPhone|null
     */
    public function getDisplayPhone(): ?DisplayPhone
    {
        return $this->displayPhone;
    }

    /**
     * @return bool
     */
    public function hasDisplayPhone(): bool
    {
        return !is_null($this->displayPhone);
    }

    /**
     * @param DisplayPhone|null $displayPhone
     *
     * @return PayeeDisplayMetadata
     */
    public function setDisplayPhone(DisplayPhone $displayPhone = null): PayeeDisplayMetadata
    {
        $this->displayPhone = $displayPhone;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBrandName(): ?string
    {
        return $this->brandName;
    }

    /**
     * @return bool
     */
    public function hasBrandName(): bool
    {
        return !is_null($this->brandName);
    }

    /**
     * @param string|null $brandName
     *
     * @return PayeeDisplayMetadata
     */
    public function setBrandName(string $brandName = null): PayeeDisplayMetadata
    {
        $this->brandName = $brandName;

        return $this;
    }
}
