<?php

namespace PaypalExpressBundle\Packet\Payment\Shared\Transaction\Payee\PayeeDisplayMetadata;

use PaypalExpressBundle\Contract\PacketDataInterface;
use Symfony\Component\Validator\Constraints as Assert;

class DisplayPhone implements PacketDataInterface
{
    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="2")
     */
    private $countryCode;

    /**
     * @var string
     * @Assert\Type(type="string")
     */
    private $number;

    /**
     * @return array
     */
    public function getData(): array
    {
        $data = [];

        if ($this->hasCountryCode()) {
            $data['country_code'] = $this->getCountryCode();
        }

        if ($this->hasNumber()) {
            $data['number'] = $this->getNumber();
        }

        return $data;
    }

    /**
     * @return string|null
     */
    public function getCountryCode(): ?string
    {
        return $this->countryCode;
    }

    /**
     * @return bool
     */
    public function hasCountryCode(): bool
    {
        return !is_null($this->countryCode);
    }

    /**
     * @param string|null $countryCode
     *
     * @return DisplayPhone
     */
    public function setCountryCode(string $countryCode = null): DisplayPhone
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNumber(): ?string
    {
        return $this->number;
    }

    /**
     * @return bool
     */
    public function hasNumber(): bool
    {
        return !is_null($this->number);
    }

    /**
     * @param string|null $number
     *
     * @return DisplayPhone
     */
    public function setNumber(string $number = null): DisplayPhone
    {
        $this->number = $number;

        return $this;
    }
}
