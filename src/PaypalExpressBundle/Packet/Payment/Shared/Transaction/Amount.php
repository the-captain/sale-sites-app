<?php

namespace PaypalExpressBundle\Packet\Payment\Shared\Transaction;

use PaypalExpressBundle\Contract\PacketDataInterface;
use PaypalExpressBundle\Packet\Payment\Shared\Transaction\Amount\Details;
use Symfony\Component\Validator\Constraints as Assert;

class Amount implements PacketDataInterface
{
    /**
     * @var string
     *
     * @Assert\Type(type="string")
     * @Assert\Length(max="10")
     * @Assert\Regex(pattern="/^[0-9]{0,10}\.?([0-9]{0,2})?$/ms")
     * @Assert\NotBlank()
     */
    protected $total;

    /**
     * @Assert\Choice(choices={"USD"})
     * @Assert\NotBlank()
     */
    protected $currency;

    /**
     * @var Details
     *
     * @Assert\Valid()
     */
    protected $details;

    /**
     * @return array
     */
    public function getData(): array
    {
        $data = [];

        if ($this->hasCurrency()) {
            $data['currency'] = $this->getCurrency();
        }

        if ($this->hasDetails()) {
            $data['details'] = $this->getDetails()->getData();
        }

        if ($this->hasTotal()) {
            $data['total'] = $this->getTotal();
        }

        return $data;
    }

    /**
     * @return mixed|null
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return bool
     */
    public function hasCurrency()
    {
        return !is_null($this->currency);
    }

    /**
     * @param mixed|null $currency
     *
     * @return Amount
     */
    public function setCurrency($currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return Details|null
     */
    public function getDetails(): ?Details
    {
        return $this->details;
    }

    /**
     * @return bool
     */
    public function hasDetails(): bool
    {
        return !is_null($this->details);
    }

    /**
     * @param Details|null $details
     *
     * @return Amount
     */
    public function setDetails(Details $details = null): Amount
    {
        $this->details = $details;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTotal(): ?string
    {
        return $this->total;
    }

    /**
     * @return bool
     */
    public function hasTotal(): bool
    {
        return !is_null($this->total);
    }

    /**
     * @param string|null $total
     *
     * @return Amount
     */
    public function setTotal(string $total = null): Amount
    {
        $this->total = $total;

        return $this;
    }

}
