<?php

namespace PaypalExpressBundle\Packet\Payment\Shared\Transaction\Amount;

use PaypalExpressBundle\Contract\PacketDataInterface;
use Symfony\Component\Validator\Constraints as Assert;

class Details implements PacketDataInterface
{
    /**
     * @var string
     *
     * @Assert\Type(type="string")
     * @Assert\Length(max="10")
     * @Assert\Regex(pattern="/^[0-9]{0,10}\.?([0-9]{0,2})?$/ms")
     * @Assert\NotBlank()
     */
    private $subtotal;

    /**
     * @var string
     *
     * @Assert\Type(type="string")
     * @Assert\Length(max="10")
     * @Assert\Regex(pattern="/^[0-9]{0,10}\.?([0-9]{0,2})?$/ms")
     */
    private $shipping;

    /**
     * @var string
     *
     * @Assert\Type(type="string")
     * @Assert\Length(max="10")
     * @Assert\Regex(pattern="/^[0-9]{0,10}\.?([0-9]{0,2})?$/ms")
     * @Assert\NotBlank()
     */
    private $tax;

    /**
     * @var string
     *
     * @Assert\Type(type="string")
     * @Assert\Length(max="10")
     * @Assert\Regex(pattern="/^[0-9]{0,10}\.?([0-9]{0,2})?$/ms")
     * @Assert\NotBlank()
     */
    private $handlingFee;

    /**
     * @var string
     *
     * @Assert\Type(type="string")
     * @Assert\Length(max="10")
     * @Assert\Regex(pattern="/^[0-9]{0,10}\.?([0-9]{0,2})?$/ms")
     * @Assert\NotBlank()
     */
    private $shipping_discount;

    /**
     * @var string
     *
     * @Assert\Type(type="string")
     * @Assert\Length(max="10")
     * @Assert\Regex(pattern="/^[0-9]{0,10}\.?([0-9]{0,2})?$/ms")
     * @Assert\NotBlank()
     */
    private $insurance;

    /**
     * @var string
     *
     * @Assert\Type(type="string")
     * @Assert\Length(max="10")
     * @Assert\Regex(pattern="/^[0-9]{0,10}\.?([0-9]{0,2})?$/ms")
     * @Assert\NotBlank()
     */
    private $gift_wrap;

    /**
     * @return array
     */
    public function getData(): array
    {
        $data = [];

        if ($this->hasSubtotal()) {
            $data['subtotal'] = $this->getSubtotal();
        }

        if ($this->hasShipping()) {
            $data['shipping'] = $this->getShipping();
        }

        if ($this->hasTax()) {
            $data['tax'] = $this->getTax();
        }

        if ($this->hasHandlingFee()) {
            $data['handling_fee'] = $this->getHandlingFee();
        }

        if ($this->hasShippingDiscount()) {
            $data['shipping_discount'] = $this->getShippingDiscount();
        }

        if ($this->hasInsurance()) {
            $data['insurance'] = $this->getInsurance();
        }

        if ($this->hasGiftWrap()) {
            $data['gift_wrap'] = $this->getGiftWrap();
        }

        return $data;
    }

    /**
     * @return string|null
     */
    public function getSubtotal(): ?string
    {
        return $this->subtotal;
    }

    /**
     * @return bool
     */
    public function hasSubtotal(): bool
    {
        return !is_null($this->subtotal);
    }

    /**
     * @param string|null $subtotal
     *
     * @return Details
     */
    public function setSubtotal(string $subtotal = null): Details
    {
        $this->subtotal = $subtotal;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getShipping(): ?string
    {
        return $this->shipping;
    }

    /**
     * @return bool
     */
    public function hasShipping(): bool
    {
        return !is_null($this->shipping);
    }

    /**
     * @param string|null $shipping
     *
     * @return Details
     */
    public function setShipping(string $shipping = null): Details
    {
        $this->shipping = $shipping;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTax(): ?string
    {
        return $this->tax;
    }

    /**
     * @return bool
     */
    public function hasTax(): bool
    {
        return !is_null($this->tax);
    }

    /**
     * @param string|null $tax
     *
     * @return Details
     */
    public function setTax(string $tax = null): Details
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getHandlingFee(): ?string
    {
        return $this->handlingFee;
    }

    /**
     * @return bool
     */
    public function hasHandlingFee(): bool
    {
        return !is_null($this->handlingFee);
    }

    /**
     * @param string|null $handlingFee
     *
     * @return Details
     */
    public function setHandlingFee(string $handlingFee = null): Details
    {
        $this->handlingFee = $handlingFee;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getShippingDiscount(): ?string
    {
        return $this->shipping_discount;
    }

    /**
     * @return bool
     */
    public function hasShippingDiscount(): bool
    {
        return !is_null($this->shipping_discount);
    }

    /**
     * @param string|null $shipping_discount
     *
     * @return Details
     */
    public function setShippingDiscount(string $shipping_discount = null): Details
    {
        $this->shipping_discount = $shipping_discount;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getInsurance(): ?string
    {
        return $this->insurance;
    }

    /**
     * @return bool
     */
    public function hasInsurance(): bool
    {
        return !is_null($this->insurance);
    }

    /**
     * @param string|null $insurance
     *
     * @return Details
     */
    public function setInsurance(string $insurance = null): Details
    {
        $this->insurance = $insurance;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getGiftWrap(): ?string
    {
        return $this->gift_wrap;
    }

    /**
     * @return bool
     */
    public function hasGiftWrap(): bool
    {
        return !is_null($this->gift_wrap);
    }

    /**
     * @param string|null $gift_wrap
     *
     * @return Details
     */
    public function setGiftWrap(string $gift_wrap = null): Details
    {
        $this->gift_wrap = $gift_wrap;

        return $this;
    }

}
