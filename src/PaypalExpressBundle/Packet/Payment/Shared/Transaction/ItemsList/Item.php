<?php

namespace PaypalExpressBundle\Packet\Payment\Shared\Transaction\ItemsList;

use PaypalExpressBundle\Contract\PacketDataInterface;
use Symfony\Component\Validator\Constraints as Assert;

class Item implements PacketDataInterface
{
    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="127")
     */
    private $sku;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="127")
     */
    private $name;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="127")
     */
    private $description;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="10")
     * @Assert\Regex(pattern="/^[0-9]{0,10}$/ms")
     * @Assert\NotBlank()
     */
    private $quantity;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="10")
     * @Assert\Regex(pattern="/^[0-9]{0,10}\.?([0-9]{0,2})?$/ms")
     * @Assert\NotBlank()
     */
    private $price;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(min="3", max="3")
     * @Assert\NotBlank()
     */
    private $currency;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="10")
     * @Assert\Regex(pattern="/^[0-9]{0,10}\.?([0-9]{0,2})?$/ms")
     */
    private $tax;

    /**
     * @var string
     * @Assert\Type(type="string")
     */
    private $url;

    /**
     * @return array
     */
    public function getData(): array
    {
        $data = [];

        if ($this->hasSku()) {
            $data['sku'] = $this->getSku();
        }

        if ($this->hasName()) {
            $data['name'] = $this->getName();
        }

        if ($this->hasDescription()) {
            $data['description'] = $this->getDescription();
        }

        if ($this->hasQuantity()) {
            $data['quantity'] = $this->getQuantity();
        }

        if ($this->hasPrice()) {
            $data['price'] = $this->getPrice();
        }

        if ($this->hasCurrency()) {
            $data['currency'] = $this->getCurrency();
        }

        if ($this->hasTax()) {
            $data['tax'] = $this->getTax();
        }

        if ($this->hasUrl()) {
            $data['url'] = $this->getUrl();
        }

        return $data;
    }

    /**
     * @return string|null
     */
    public function getSku(): ?string
    {
        return $this->sku;
    }

    /**
     * @return bool
     */
    public function hasSku(): bool
    {
        return !is_null($this->sku);
    }

    /**
     * @param string|null $sku
     *
     * @return Item
     */
    public function setSku(string $sku = null): Item
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function hasName(): bool
    {
        return !is_null($this->name);
    }

    /**
     * @param string|null $name
     *
     * @return Item
     */
    public function setName(string $name = null): Item
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return bool
     */
    public function hasDescription(): bool
    {
        return !is_null($this->description);
    }

    /**
     * @param string|null $description
     *
     * @return Item
     */
    public function setDescription(string $description = null): Item
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getQuantity(): ?string
    {
        return $this->quantity;
    }

    /**
     * @return bool
     */
    public function hasQuantity(): bool
    {
        return !is_null($this->quantity);
    }

    /**
     * @param string|null $quantity
     *
     * @return Item
     */
    public function setQuantity(string $quantity = null): Item
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPrice(): ?string
    {
        return $this->price;
    }

    /**
     * @return bool
     */
    public function hasPrice(): bool
    {
        return !is_null($this->price);
    }

    /**
     * @param string|null $price
     *
     * @return Item
     */
    public function setPrice(string $price = null): Item
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @return bool
     */
    public function hasCurrency(): bool
    {
        return !is_null($this->currency);
    }

    /**
     * @param string|null $currency
     *
     * @return Item
     */
    public function setCurrency(string $currency = null): Item
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTax(): ?string
    {
        return $this->tax;
    }

    /**
     * @return bool
     */
    public function hasTax(): bool
    {
        return !is_null($this->tax);
    }

    /**
     * @param string|null $tax
     *
     * @return Item
     */
    public function setTax(string $tax = null): Item
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @return bool
     */
    public function hasUrl(): bool
    {
        return !is_null($this->url);
    }

    /**
     * @param string|null $url
     *
     * @return Item
     */
    public function setUrl(string $url = null): Item
    {
        $this->url = $url;

        return $this;
    }
}
