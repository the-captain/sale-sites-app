<?php

namespace PaypalExpressBundle\Packet\Payment\Shared\Transaction;

use PaypalExpressBundle\Contract\PacketDataInterface;
use PaypalExpressBundle\Packet\Payment\Shared\Transaction\Payee\PayeeDisplayMetadata;
use Symfony\Component\Validator\Constraints as Assert;

class Payee implements PacketDataInterface
{
    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string
     * @Assert\Type(type="string")
     */
    private $merchantId;

    /**
     * @var PayeeDisplayMetadata
     * @Assert\Valid()
     */
    private $payeeDisplayMetadata;

    /**
     * @return array
     */
    public function getData(): array
    {
        $data = [];

        if ($this->hasEmail()) {
            $data['email'] = $this->getEmail();
        }

        if ($this->hasMerchantId()) {
            $data['merchant_id'] = $this->getMerchantId();
        }

        if ($this->hasPayeeDisplayMetadata()) {
            $data['payee_display_metadata'] = $this->getPayeeDisplayMetadata()->getData();
        }

        return $data;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return bool
     */
    public function hasEmail(): bool
    {
        return !is_null($this->email);
    }

    /**
     * @param string|null $email
     *
     * @return Payee
     */
    public function setEmail(string $email = null): Payee
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMerchantId(): ?string
    {
        return $this->merchantId;
    }

    /**
     * @return bool
     */
    public function hasMerchantId(): bool
    {
        return !is_null($this->merchantId);
    }

    /**
     * @param string|null $merchantId
     *
     * @return Payee
     */
    public function setMerchantId(string $merchantId = null): Payee
    {
        $this->merchantId = $merchantId;

        return $this;
    }

    /**
     * @return PayeeDisplayMetadata|null
     */
    public function getPayeeDisplayMetadata(): ?PayeeDisplayMetadata
    {
        return $this->payeeDisplayMetadata;
    }

    /**
     * @return bool
     */
    public function hasPayeeDisplayMetadata(): bool
    {
        return !is_null($this->payeeDisplayMetadata);
    }

    /**
     * @param PayeeDisplayMetadata|null $payeeDisplayMetadata
     *
     * @return Payee
     */
    public function setPayeeDisplayMetadata(PayeeDisplayMetadata $payeeDisplayMetadata = null): Payee
    {
        $this->payeeDisplayMetadata = $payeeDisplayMetadata;

        return $this;
    }
}
