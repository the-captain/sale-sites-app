<?php

namespace PaypalExpressBundle\Packet\Payment\Shared\Transaction;

use PaypalExpressBundle\Contract\PacketDataInterface;
use Symfony\Component\Validator\Constraints as Assert;

class PaymentOptions implements PacketDataInterface
{
    const UNRESTRICTED = 'UNRESTRICTED';

    const INSTANT_FUNDING_SOURCE = 'INSTANT_FUNDING_SOURCE';

    const IMMEDIATE_PAY = 'IMMEDIATE_PAY';

    /**
     * @var string
     * @Assert\Choice(
     *     choices={PaymentOptions::UNRESTRICTED, PaymentOptions::INSTANT_FUNDING_SOURCE, PaymentOptions::IMMEDIATE_PAY}
     * )
     */
    protected $allowedPaymentMethod = self::UNRESTRICTED;

    /**
     * @return array
     */
    public function getData(): array
    {
        $data = [];

        if ($this->hasAllowedPaymentMethod()) {
            $data['allowed_payment_method'] = $this->getAllowedPaymentMethod();
        }

        return $data;
    }

    /**
     * @return string|null
     */
    public function getAllowedPaymentMethod(): ?string
    {
        return $this->allowedPaymentMethod;
    }

    /**
     * @return bool
     */
    public function hasAllowedPaymentMethod(): bool
    {
        return !is_null($this->allowedPaymentMethod);
    }

    /**
     * @param string|null $allowedPaymentMethod
     *
     * @return PaymentOptions
     */
    public function setAllowedPaymentMethod(string $allowedPaymentMethod = null): PaymentOptions
    {
        $this->allowedPaymentMethod = $allowedPaymentMethod;

        return $this;
    }
}
