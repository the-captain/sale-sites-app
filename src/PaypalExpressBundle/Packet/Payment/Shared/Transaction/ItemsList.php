<?php

namespace PaypalExpressBundle\Packet\Payment\Shared\Transaction;

use PaypalExpressBundle\Contract\PacketDataInterface;
use PaypalExpressBundle\Packet\Payment\Shared\Address;
use PaypalExpressBundle\Packet\Payment\Shared\Transaction\ItemsList\Item;
use Symfony\Component\Validator\Constraints as Assert;

class ItemsList implements PacketDataInterface
{
    /**
     * @var Item[]
     * @Assert\Valid()
     */
    private $items;

    /**
     * @var Address
     * @Assert\Valid()
     */
    private $shippingAddress;

    /**
     * @return array
     */
    public function getData(): array
    {
        $data = [];

        if ($this->hasItems()) {
            foreach ($this->getItems() as $item) {
                $data['items'][] = $item->getData();
            }
        }

        if ($this->hasShippingAddress()) {
            $data['shipping_address'] = $this->getShippingAddress()->getData();
        }

        return $data;
    }

    /**
     * @return Item[]|null
     */
    public function getItems(): ?array
    {
        return $this->items;
    }

    /**
     * @return bool
     */
    public function hasItems(): bool
    {
        return !is_null($this->items);
    }

    /**
     * @param Item[]|null $items
     *
     * @return ItemsList
     */
    public function setItems(array $items = null): ItemsList
    {
        $this->items = $items;

        return $this;
    }

    /**
     * @return Address|null
     */
    public function getShippingAddress(): ?Address
    {
        return $this->shippingAddress;
    }

    /**
     * @return bool
     */
    public function hasShippingAddress(): bool
    {
        return !is_null($this->shippingAddress);
    }

    /**
     * @param Address|null $shippingAddress
     *
     * @return ItemsList
     */
    public function setShippingAddress(Address $shippingAddress = null): ItemsList
    {
        $this->shippingAddress = $shippingAddress;

        return $this;
    }
}
