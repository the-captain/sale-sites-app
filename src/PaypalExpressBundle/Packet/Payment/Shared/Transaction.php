<?php

namespace PaypalExpressBundle\Packet\Payment\Shared;

use PaypalExpressBundle\Contract\PacketDataInterface;
use PaypalExpressBundle\Packet\Payment\Shared\Transaction\Amount;
use PaypalExpressBundle\Packet\Payment\Shared\Transaction\ItemsList;
use PaypalExpressBundle\Packet\Payment\Shared\Transaction\Payee;
use PaypalExpressBundle\Packet\Payment\Shared\Transaction\PaymentOptions;
use Symfony\Component\Validator\Constraints as Assert;

class Transaction implements PacketDataInterface
{
    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="256")
     */
    private $referenceId;

    /**
     * @var Amount
     * @Assert\Valid()
     * @Assert\NotBlank()
     */
    private $amount;

    /**
     * @var Payee
     * @Assert\Valid()
     */
    private $payee;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="127")
     */
    private $description;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="255")
     */
    private $noteToPayee;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="127")
     */
    private $custom;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="127")
     */
    private $invoiceNumber;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="127")
     */
    private $purchaseOrder;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="22")
     */
    private $softDescriptor;

    /**
     * @var PaymentOptions
     * @Assert\Valid()
     */
    private $paymentOptions;

    /**
     * @var ItemsList
     * @Assert\Valid()
     */
    private $itemsList;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Url()
     * @Assert\Length(max="2048")
     */
    private $notifyUrl;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Url()
     * @Assert\Length(max="2048")
     */
    private $orderUrl;

    /**
     * @return array
     */
    public function getData(): array
    {
        $data = [];

        if ($this->hasReferenceId()) {
            $data['reference_id'] = $this->getReferenceId();
        }

        if ($this->hasAmount()) {
            $data['amount'] = $this->getAmount()->getData();
        }

        if ($this->hasPayee()) {
            $data['payee'] = $this->getPayee();
        }

        if ($this->hasDescription()) {
            $data['description'] = $this->getDescription();
        }

        if ($this->hasNoteToPayee()) {
            $data['note_to_payee'] = $this->getNoteToPayee();
        }

        if ($this->hasCustom()) {
            $data['custom'] = $this->hasCustom();
        }

        if ($this->hasInvoiceNumber()) {
            $data['invoice_number'] = $this->getInvoiceNumber();
        }

        if ($this->hasPurchaseOrder()) {
            $data['purchase_order'] = $this->getPurchaseOrder();
        }

        if ($this->hasSoftDescriptor()) {
            $data['soft_descriptor'] = $this->getSoftDescriptor();
        }

        if ($this->hasPaymentOptions()) {
            $data['payment_options'] = $this->getPaymentOptions()->getData();
        }

        if ($this->hasItemsList()) {
            $data['item_list'] = $this->getItemsList()->getData();
        }

        return $data;
    }

    /**
     * @return string|null
     */
    public function getReferenceId(): ?string
    {
        return $this->referenceId;
    }

    /**
     * @return bool
     */
    public function hasReferenceId(): bool
    {
        return !is_null($this->referenceId);
    }

    /**
     * @param string|null $referenceId
     *
     * @return Transaction
     */
    public function setReferenceId(string $referenceId = null): Transaction
    {
        $this->referenceId = $referenceId;

        return $this;
    }

    /**
     * @return Amount|null
     */
    public function getAmount(): ?Amount
    {
        return $this->amount;
    }

    /**
     * @return bool
     */
    public function hasAmount(): bool
    {
        return !is_null($this->amount);
    }

    /**
     * @param Amount|null $amount
     *
     * @return Transaction
     */
    public function setAmount(Amount $amount = null): Transaction
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return Payee|null
     */
    public function getPayee(): ?Payee
    {
        return $this->payee;
    }

    /**
     * @return bool
     */
    public function hasPayee(): bool
    {
        return !is_null($this->payee);
    }

    /**
     * @param Payee|null $payee
     *
     * @return Transaction
     */
    public function setPayee(Payee $payee = null): Transaction
    {
        $this->payee = $payee;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return bool
     */
    public function hasDescription(): bool
    {
        return !is_null($this->description);
    }

    /**
     * @param string|null $description
     *
     * @return Transaction
     */
    public function setDescription(string $description = null): Transaction
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNoteToPayee(): ?string
    {
        return $this->noteToPayee;
    }

    /**
     * @return bool
     */
    public function hasNoteToPayee(): bool
    {
        return !is_null($this->noteToPayee);
    }

    /**
     * @param string|null $noteToPayee
     *
     * @return Transaction
     */
    public function setNoteToPayee(string $noteToPayee = null): Transaction
    {
        $this->noteToPayee = $noteToPayee;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getInvoiceNumber(): ?string
    {
        return $this->invoiceNumber;
    }

    /**
     * @return bool
     */
    public function hasInvoiceNumber(): bool
    {
        return !is_null($this->invoiceNumber);
    }

    /**
     * @param string|null $invoiceNumber
     *
     * @return Transaction
     */
    public function setInvoiceNumber(string $invoiceNumber = null): Transaction
    {
        $this->invoiceNumber = $invoiceNumber;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPurchaseOrder(): ?string
    {
        return $this->purchaseOrder;
    }

    /**
     * @return bool
     */
    public function hasPurchaseOrder(): bool
    {
        return !is_null($this->purchaseOrder);
    }

    /**
     * @param string|null $purchaseOrder
     *
     * @return Transaction
     */
    public function setPurchaseOrder(string $purchaseOrder = null): Transaction
    {
        $this->purchaseOrder = $purchaseOrder;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSoftDescriptor(): ?string
    {
        return $this->softDescriptor;
    }

    /**
     * @return bool
     */
    public function hasSoftDescriptor(): bool
    {
        return !is_null($this->softDescriptor);
    }

    /**
     * @param string|null $softDescriptor
     *
     * @return Transaction
     */
    public function setSoftDescriptor(string $softDescriptor = null): Transaction
    {
        $this->softDescriptor = $softDescriptor;

        return $this;
    }

    /**
     * @return PaymentOptions|null
     */
    public function getPaymentOptions(): ?PaymentOptions
    {
        return $this->paymentOptions;
    }

    /**
     * @return bool
     */
    public function hasPaymentOptions(): bool
    {
        return !is_null($this->paymentOptions);
    }

    /**
     * @param PaymentOptions|null $paymentOptions
     *
     * @return Transaction
     */
    public function setPaymentOptions(PaymentOptions $paymentOptions = null): Transaction
    {
        $this->paymentOptions = $paymentOptions;

        return $this;
    }

    /**
     * @return ItemsList|null
     */
    public function getItemsList(): ?ItemsList
    {
        return $this->itemsList;
    }

    /**
     * @return bool
     */
    public function hasItemsList(): bool
    {
        return !is_null($this->itemsList);
    }

    /**
     * @param ItemsList|null $itemsList
     *
     * @return Transaction
     */
    public function setItemsList(ItemsList $itemsList = null): Transaction
    {
        $this->itemsList = $itemsList;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustom(): ?string
    {
        return $this->custom;
    }

    /**
     * @return bool
     */
    public function hasCustom(): bool
    {
        return !is_null($this->custom);
    }

    /**
     * @param string|null $custom
     *
     * @return Transaction
     */
    public function setCustom(string $custom = null): Transaction
    {
        $this->custom = $custom;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNotifyUrl(): ?string
    {
        return $this->notifyUrl;
    }

    /**
     * @return bool
     */
    public function hasNotifyUrl(): bool
    {
        return !is_null($this->notifyUrl);
    }

    /**
     * @param string|null $notifyUrl
     *
     * @return Transaction
     */
    public function setNotifyUrl(string $notifyUrl = null): Transaction
    {
        $this->notifyUrl = $notifyUrl;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getOrderUrl(): ?string
    {
        return $this->orderUrl;
    }

    /**
     * @return bool
     */
    public function hasOrderUrl(): bool
    {
        return !is_null($this->orderUrl);
    }

    /**
     * @param string|null $orderUrl
     *
     * @return Transaction
     */
    public function setOrderUrl(string $orderUrl = null): Transaction
    {
        $this->orderUrl = $orderUrl;

        return $this;
    }
}
