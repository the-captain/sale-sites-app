<?php

namespace PaypalExpressBundle\Packet\Payment\Shared\Payer\FundingInstrument;

use PaypalExpressBundle\Contract\PacketDataInterface;
use PaypalExpressBundle\Packet\Payment\Shared\Address;
use PaypalExpressBundle\Packet\Payment\Shared\Payer\FundingInstrument\CreditCard\LinkDescription;
use Symfony\Component\Validator\Constraints as Assert;

class CreditCard implements PacketDataInterface
{
    const TYPE_VISA = 'visa';

    const TYPE_MASTERCARD = 'mastercard';

    const TYPE_DISCOVER = 'discover';

    const TYPE_AMEX = 'amex';

    /**
     * @var string
     * @Assert\CardScheme()
     * @Assert\NotBlank()
     */
    private $number;

    /**
     * @var string
     * @Assert\Choice(choices={
     *      CreditCard::TYPE_VISA,
     *      CreditCard::TYPE_MASTERCARD,
     *      CreditCard::TYPE_DISCOVER,
     *      CreditCard::TYPE_AMEX
     *     })
     * @Assert\NotBlank()
     */
    private $type;

    /**
     * @var int
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1", max="12")
     * @Assert\NotBlank()
     */
    private $expireMonth;

    /**
     * @var int
     * @Assert\Type(type="integer")
     * @Assert\Range(min="0001", max="9999")
     * @Assert\NotBlank()
     */
    private $expireYear;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(min="3", max="4")
     * @Assert\Regex(pattern="^[0-9]{3,4}$")
     */
    private $cvv2;

    /**
     * @var string
     * @Assert\Type(type="string")
     */
    private $firstName;

    /**
     * @var string
     * @Assert\Type(type="string")
     */
    private $lastName;

    /**
     * @var Address
     * @Assert\Valid()
     */
    private $billingAddress;

    /**
     * @var LinkDescription[]
     * @Assert\Valid()
     */
    private $links;

    /**
     * @return array
     */
    public function getData(): array
    {
        $data = [];

        if ($this->hasNumber()) {
            $data['number'] = $this->getNumber();
        }

        if ($this->hasType()) {
            $data['type'] = $this->getType();
        }

        if ($this->hasExpireMonth()) {
            $data['expire_month'] = $this->getExpireMonth();
        }

        if ($this->hasExpireYear()) {
            $data['expire_year'] = $this->getExpireYear();
        }

        if ($this->hasCvv2()) {
            $data['cvv2'] = $this->getCvv2();
        }

        if ($this->hasFirstName()) {
            $data['first_name'] = $this->getFirstName();
        }

        if ($this->hasLastName()) {
            $data['last_name'] = $this->getLastName();
        }

        if ($this->hasBillingAddress()) {
            $data['billing_address'] = $this->getBillingAddress()->getData();
        }

        if ($this->hasLinks()) {
            foreach ($this->getLinks() as $linkDescription) {
                $data['links'][] = $linkDescription->getData();
            }
        }

        return $data;
    }

    /**
     * @return string|null
     */
    public function getNumber(): ?string
    {
        return $this->number;
    }

    /**
     * @return bool
     */
    public function hasNumber(): bool
    {
        return !is_null($this->number);
    }

    /**
     * @param string|null $number
     *
     * @return CreditCard
     */
    public function setNumber(string $number = null): CreditCard
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @return bool
     */
    public function hasType(): bool
    {
        return !is_null($this->type);
    }

    /**
     * @param string|null $type
     *
     * @return CreditCard
     */
    public function setType(string $type = null): CreditCard
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getExpireMonth(): ?int
    {
        return $this->expireMonth;
    }

    /**
     * @return bool
     */
    public function hasExpireMonth(): bool
    {
        return !is_null($this->expireMonth);
    }

    /**
     * @param int|null $expireMonth
     *
     * @return CreditCard
     */
    public function setExpireMonth(int $expireMonth = null): CreditCard
    {
        $this->expireMonth = $expireMonth;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getExpireYear(): ?int
    {
        return $this->expireYear;
    }

    /**
     * @return bool
     */
    public function hasExpireYear(): bool
    {
        return !is_null($this->expireYear);
    }

    /**
     * @param int|null $expireYear
     *
     * @return CreditCard
     */
    public function setExpireYear(int $expireYear = null): CreditCard
    {
        $this->expireYear = $expireYear;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCvv2(): ?string
    {
        return $this->cvv2;
    }

    /**
     * @return bool
     */
    public function hasCvv2(): bool
    {
        return !is_null($this->cvv2);
    }

    /**
     * @param string|null $cvv2
     *
     * @return CreditCard
     */
    public function setCvv2(string $cvv2 = null): CreditCard
    {
        $this->cvv2 = $cvv2;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @return bool
     */
    public function hasFirstName(): bool
    {
        return !is_null($this->firstName);
    }

    /**
     * @param string|null $firstName
     *
     * @return CreditCard
     */
    public function setFirstName(string $firstName = null): CreditCard
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @return bool
     */
    public function hasLastName(): bool
    {
        return !is_null($this->lastName);
    }

    /**
     * @param string|null $lastName
     *
     * @return CreditCard
     */
    public function setLastName(string $lastName = null): CreditCard
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return Address|null
     */
    public function getBillingAddress(): ?Address
    {
        return $this->billingAddress;
    }

    /**
     * @return bool
     */
    public function hasBillingAddress(): bool
    {
        return !is_null($this->billingAddress);
    }

    /**
     * @param Address|null $billingAddress
     *
     * @return CreditCard
     */
    public function setBillingAddress(Address $billingAddress = null): CreditCard
    {
        $this->billingAddress = $billingAddress;

        return $this;
    }

    /**
     * @return LinkDescription[]|null
     */
    public function getLinks(): ?array
    {
        return $this->links;
    }

    /**
     * @return bool
     */
    public function hasLinks(): bool
    {
        return !is_null($this->links);
    }

    /**
     * @param LinkDescription[]|null $links
     *
     * @return CreditCard
     */
    public function setLinks(array $links = null): CreditCard
    {
        $this->links = $links;

        return $this;
    }
}
