<?php

namespace PaypalExpressBundle\Packet\Payment\Shared\Payer\FundingInstrument\CreditCard;

use PaypalExpressBundle\Contract\PacketDataInterface;
use Symfony\Component\Validator\Constraints as Assert;

class LinkDescription implements PacketDataInterface
{
    const METHOD_GET = 'GET';

    const METHOD_POST = 'POST';

    const METHOD_PUT = 'PUT';

    const METHOD_DELETE = 'DELETE';

    const METHOD_HEAD = 'HEAD';

    const METHOD_CONNECT = 'CONNECT';

    const METHOD_OPTIONS = 'OPTIONS';

    const METHOD_PATCH = 'PATCH';

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\NotBlank()
     */
    private $href;

    /**
     * @var string
     * @Assert\Type(type="string")
     */
    private $rel;

    /**
     * @var string
     * @Assert\Choice(
     *     choices={
     *     LinkDescription::METHOD_GET,
     *     LinkDescription::METHOD_POST,
     *     LinkDescription::METHOD_PUT,
     *     LinkDescription::METHOD_DELETE,
     *     LinkDescription::METHOD_HEAD,
     *     LinkDescription::METHOD_CONNECT,
     *     LinkDescription::METHOD_OPTIONS,
     *     LinkDescription::METHOD_PATCH
     *      }
     * )
     * @Assert\NotBlank()
     */
    private $method;

    /**
     * @return array
     */
    public function getData(): array
    {
        $data = [];

        if ($this->hasHref()) {
            $data['href'] = $this->getHref();
        }

        if ($this->hasRel()) {
            $data['rel'] = $this->getRel();
        }

        if ($this->hasMethod()) {
            $data['method'] = $this->getMethod();
        }

        return $data;
    }

    /**
     * @return string|null
     */
    public function getHref(): ?string
    {
        return $this->href;
    }

    /**
     * @return bool
     */
    public function hasHref(): bool
    {
        return !is_null($this->href);
    }

    /**
     * @param string|null $href
     *
     * @return LinkDescription
     */
    public function setHref(string $href = null): LinkDescription
    {
        $this->href = $href;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRel(): ?string
    {
        return $this->rel;
    }

    /**
     * @return bool
     */
    public function hasRel(): bool
    {
        return !is_null($this->rel);
    }

    /**
     * @param string|null $rel
     *
     * @return LinkDescription
     */
    public function setRel(string $rel = null): LinkDescription
    {
        $this->rel = $rel;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMethod(): ?string
    {
        return $this->method;
    }

    /**
     * @return bool
     */
    public function hasMethod(): bool
    {
        return !is_null($this->method);
    }

    /**
     * @param string|null $method
     *
     * @return LinkDescription
     */
    public function setMethod(string $method = null): LinkDescription
    {
        $this->method = $method;

        return $this;
    }
}
