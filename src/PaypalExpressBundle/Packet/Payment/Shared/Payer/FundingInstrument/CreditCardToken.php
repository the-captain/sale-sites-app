<?php

namespace PaypalExpressBundle\Packet\Payment\Shared\Payer\FundingInstrument;

use PaypalExpressBundle\Contract\PacketDataInterface;
use Symfony\Component\Validator\Constraints as Assert;

class CreditCardToken implements PacketDataInterface
{
    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\NotBlank()
     */
    private $creditCardId;

    /**
     * @var string
     * @Assert\Type(type="string")
     */
    private $payerId;

    /**
     * @var string
     * @Assert\Choice(
     *     choices={
     *     CreditCard::TYPE_AMEX,
     *     CreditCard::TYPE_DISCOVER,
     *     CreditCard::TYPE_MASTERCARD,
     *     CreditCard::TYPE_VISA
     *     }
     * )
     */
    private $type;

    /**
     * @var int
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1", max="12")
     * @Assert\NotBlank()
     */
    private $expireMonth;

    /**
     * @var int
     * @Assert\Type(type="integer")
     * @Assert\Range(min="0001", max="9999")
     * @Assert\NotBlank()
     */
    private $expireYear;

    /**
     * @return array
     */
    public function getData(): array
    {
        $data = [];

        if ($this->hasCreditCardId()) {
            $data['credit_card_id'] = $this->getCreditCardId();
        }

        if ($this->hasPayerId()) {
            $data['payer_id'] = $this->getPayerId();
        }

        if ($this->hasType()) {
            $data['type'] = $this->getType();
        }

        if ($this->hasExpireMonth()) {
            $data['expire_month'] = $this->getExpireMonth();
        }

        if ($this->hasExpireYear()) {
            $data['expire_year'] = $this->getExpireYear();
        }

        return $data;
    }

    /**
     * @return string|null
     */
    public function getCreditCardId(): ?string
    {
        return $this->creditCardId;
    }

    /**
     * @return bool
     */
    public function hasCreditCardId(): bool
    {
        return !is_null($this->creditCardId);
    }

    /**
     * @param string|null $creditCardId
     *
     * @return CreditCardToken
     */
    public function setCreditCardId(string $creditCardId = null): CreditCardToken
    {
        $this->creditCardId = $creditCardId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPayerId(): ?string
    {
        return $this->payerId;
    }

    /**
     * @return bool
     */
    public function hasPayerId(): bool
    {
        return !is_null($this->payerId);
    }

    /**
     * @param string|null $payerId
     *
     * @return CreditCardToken
     */
    public function setPayerId(string $payerId = null): CreditCardToken
    {
        $this->payerId = $payerId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @return bool
     */
    public function hasType(): bool
    {
        return !is_null($this->type);
    }

    /**
     * @param string|null $type
     *
     * @return CreditCardToken
     */
    public function setType(string $type = null): CreditCardToken
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getExpireMonth(): ?int
    {
        return $this->expireMonth;
    }

    /**
     * @return bool
     */
    public function hasExpireMonth(): bool
    {
        return !is_null($this->expireMonth);
    }

    /**
     * @param int|null $expireMonth
     *
     * @return CreditCardToken
     */
    public function setExpireMonth(int $expireMonth = null): CreditCardToken
    {
        $this->expireMonth = $expireMonth;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getExpireYear(): ?int
    {
        return $this->expireYear;
    }

    /**
     * @return bool
     */
    public function hasExpireYear(): bool
    {
        return !is_null($this->expireYear);
    }

    /**
     * @param int|null $expireYear
     *
     * @return CreditCardToken
     */
    public function setExpireYear(int $expireYear = null): CreditCardToken
    {
        $this->expireYear = $expireYear;

        return $this;
    }
}
