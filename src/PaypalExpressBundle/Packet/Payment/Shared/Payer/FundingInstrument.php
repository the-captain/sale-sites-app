<?php

namespace PaypalExpressBundle\Packet\Payment\Shared\Payer;

use PaypalExpressBundle\Contract\PacketDataInterface;
use PaypalExpressBundle\Packet\Payment\Shared\Payer\FundingInstrument\CreditCard;
use PaypalExpressBundle\Packet\Payment\Shared\Payer\FundingInstrument\CreditCardToken;
use Symfony\Component\Validator\Constraints as Assert;

class FundingInstrument implements PacketDataInterface
{
    /**
     * @var CreditCard
     * @Assert\Valid()
     */
    private $creditCard;

    /**
     * @var CreditCardToken
     * @Assert\Valid()
     */
    private $creditCardToken;

    /**
     * @return array
     */
    public function getData(): array
    {
        $data = [];

        if ($this->hasCreditCard()) {
            $data['credit_card'] = $this->getCreditCard()->getData();
        }

        if ($this->hasCreditCardToken()) {
            $data['credit_card_token'] = $this->getCreditCardToken()->getData();
        }

        return $data;
    }

    /**
     * @return CreditCard|null
     */
    public function getCreditCard(): ?CreditCard
    {
        return $this->creditCard;
    }

    /**
     * @return bool
     */
    public function hasCreditCard(): bool
    {
        return !is_null($this->creditCard);
    }

    /**
     * @param CreditCard|null $creditCard
     *
     * @return FundingInstrument
     */
    public function setCreditCard(CreditCard $creditCard = null): FundingInstrument
    {
        $this->creditCard = $creditCard;

        return $this;
    }

    /**
     * @return CreditCardToken|null
     */
    public function getCreditCardToken(): ?CreditCardToken
    {
        return $this->creditCardToken;
    }

    /**
     * @return bool
     */
    public function hasCreditCardToken(): bool
    {
        return !is_null($this->creditCardToken);
    }

    /**
     * @param CreditCardToken|null $creditCardToken
     *
     * @return FundingInstrument
     */
    public function setCreditCardToken(CreditCardToken $creditCardToken = null): FundingInstrument
    {
        $this->creditCardToken = $creditCardToken;

        return $this;
    }
}
