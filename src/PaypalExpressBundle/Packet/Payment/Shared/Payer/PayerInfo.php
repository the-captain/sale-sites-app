<?php

namespace PaypalExpressBundle\Packet\Payment\Shared\Payer;

use PaypalExpressBundle\Contract\PacketDataInterface;
use PaypalExpressBundle\Packet\Payment\Shared\Address;
use Symfony\Component\Validator\Constraints as Assert;

class PayerInfo implements PacketDataInterface
{
    const TAX_ID_TYPE_BR_CPF = 'BR_CPF';

    const TAX_ID_TYPE_BR_CNPJ = 'BR_CNPJ';

    /**
     * @var string
     * @Assert\Length(max="127")
     */
    private $email;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Regex(pattern="/[0-9]{4}-[0-9]{2}-[0-9]{2}/ms")
     */
    private $birtDate;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="14")
     */
    private $taxId;

    /**
     * @var string
     * @Assert\Choice(choices={
     *     PayerInfo::TAX_ID_TYPE_BR_CNPJ,
     *     PayerInfo::TAX_ID_TYPE_BR_CPF
     *     }
     * )
     */
    private $taxIdType;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="2", min="2")
     */
    private $countryCode;

    /**
     * @var Address
     * @Assert\Valid()
     */
    private $billingAddress;

    /**
     * @return array
     */
    public function getData(): array
    {
        $data = [];

        return $data;
    }
}
