<?php

namespace PaypalExpressBundle\Packet\Payment\Shared;

use PaypalExpressBundle\Contract\PacketDataInterface;
use Symfony\Component\Validator\Constraints as Assert;

class Address implements PacketDataInterface
{
    const TYPE_HOME_OR_WORK = 'HOME_OR_WORK';

    const TYPE_GIFT = 'GIFT';

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="100")
     * @Assert\NotBlank()
     */
    private $line1;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="100")
     */
    private $line2;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="64")
     */
    private $city;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="2", min="2")
     * @Assert\Regex(pattern="/^([A-Z]{2}|C2)$/ms")
     * @Assert\NotBlank()
     */
    private $countryCode;

    /**
     * @var string
     * @Assert\Type(type="string")
     */
    private $postalCode;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="40")
     */
    private $state;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="50")
     */
    private $phone;

    /**
     * @var string
     * @Assert\Choice(choices={Address::TYPE_GIFT, Address::TYPE_HOME_OR_WORK})
     */
    private $type;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="127")
     */
    private $recipientName;

    /**
     * @return array
     */
    public function getData(): array
    {
        $data = [];

        if ($this->hasLine1()) {
            $data['line1'] = $this->getLine1();
        }

        if ($this->hasLine2()) {
            $data['line2'] = $this->getLine2();
        }

        if ($this->hasCity()) {
            $data['city'] = $this->getCity();
        }

        if ($this->hasCountryCode()) {
            $data['country_code'] = $this->getCountryCode();
        }

        if ($this->hasPostalCode()) {
            $data['postal_code'] = $this->getPostalCode();
        }

        if ($this->hasState()) {
            $data['state'] = $this->getState();
        }

        if ($this->hasPhone()) {
            $data['phone'] = $this->getPhone();
        }

        if ($this->hasType()) {
            $data['type'] = $this->getType();
        }

        if ($this->hasRecipientName()) {
            $data['recipient_name'] = $this->getRecipientName();
        }

        return $data;
    }

    /**
     * @return string|null
     */
    public function getLine1(): ?string
    {
        return $this->line1;
    }

    /**
     * @return bool
     */
    public function hasLine1(): bool
    {
        return !is_null($this->line1);
    }

    /**
     * @param string|null $line1
     *
     * @return Address
     */
    public function setLine1(string $line1 = null): Address
    {
        $this->line1 = $line1;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @return bool
     */
    public function hasCity(): bool
    {
        return !is_null($this->city);
    }

    /**
     * @param string|null $city
     *
     * @return Address
     */
    public function setCity(string $city = null): Address
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountryCode(): ?string
    {
        return $this->countryCode;
    }

    /**
     * @return bool
     */
    public function hasCountryCode(): bool
    {
        return !is_null($this->countryCode);
    }

    /**
     * @param string|null $countryCode
     *
     * @return Address
     */
    public function setCountryCode(string $countryCode = null): Address
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    /**
     * @return bool
     */
    public function hasPostalCode(): bool
    {
        return !is_null($this->postalCode);
    }

    /**
     * @param string|null $postalCode
     *
     * @return Address
     */
    public function setPostalCode(string $postalCode = null): Address
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getState(): ?string
    {
        return $this->state;
    }

    /**
     * @return bool
     */
    public function hasState(): bool
    {
        return !is_null($this->state);
    }

    /**
     * @param string|null $state
     *
     * @return Address
     */
    public function setState(string $state = null): Address
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @return bool
     */
    public function hasPhone(): bool
    {
        return !is_null($this->phone);
    }

    /**
     * @param string|null $phone
     *
     * @return Address
     */
    public function setPhone(string $phone = null): Address
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @return bool
     */
    public function hasType(): bool
    {
        return !is_null($this->type);
    }

    /**
     * @param string|null $type
     *
     * @return Address
     */
    public function setType(string $type = null): Address
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRecipientName(): ?string
    {
        return $this->recipientName;
    }

    /**
     * @return bool
     */
    public function hasRecipientName(): bool
    {
        return !is_null($this->recipientName);
    }

    /**
     * @param string|null $recipientName
     *
     * @return Address
     */
    public function setRecipientName(string $recipientName = null): Address
    {
        $this->recipientName = $recipientName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLine2(): ?string
    {
        return $this->line2;
    }

    /**
     * @return bool
     */
    public function hasLine2(): bool
    {
        return !is_null($this->line2);
    }

    /**
     * @param string|null $line2
     *
     * @return Address
     */
    public function setLine2(string $line2 = null): Address
    {
        $this->line2 = $line2;

        return $this;
    }
}
