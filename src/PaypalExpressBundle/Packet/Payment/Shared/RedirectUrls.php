<?php

namespace PaypalExpressBundle\Packet\Payment\Shared;

use PaypalExpressBundle\Contract\PacketDataInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class RedirectUrls.
 */
class RedirectUrls implements PacketDataInterface
{
    /**
     *
     * @Assert\Url()
     *
     * @var string
     */
    private $returnUrl;

    /**
     * @Assert\Url()
     *
     * @var string
     */
    private $cancelUrl;

    /**
     * @return array
     */
    public function getData(): array
    {
        $data = [];

        if ($this->hasCancelUrl()) {
            $data['cancel_url'] = $this->getCancelUrl();
        }

        if ($this->hasReturnUrl()) {
            $data['return_url'] = $this->getReturnUrl();
        }

        return $data;
    }

    /**
     * @return string|null
     */
    public function getCancelUrl(): ?string
    {
        return $this->cancelUrl;
    }

    /**
     * @return bool
     */
    public function hasCancelUrl(): bool
    {
        return !is_null($this->cancelUrl);
    }

    /**
     * @param string|null $cancelUrl
     *
     * @return RedirectUrls
     */
    public function setCancelUrl(string $cancelUrl = null): RedirectUrls
    {
        $this->cancelUrl = $cancelUrl;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getReturnUrl(): ?string
    {
        return $this->returnUrl;
    }

    /**
     * @return bool
     */
    public function hasReturnUrl(): bool
    {
        return !is_null($this->returnUrl);
    }

    /**
     * @param string|null $returnUrl
     *
     * @return RedirectUrls
     */
    public function setReturnUrl(string $returnUrl = null): RedirectUrls
    {
        $this->returnUrl = $returnUrl;

        return $this;
    }
}
