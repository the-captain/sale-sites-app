<?php

namespace PaypalExpressBundle\Packet\Payment;

use Doctrine\Common\Annotations\Annotation\Required;
use PaypalExpressBundle\Packet\AbstractAccessTokenPacket;
use PaypalExpressBundle\Packet\Payment\Shared\Payer;
use PaypalExpressBundle\Packet\Payment\Shared\RedirectUrls;
use PaypalExpressBundle\Packet\Payment\Shared\Transaction;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Create.
 */
class Create extends AbstractAccessTokenPacket
{
    const INTENT_SALE = 'sale';

    const INTENT_AUTHORIZE = 'authorize';

    const INTENT_ORDER = 'order';

    /**
     * Set to one of the following:
     * sale. Makes an immediate Express Checkout payment.
     * authorize. Places the funds on hold for a later payment.
     * order. Indicates that the buyer has consented to a future purchase.
     * Funds are authorized and captured at a later time without placing the funds on hold.
     *
     * @Assert\Choice(choices={Create::INTENT_AUTHORIZE, Create::INTENT_ORDER, Create::INTENT_SALE})
     * @Assert\NotBlank()
     *
     * @var string
     */
    private $intent;

    /**
     * @var Payer
     * @Assert\Valid()
     * @Assert\NotBlank()
     */
    private $payer;

    /**
     * Optionally, specify the PayPal-generated ID for a web experience profile.
     *
     * @Assert\Type(type="string")
     *
     * @var string
     */
    private $experienceProfileId;

    /**
     * @Assert\Valid()
     * @Assert\NotBlank()
     *
     * @var RedirectUrls
     */
    private $redirectUrls;

    /**
     * @Assert\NotBlank()
     * @Assert\Valid()
     *
     * @var Transaction[]
     */
    private $transactions;

    /**
     * @var string
     * @Assert\Type(type="string")
     * @Assert\Length(max="165")
     */
    private $notToPayer;

    /**
     * {@inheritdoc}
     */
    public function getHttpHeaders(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getData(): array
    {
        $data = [];

        if ($this->hasIntent()) {
            $data['intent'] = $this->getIntent();
        }

        if ($this->hasPayer()) {
            $data['payer'] = $this->getPayer()->getData();
        }

        if ($this->hasExperienceProfileId()) {
            $data['expirience_profile_id'] = $this->getExperienceProfileId();
        }

        if ($this->hasTransactions()) {
            foreach ($this->getTransactions() as $transaction) {
                $data['transactions'][] = $transaction->getData();
            }
        }

        if ($this->hasNotToPayer()) {
            $data['note_to_payer'] = $this->getNotToPayer();
        }

        if ($this->hasRedirectUrls()) {
            $data['redirect_urls'] = $this->getRedirectUrls()->getData();
        }

        return $data;
    }

    /**
     * @return string|null
     */
    public function getIntent(): ?string
    {
        return $this->intent;
    }

    /**
     * @return bool
     */
    public function hasIntent(): bool
    {
        return !is_null($this->intent);
    }

    /**
     * @param string|null $intent
     *
     * @return Create
     */
    public function setIntent(string $intent = null): Create
    {
        $this->intent = $intent;

        return $this;
    }

    /**
     * @return Payer|null
     */
    public function getPayer(): ?Payer
    {
        return $this->payer;
    }

    /**
     * @return bool
     */
    public function hasPayer(): bool
    {
        return !is_null($this->payer);
    }

    /**
     * @param Payer|null $payer
     *
     * @return Create
     */
    public function setPayer(Payer $payer = null): Create
    {
        $this->payer = $payer;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getExperienceProfileId(): ?string
    {
        return $this->experienceProfileId;
    }

    /**
     * @return bool
     */
    public function hasExperienceProfileId(): bool
    {
        return !is_null($this->experienceProfileId);
    }

    /**
     * @param string|null $experienceProfileId
     *
     * @return Create
     */
    public function setExperienceProfileId(string $experienceProfileId = null): Create
    {
        $this->experienceProfileId = $experienceProfileId;

        return $this;
    }

    /**
     * @return Transaction[]|null
     */
    public function getTransactions(): ?array
    {
        return $this->transactions;
    }

    /**
     * @return bool
     */
    public function hasTransactions(): bool
    {
        return !is_null($this->transactions);
    }

    /**
     * @param Transaction[]|null $transactions
     *
     * @return Create
     */
    public function setTransactions(array $transactions = null): Create
    {
        $this->transactions = $transactions;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNotToPayer(): ?string
    {
        return $this->notToPayer;
    }

    /**
     * @return bool
     */
    public function hasNotToPayer(): bool
    {
        return !is_null($this->notToPayer);
    }

    /**
     * @param string|null $notToPayer
     *
     * @return Create
     */
    public function setNotToPayer(string $notToPayer = null): Create
    {
        $this->notToPayer = $notToPayer;

        return $this;
    }

    /**
     * @return RedirectUrls|null
     */
    public function getRedirectUrls(): ?RedirectUrls
    {
        return $this->redirectUrls;
    }

    /**
     * @return bool
     */
    public function hasRedirectUrls(): bool
    {
        return !is_null($this->redirectUrls);
    }

    /**
     * @param RedirectUrls|null $redirectUrls
     *
     * @return Create
     */
    public function setRedirectUrls(RedirectUrls $redirectUrls = null): Create
    {
        $this->redirectUrls = $redirectUrls;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getHttpUri(): string
    {
        return 'payments/payment';
    }

    /**
     * {@inheritdoc}
     */
    public function getHttpMethod(): string
    {
        return self::METHOD_POST;
    }
}
