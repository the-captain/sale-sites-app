<?php

namespace PaypalExpressBundle\Packet\Payment;

use PaypalExpressBundle\Packet\AbstractAccessTokenPacket;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Execute.
 */
class Execute extends AbstractAccessTokenPacket
{
    /**
     * @var string
     *
     * @Assert\Type(type="string")
     */
    private $payerId;

    /**
     * @var string
     * @Assert\Type(type="string")
     */
    private $paymentId;

    /**
     * {@inheritdoc}
     */
    public function getData(): array
    {
        $data = [];

        if ($this->hasPayerId()) {
            $data['payer_id'] = $this->getPayerId();
        }

        return $data;
    }

    /**
     * @return string|null
     */
    public function getPayerId(): ?string
    {
        return $this->payerId;
    }

    /**
     * @return bool
     */
    public function hasPayerId(): bool
    {
        return !is_null($this->payerId);
    }

    /**
     * @param string|null $payerId
     *
     * @return Execute
     */
    public function setPayerId(string $payerId = null): Execute
    {
        $this->payerId = $payerId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getHttpUri(): string
    {
        return sprintf('payments/payment/%s/execute/', $this->getPaymentId());
    }

    /**
     * @return string|null
     */
    public function getPaymentId(): ?string
    {
        return $this->paymentId;
    }

    /**
     * @return bool
     */
    public function hasPaymentId(): bool
    {
        return !is_null($this->paymentId);
    }

    /**
     * @param string|null $paymentId
     *
     * @return Execute
     */
    public function setPaymentId(string $paymentId = null): Execute
    {
        $this->paymentId = $paymentId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getHttpMethod(): string
    {
        return self::METHOD_POST;
    }
}
