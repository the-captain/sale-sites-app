<?php

namespace PaypalExpressBundle\Service;

use Exception;
use InvalidArgumentException;
use PaypalExpressBundle\Contract\PacketDataInterface;
use PaypalExpressBundle\Contract\PacketInterface;
use PaypalExpressBundle\Contract\PacketResponseInterface;
use PaypalExpressBundle\Packet\AbstractAccessTokenPacket;
use PaypalExpressBundle\Packet\OAuthV2\GetAccessToken;
use PaypalExpressBundle\Packet\Payment\Create;
use PaypalExpressBundle\Packet\Payment\Execute;
use PaypalExpressBundle\Packet\Payment\ShowDetails;
use PaypalExpressBundle\Packet\ResponseResolver;
use RuntimeException;
use SaleSitesBundle\EventListener\ClientSubscriber;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use ThreeWebOneEntityBundle\Entity\PaymentConfig;

/**
 * Class PaypalExpressService.
 */
class PaypalExpressService
{
    const API_BASE_PATH = '';

    /**
     * @var bool
     */
    private $enabled = false;

    /**
     * @var string
     */
    private $account;

    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $secret;

    /**
     * @var string
     */
    private $environment;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var string
     */
    private $apiBasePath;

    /**
     * PaypalExpressService constructor.
     *
     * @param ClientSubscriber   $clientSubscriber
     * @param bool               $enabled
     * @param null|string        $apiBasePath
     * @param ValidatorInterface $validator
     * @param null|string        $account
     * @param null|string        $clientId
     * @param null|string        $secret
     * @param null|string        $environment
     */
    public function __construct(
        ClientSubscriber $clientSubscriber,
        ?string $apiBasePath,
        ?ValidatorInterface $validator,
        ?string $environment
    ) {
        $config = $this->getPaypalConfig($clientSubscriber);

        if (
            $config &&
            $config->getStatus() &&
            !empty($config->getAccount()) &&
            !empty($config->getClientId()) &&
            !empty($config->getClientSecret())
        ) {
            if (empty($apiBasePath) || empty($validator)) {
                throw new InvalidArgumentException(
                    'When paypal express is enabled - $validator, $account, $clientId and $secret is required.'
                );
            }

            $this->setEnabled($config->getStatus());
            $this->setApiBasePath($apiBasePath);
            $this->setValidator($validator);
            $this->setAccount($config->getAccount());
            $this->setClientId($config->getClientId());
            $this->setSecret($config->getClientSecret());

            if (!empty($environment)) {
                $this->setEnvironment($environment);
            }
        } else {
            $this->setEnabled(false);
        }
    }

    /**
     * @param ClientSubscriber $clientSubscriber
     * @return null|PaymentConfig
     */
    private function getPaypalConfig(ClientSubscriber $clientSubscriber)
    {
        $owner = $clientSubscriber->getUser();

        return $owner->getPaymentConfigByType(PaymentConfig::PAYPAL);
    }

    /**
     * @return bool|null
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool|null $enabled
     *
     * @return PaypalExpressService
     */
    public function setEnabled(bool $enabled = null): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEnvironment(): ?string
    {
        return $this->environment;
    }

    /**
     * @return bool
     */
    public function hasEnvironment(): bool
    {
        return !is_null($this->environment);
    }

    /**
     * @param string|null $environment
     *
     * @return PaypalExpressService
     */
    public function setEnvironment(string $environment = null): PaypalExpressService
    {
        $this->environment = $environment;

        return $this;
    }

    /**
     * @param Create $paymentData
     * @return array
     */
    public function createPayment(Create $paymentData): array
    {
        $paymentData
            ->setAccessToken($this->getAccessTocken());

        return $this->proceedPacket($paymentData)->getResponseData();
    }

    /**
     * @return string
     */
    public function getAccessTocken(): string
    {
        $getTokenCommand = new GetAccessToken();

        $getTokenCommand
            ->setSecret($this->getSecret())
            ->setClientId($this->getClientId())
            ->setLanguage('en_US');

        return $this->proceedPacket($getTokenCommand)->getResponseData()['access_token'];
    }

    /**
     * @return string|null
     */
    public function getSecret(): ?string
    {
        return $this->secret;
    }

    /**
     * @return bool
     */
    public function hasSecret(): bool
    {
        return !is_null($this->secret);
    }

    /**
     * @param string|null $secret
     *
     * @return PaypalExpressService
     */
    public function setSecret(string $secret = null): self
    {
        $this->secret = $secret;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getClientId(): ?string
    {
        return $this->clientId;
    }

    /**
     * @return bool
     */
    public function hasClientId(): bool
    {
        return !is_null($this->clientId);
    }

    /**
     * @param string|null $clientId
     *
     * @return PaypalExpressService
     */
    public function setClientId(string $clientId = null): self
    {
        $this->clientId = $clientId;

        return $this;
    }

    /**
     * @param PacketInterface $packet
     *
     * @return PacketResponseInterface
     */
    protected function proceedPacket(PacketInterface $packet): PacketResponseInterface
    {
        if ($this->isEnabled() && $this->validate($packet)) {
            $headers = [
                'Content-Type: application/json',
            ];

            $url = $this->getApiBasePath() . $packet->getHttpUri();
            $cUrlRequest = curl_init();

            $uPassword = $packet->getUserPassword();
            if (!empty($uPassword)) {
                curl_setopt($cUrlRequest, CURLOPT_USERPWD, $uPassword);
            }

            if ($packet instanceof AbstractAccessTokenPacket) {
                $accessToken = $packet->getAccessToken();

                if (!empty($accessToken)) {
                    $headers[] = 'Authorization: Bearer ' . $accessToken;
                }
            }

            curl_setopt($cUrlRequest, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($cUrlRequest, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($cUrlRequest, CURLOPT_HTTPHEADER, $packet->getHttpHeaders());

            if (PacketInterface::METHOD_POST === $packet->getHttpMethod()) {
                $data = $packet->getData();
                $body = json_encode($data);
                $headers[] = 'Content-Length: ' . strlen($body);
                curl_setopt($cUrlRequest, CURLOPT_POST, true);
                curl_setopt($cUrlRequest, CURLOPT_POSTFIELDS, $body);
                curl_setopt(
                    $cUrlRequest,
                    CURLOPT_HTTPHEADER,
                    $headers
                );
            } elseif (PacketInterface::METHOD_AUTH === $packet->getHttpMethod()) {
                curl_setopt($cUrlRequest, CURLOPT_POST, true);
                curl_setopt($cUrlRequest, CURLOPT_POSTFIELDS, http_build_query($packet->getData()));
            }

            curl_setopt($cUrlRequest, CURLOPT_URL, $url);

            try {
                $data = curl_exec($cUrlRequest);
            } catch (Exception $e) {
                throw new RuntimeException('unable to perform request: <![CDATA[' . $e->getMessage() . ']]>');
            }

            return new ResponseResolver($data);
        }
    }

    /**
     * @param PacketDataInterface $data
     * @return bool
     */
    protected function validate(PacketDataInterface $data): bool
    {
        $violations = $this->getValidator()->validate($data);

        if ($violations->count() > 0) {
            $message = [];

            /** @var ConstraintViolation $violation */
            foreach ($violations as $violation) {
                $message[] = 'Property ' .
                    get_class($violation->getRoot()) .
                    '.' .
                    $violation->getPropertyPath() .
                    ' => ' .
                    $violation->getMessage();
            }

            throw new ValidatorException(implode(PHP_EOL, $message));
        }

        return true;
    }

    /**
     * @return ValidatorInterface|null
     */
    public function getValidator(): ?ValidatorInterface
    {
        return $this->validator;
    }

    /**
     * @return bool
     */
    public function hasValidator(): bool
    {
        return !is_null($this->validator);
    }

    /**
     * @param ValidatorInterface|null $validator
     *
     * @return PaypalExpressService
     */
    public function setValidator(ValidatorInterface $validator = null): self
    {
        $this->validator = $validator;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getApiBasePath(): ?string
    {
        return $this->apiBasePath;
    }

    /**
     * @return bool
     */
    public function hasApiBasePath(): bool
    {
        return !is_null($this->apiBasePath);
    }

    /**
     * @param string|null $apiBasePath
     *
     * @return PaypalExpressService
     */
    public function setApiBasePath(string $apiBasePath = null): self
    {
        $this->apiBasePath = $apiBasePath;

        return $this;
    }

    /**
     * @param Execute $paymentData
     * @return array
     */
    public function executePayment(Execute $paymentData): array
    {
        $paymentData
            ->setAccessToken($this->getAccessTocken());

        return $this->proceedPacket($paymentData)->getResponseData();
    }

    /**
     * @param ShowDetails $paymentData
     * @return array
     */
    public function showDetails(ShowDetails $paymentData): array
    {
        $paymentData
            ->setAccessToken($this->getAccessTocken());

        return $this->proceedPacket($paymentData)->getResponseData();
    }

    /**
     * @return string|null
     */
    public function getAccount(): ?string
    {
        return $this->account;
    }

    /**
     * @return bool
     */
    public function hasAccount(): bool
    {
        return !is_null($this->account);
    }

    /**
     * @param string|null $account
     *
     * @return PaypalExpressService
     */
    public function setAccount(string $account = null): self
    {
        $this->account = $account;

        return $this;
    }
}
