(function () {

    'use strict';

    angular
        .module('PaypalExpress', [
            'paypal-express.components'
        ]);

    angular.module('paypal-express.shared', []);
    angular.module('paypal-express.components', ['paypal-express.shared']);
})();
