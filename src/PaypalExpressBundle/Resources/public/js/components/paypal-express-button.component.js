(function () {
    'use strict';

    angular
        .module('paypal-express.components')
        .component('paypalExpressButton', {
            template: '<div ng-cloak ng-attr-id="{{vmECB.buttonId}}" ng-init="vmECB.init()"></div>',
            bindings: {
                environment: '@',
                onPayment: '&',
                onAuthorize: '&',
                onCancel: '&',
                onError: '&',
                validForm: '='
            },
            controller: ["$scope", "$timeout", "$window", "$rootScope", function ($scope, $timeout, $window, $rootScope) {
                var vm = this;

                if($window.buttonId === undefined) {
                    $window.buttonId = 0;
                } else {
                    $window.buttonId += 1;
                }

                vm.environment = vm.environment || 'sandbox';

                if (vm.environment === 'sandbox') {
                    console.warn('Paypal express button directive working in demo mode.');
                }

                vm.init = function () {
                    vm.buttonId = 'paypal-express-button-' + $window.buttonId;

                    $timeout(function () {
                        paypal.Button.render({
                            env: vm.environment,
                            payment: function () {
                                return vm.onPayment({})
                            },
                            onAuthorize: function (data, action) {
                                return vm.onAuthorize({'data': data, 'actions': action})
                            },
                            onCancel: function (data, action) {
                                return vm.onCancel({'data': data, 'actions': action});
                            },
                            validate: function (actions) {
                                vm.validForm(actions);
                            },
                            onClick: function () {
                                $rootScope.$broadcast('paypal_submit');
                            }
                        }, vm.buttonId);
                    });
                }
            }],
            controllerAs: "vmECB"
        });

})();
