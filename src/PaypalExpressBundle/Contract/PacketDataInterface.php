<?php

namespace PaypalExpressBundle\Contract;

/**
 * Interface PacketDataInterface.
 */
interface PacketDataInterface
{
    /**
     * @return array
     */
    public function getData(): array;
}
