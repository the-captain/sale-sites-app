<?php

namespace PaypalExpressBundle\Contract;

/**
 * Interface PacketResponseInterface.
 */
interface PacketResponseInterface
{
    /**
     * @return array
     */
    public function getResponseData(): array;
}
