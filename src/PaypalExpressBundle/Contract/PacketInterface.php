<?php

namespace PaypalExpressBundle\Contract;

/**
 * Interface PacketInterface.
 */
interface PacketInterface extends PacketDataInterface
{
    const METHOD_POST = 'POST';

    const METHOD_GET = 'GET';

    const METHOD_AUTH = 'AUTH';

    /**
     * @return string
     */
    public function getHttpUri(): string;

    /**
     * @return string
     */
    public function getHttpMethod(): string;

    /**
     * @return array
     */
    public function getHttpHeaders(): array;

    /**
     * @return string
     */
    public function getUserPassword(): string;
}
