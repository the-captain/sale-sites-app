<?php

namespace PaypalExpressBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration.
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('paypal_express');

        $rootNode
            ->children()
            ->scalarNode('api_path')->defaultValue('')->end()
            ->scalarNode('environment')->defaultValue('')->end();

        return $treeBuilder;
    }
}
