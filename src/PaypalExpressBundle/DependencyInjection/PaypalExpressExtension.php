<?php

namespace PaypalExpressBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;

/**
 * Class PaypalExpressExtension.
 */
class PaypalExpressExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $processor = new Processor();
        $configuration = new Configuration();

        //reading config
        $config = $processor->processConfiguration($configuration, $configs);

        //preparing bundle service params
        $container->setParameter('paypal_express_api_url', $config['api_path']);
        $container->setParameter('paypal_express_environment', $config['environment']);
    }
}
