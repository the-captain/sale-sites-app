sale-sites-app
==============

Sale Sites is one of the major parts of our application. 
Entity Bundle is the core bundle of the application located in the separate repository and loaded through composer in `vendors` folder.

Deployment and Update of major changes:
=========

#Adminapp side:
Always keep in mind that you need to check and redeploy(if needed) AdminApp side first:
`https://bitbucket.org/mxnr/adminapp/overview`

#SaleSite side:

##For the first deployment:

###1.1 Clone the repository:
    `git clone git@bitbucket.org:the-captain/sale-sites-app.git`
    
###1.2 Go to the project's folder:
    `cd sale-sites-app`

###1.3 Proceed to the 2.1 part

##For Update:

###1.1 Go to the project's folder:
    `cd sale-sites-app`
    
###1.2 Clear the cache:
    `rm -rf var/cache/dev/*`
    
###1.3 Pull the latest master:
    `git pull origin master`
    
###2.1 Install new dependencies in composer.lock:
    `composer install`
    
###2.2 Check that Entity Bundle has the latest version(or the version that you need):
    - For update:
        `composer update 3web1/entity-bundle`
    - The problem can be in that you forgot to tag entity bundle's version. You need to go to Entity Bundle project and check Readme file:
            https://bitbucket.org/the-captain/entitybundle/overview
    
###3.1 Install npm dependencies:
    `npm install`
    
###3.2 Install bower dependencies(DEPRECATED. Needs to be replaced by yarn):
    `bower install`
    
###4. Webpack (http://symfony.com/doc/current/frontend.html - read this article):
    - Compile
        `yarn run encore production`

    - If you need to install yarn (https://yarnpkg.com/lang/en/docs/install/#mac-tab)
        `brew install yarn` - for mac 
        or
        `curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
        echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list`
       
        `sudo apt-get update && sudo apt-get install yarn` - on Linux

##Optional
    - For assets cleaning use
        `bin/console assets:install web/sites`
    - We don't use assetic and bower anymore - make sure we migrated all parts